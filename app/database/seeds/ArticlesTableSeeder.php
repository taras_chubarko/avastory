<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class ArticlesTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create('ru_RU');

		foreach(range(1, 20) as $index)
		{
			$im = $faker->image($dir = '/var/www/admin/www/avastory.progim.net/public/uploads/articles', $width = 800, $height = 600);
			$im = explode('/var/www/admin/www/avastory.progim.net/public/uploads/articles/', $im);
			
			Article::create([
				'name' => $faker->sentence($nbWords = 6),
				'body' => $faker->realText(500),
				'image' => $im[1],
				'status' => 1,
				'user_id' => 1,
			]);
		}
	}

}