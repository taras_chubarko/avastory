<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class HistoriesTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create('ru_RU');

		foreach(range(1, 30) as $index)
		{
			$im = $faker->image($dir = '/var/www/admin/www/avastory.progim.net/public/uploads/histories', $width = 800, $height = 600);
			$im = explode('/var/www/admin/www/avastory.progim.net/public/uploads/histories/', $im);
			
			History::create([
				'name' => $faker->sentence($nbWords = 6),
				'body' => $faker->realText(1000, 2),
				'image' => $im[1],
				'status' => 1,
				'user_id' => 1,
			]);
		}
	}

}