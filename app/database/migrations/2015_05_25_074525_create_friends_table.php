<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFriendsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		
		Schema::create('friends_req', function(Blueprint $table)
		{
			
			$table->increments('id');
			$table->integer('user_from');
			$table->integer('user_to');
			$table->foreign('user_from')->references('id')->on('users');
			$table->foreign('user_to')->references('id')->on('users');
			$table->timestamps();
			
		});
		
		Schema::create('friends', function(Blueprint $table)
		{
			$table->integer('user_one')->unsigned();
			$table->integer('user_two')->unsigned();
			$table->foreign('user_one')->references('id')->on('users');
			$table->foreign('user_two')->references('id')->on('users');
		   
			$table->primary(array('user_one', 'user_two'));
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('friends_req');
		Schema::drop('friends');
	}

}
