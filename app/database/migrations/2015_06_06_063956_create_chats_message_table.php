<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateChatsMessageTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('chats_message', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('chat_id');
			$table->foreign('chat_id')->references('id')->on('chats')->onDelete('cascade');
			$table->integer('user_id');
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
			$table->text('message');
			$table->integer('flags');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('chats_message');
	}

}
