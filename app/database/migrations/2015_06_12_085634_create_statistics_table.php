<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStatisticsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('looks', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('type');
			$table->integer('type_id');
			$table->string('ip');
			$table->timestamps();
		});
		
		Schema::create('likes', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('type');
			$table->integer('type_id');
			$table->string('ip');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('looks');
		Schema::drop('likes');
	}

}
