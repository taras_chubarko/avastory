<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUslugisTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('uslugis', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->longText('body');
			$table->string('slug');
			$table->string('image');
			$table->string('title');
			$table->string('keywords');
			$table->longText('description');
			$table->integer('status');
			$table->integer('user_id');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('uslugis');
	}

}
