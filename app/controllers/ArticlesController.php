<?php

class ArticlesController extends \BaseController {

	/**
	 * Display a listing of articles
	 *
	 * @return Response
	 */
	public function index()
	{
		$articles = Article::orderBy('updated_at', 'desc')->paginate(10);

		return View::make('articles.index', compact('articles'));
	}
	
	/*
	 * function articles
	 * @param 
	 * ---------------------------------------
	 * | 
	 * ---------------------------------------
	 */
	
	public function articles() {
		
		$articles = Article::orderBy('updated_at', 'desc')->paginate(10);

		return View::make('articles.articles', compact('articles'));
	}

	/**
	 * Show the form for creating a new article
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('articles.create');
	}

	/**
	 * Store a newly created article in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Article::$rules, Article::$messages);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		Article::create($data);

	        return Redirect::route('admin.articles.index')->with('success', 'Статья создана.');
	}

	/**
	 * Display the specified article.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($slug)
	{
		$article = Article::slug($slug);
		
		if (empty($article->id))
		{
			App::abort(404);
		}
		
		Event::fire('show.article', array($article));

		return View::make('articles.show', compact('article'));
	}

	/**
	 * Show the form for editing the specified article.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$article = Article::find($id);

		return View::make('articles.edit', compact('article'));
	}

	/**
	 * Update the specified article in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$article = Article::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Article::$rules, Article::$messages);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$article->update($data);

		return Redirect::route('admin.articles.index')->with('success', 'Статья обновлена.');
	}

	/**
	 * Remove the specified article from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Article::destroy($id);

		return Redirect::route('admin.articles.index')->with('success', 'Статья удалена.');
	}

}
