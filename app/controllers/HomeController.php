<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function showWelcome()
	{

                if (!Sentry::check())
                {
		    return View::make('front');
                }
                else
                {
		    
		    if(Input::has('chat') && Input::has('user_to'))
		    {
			
			$user1 = Sentry::getUser();
	              
			$user2 = Sentry::findUserById(Input::get('user_to'));
			
			$chat = Chat::find(Input::get('chat'));
			
			return View::make('frontLogined', compact('user1', 'user2', 'chat'));
		    }
		    else
		    {
			return View::make('frontLogined');
		    }
                }

		
	}
	
	
	
	
	/*
	 * function success
	 * @param $id
	 * ---------------------------------------
	 * | 
	 * ---------------------------------------
	 */
	
	public function success() {
	    
	    $data = Input::all();
	    $user = Sentry::getUser();
	    
	    if($data['ik_x_type'] == 'onepay')
	    {
		
		$profile = Profile::where('user_id', $user->id)->first();
		$profile->onepay = 1;
		$profile->onpeystart = $data['ik_inv_crt'];
		$profile->save();
	    }
	    
	    if($data['ik_x_type'] == 'vips')
	    {
		$profile = Profile::where('user_id', $user->id)->first();
		$profile->vip = 1;
		$profile->save();
	    }
	    
	    
	    //print '<pre>' . htmlspecialchars(print_r($data, true)) . '</pre>';
	    
	    return Redirect::route('pages.view', 'psihologicheskaya-podderzhka')->with('success', 'Оплата принята');
	}
	
	
	/*
	 * function fail
	 * @param $id
	 * ---------------------------------------
	 * | 
	 * ---------------------------------------
	 */
	
	public function fail() {
	    $data = Input::all();
	    //print '<pre>' . htmlspecialchars(print_r($data, true)) . '</pre>';
	    
	    return Redirect::route('pages.view', 'psihologicheskaya-podderzhka')->with('info', 'Оплата отменена');
	}
	
	/*
	 * function pending
	 * @param $	
	 * ---------------------------------------
	 * | 
	 * ---------------------------------------
	 */
	
	public function pending() {
	    $data = Input::all();
	    //print '<pre>' . htmlspecialchars(print_r($data, true)) . '</pre>';
	    return Redirect::route('pages.view', 'psihologicheskaya-podderzhka')->with('info', 'Оплата не завершена.');
	}
	
	

}
