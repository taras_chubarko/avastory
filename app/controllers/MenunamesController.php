<?php

class MenunamesController extends \BaseController {

	/**
	 * Display a listing of menunames
	 *
	 * @return Response
	 */
	public function index()
	{
		$menunames = Menuname::all();

		return View::make('menu.index', compact('menunames'));
	}

	/**
	 * Show the form for creating a new menuname
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('menu.create');
	}

	/**
	 * Store a newly created menuname in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Menuname::$rules);
		$validator->setAttributeNames(Menuname::$names);
		
		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		Menuname::create($data);

		return Redirect::route('admin.menu.index')->with('success', 'Меню создано');
	}

	/**
	 * Display the specified menuname.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$menuname = Menuname::findOrFail($id);

		return View::make('menunames.show', compact('menuname'));
	}

	/**
	 * Show the form for editing the specified menuname.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$menuname = Menuname::find($id);

		return View::make('menunames.edit', compact('menuname'));
	}

	/**
	 * Update the specified menuname in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$menuname = Menuname::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Menuname::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$menuname->update($data);

		return Redirect::route('menunames.index');
	}

	/**
	 * Remove the specified menuname from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Menuname::destroy($id);

		return Redirect::route('menunames.index');
	}

}
