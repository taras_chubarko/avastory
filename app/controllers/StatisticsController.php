<?php

class StatisticsController extends \BaseController {
	
	/* print '<pre>' . htmlspecialchars(print_r($input, true)) . '</pre>';
	 * function setLike
	 * @param $id
	 *-----------------------------------
	 *| Ставим лайк
	 *-----------------------------------
	 */
	
	function setLike() {
		
		$today = new DateTime('today');
		
		$ip = Request::getClientIp();
		
		$data = Input::all();
		
		$like = Like::where('type', $data['type'])->where('type_id', $data['type_id'])->where('ip', $ip)->where('created_at', '>=', $today)->first();
		
		if(!$like)
		{
			$lk = new Like;
			$lk->type = $data['type'];
			$lk->type_id = $data['type_id'];
			$lk->ip = $ip;
			$lk->save();
		}
		
		$likes = Like::where('type', $data['type'])->where('type_id', $data['type_id'])->count();
	 
		
		return Response::json(['likes' => $likes, 'type_id' => $data['type_id']]);
		
		
	}
}
