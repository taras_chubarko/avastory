<?php

class UsersController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/
        
        public function login()
	{
               try
		{
		    // Login credentials
		    $credentials = array(
			'email'    => Input::get('email'),
			'password' => Input::get('password'),
		    );
		
		    // Authenticate the user
		    $user = Sentry::authenticate($credentials, false);
		    
			if($user->email == 'tchubarko@gmail.com')
			{
			    Sentry::loginAndRemember($user);
			}
			
		    return Redirect::back()->with('info', 'Вы успешно авторизовались!');
		    
		}
		catch (Cartalyst\Sentry\Users\LoginRequiredException $e)
		{
		    //echo 'Login field is required.';
		    return Redirect::back()->with('error', '<p>E-mail обязательно для заполнения.</p>');
		}
		catch (Cartalyst\Sentry\Users\PasswordRequiredException $e)
		{
		    //echo 'Password field is required.';
		    return Redirect::back()->with('error', '<p>Пароль обязательно для заполнения.</p>');
		}
		catch (Cartalyst\Sentry\Users\WrongPasswordException $e)
		{
		    //echo 'Wrong password, try again.';
		    return Redirect::back()->with('error', '<p>Неверный пароль, попробуйте еще раз.</p>');
		}
		catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
		{
		    //echo 'User was not found.';
		    return Redirect::back()->with('error', '<p>Пользователь ненайден.</p>');
		}
		catch (Cartalyst\Sentry\Users\UserNotActivatedException $e)
		{
		   // echo 'User is not activated.';
		   return Redirect::back()->with('error', '<p>Пользователь не активирован.</p>');
		}
		
		// The following is only required if the throttling is enabled
		catch (Cartalyst\Sentry\Throttling\UserSuspendedException $e)
		{
		    //echo 'User is suspended.';
		    return Redirect::back()->with('error', '<p>Пользователь деактивирован.</p>');
		}
		catch (Cartalyst\Sentry\Throttling\UserBannedException $e)
		{
		    //echo 'User is banned.';
		    return Redirect::back()->with('error', '<p>Пользователь заблокирован.</p>');
		}
	}
        
        public function user_logout() {
		
		Sentry::logout();
		return Redirect::route('front');
		
	}

	public function register()
	{
		return View::make('users.register');
	}
        
        public function register_post()
	{
		$input = Input::all();
		
		$validation = Validator::make($input, User::$rules, User::$messages);
		
		//$pre = '<pre>' . htmlspecialchars(print_r($input, true)) . '</pre>';
	    
		
		if ($validation->passes())
		{
			try
			{
			    
			    $user = Sentry::createUser(array(
				'email'    => Input::get('email'),
				'password' => Input::get('password'),
			    ));
			    
			    $profile = new Profile;
			    $profile->user_id = $user->id;
			    $profile->name = Input::get('name');
			    $profile->city = Input::get('city');
			    $profile->adres = Input::get('adres');
			    $profile->telephone = Input::get('telephone');
			    $profile->gender = Input::get('gender');
			    $profile->gender_search = Input::get('gender_search');
			    $profile->lat = Input::get('lat');
			    $profile->lng = Input::get('lng');
			    $profile->save();
			    
			    
			     
			    $userGroup = Sentry::findGroupById(2);
			 
			    $user->addGroup($userGroup);
			    
			    $activationCode = $user->getActivationCode();
					
				$array = array();
					
				$array['email'] = Input::get('email');
				$array['password'] = Input::get('password');
				$array['activationCode'] = $activationCode;
					
				Mail::queue('emails.registerToAdmin', $array, function ($message) {
					$message->to(Config::get('mail.admin_mail'))->subject('Новая регистрация на сайте');
				});
                                
                                Mail::queue('emails.registerToUser', $array, function ($message) {
					$message->to(Input::get('email'))->subject('Регистрация на сайте "'. Config::get('site.site_name').'"');
				});
			    
			}
			catch (Cartalyst\Sentry\Users\UserExistsException $e)
			{
			    return Redirect::back()->with('error', 'Такой пользователь уже зарегистрирован');
			}
			
			
			return Redirect::to('/')->with('success', 'Спасибо за регистрацию на нашем сайте. Код активации отправлен вам на почту.');
		}
			return Redirect::back()->withInput()->withErrors($validation);
	}
        
        
        
        function activate($activationCode) {
		try
		{
		    $user = Sentry::findUserByActivationCode($activationCode);
		    // Attempt to activate the user
		    if ($user->attemptActivation($activationCode))
		    {
			return Redirect::route('front')->with('success', 'Вы активирували аккаунт. Можете ввести свои данные в форме входа');
		    }
		    else
		    {
			return Redirect::route('front')->with('error', 'Ошибка активации');
		    }
		}
		catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
		{
		    return Redirect::route('front')->with('warning', 'Аккаунт уже был активирован.');
		}
		catch (Cartalyst\Sentry\Users\UserAlreadyActivatedException $e)
		{
		    return Redirect::route('front')->with('info', 'Аккаунт уже был активирован');
		}
	}
	
	
	/*
	 * function users
	 * @param 
	 * ---------------------------------------
	 * | Список пользователей
	 * ---------------------------------------
	 */
	
	public function index() {
	      
	      $admins = User::leftJoin('users_groups', 'users.id', '=', 'users_groups.user_id')->where('group_id', 1)->paginate(20);
	      $users = User::leftJoin('users_groups', 'users.id', '=', 'users_groups.user_id')->where('group_id', 2)->orderBy('created_at', 'DESC')->paginate(20);
	      
	      //print '<pre>' . htmlspecialchars(print_r($groups, true)) . '</pre>';
	      return View::make('admin.users.index', compact('admins', 'users'));		
	}
	
	/*
	 * function roles
	 * @param 
	 * ---------------------------------------
	 * | Роли пользователей
	 * ---------------------------------------
	 */
	
	public function roles() {
	      
	      $roles = Sentry::findAllGroups();;
	      
	      return View::make('admin.users.roles', compact('roles'));
	}
	
	/*
	 * function edit
	 * @param $arg
	 *---------------------------------------
	 *|
	 *---------------------------------------
	 */
	
	public function edit($id) {
		
		$user = Sentry::getUser();   
		   
		return View::make('users.edit', compact('user'));   
	}
        
		   /*
			* function edit_save
			* @param $id
			*---------------------------------------
			*|
			*---------------------------------------
			*/
		   
		   public function edit_save($id) {
			
		            $user = Sentry::findUserById($id);
					 
				   $rules = array(
					  'password' => 'required|min:6|confirmed',
					  'password_confirmation' => 'required|min:6',
					  'name' => 'required|alpha_num',
				     );
				  
				     $messages = array(
						 'password.required'              => 'Поле "Пароль" обязательно для заполнения',
						 'password_confirmation.required' => 'Поле "Повтор пароля" обязательно для заполнения',
						 'password.confirmed'             => 'Поле "Повтор паролля" несовпадает',
						 'name.required'                  => 'Поле "Имя" обязательно для заполнения',
					  );
					 
					 
					  //print '<pre>' . htmlspecialchars(print_r($input, true)) . '</pre>';
					  $input = Input::all();
					  
					  $validation = Validator::make($input, $rules, $messages);
					  
					  if ($validation->passes())
					  {
							  
							  $user->name = $input['name'];	
							  $user->password = $input['password_confirmation'];
							  $user->save();
							  
							  return Redirect::back()->with('success', 'Изминения сохранены');
					  }
					  
					  return Redirect::back()->withInput()->withErrors($validation);
		
		   }
		   
		   /*
			* function favorite
			* @param $id
			*---------------------------------------
			*|
			*---------------------------------------
			*/
		   
		   public function favorite($id) {
				
		       $user = Sentry::findUserById($id);  
		       
			   //print '<pre>' . htmlspecialchars(print_r(Request::segment(2), true)) . '</pre>';
			   
		       return View::make('users.favorite', compact('user'));  
		   }
        
        
	
	/*
	 * function user_view
	 * @param $id
	 * ---------------------------------------
	 * | Профиль пользователя
	 * ---------------------------------------
	 */
	
	public function user_view($id) {
	      
	      $user = Sentry::findUserById($id);
	      
	      if(Sentry::getUser()->id == $id || Sentry::getUser()->hasAccess('users.edit'))
	      {
		     return View::make('profiles.my.profile', compact('user'));  
	      }
	      else
	      {
		     return View::make('profiles.any.profile', compact('user'));  
	      }
	      
	}
	
	/*
	 * function information
	 * @param $id
	 * ---------------------------------------
	 * | Информация о пользователе
	 * ---------------------------------------
	 */
	
	public function information($id) {
	      
	      $user = Sentry::findUserById($id);
	      
	      if ( is_null($user)  || $user->activated != 1 )
	      {
		     App::abort(404);
	      }
	      
	      if(Sentry::getUser()->id == $id || Sentry::getUser()->hasAccess('users.edit'))
	      {
		     return View::make('profiles.my.information', compact('user'));  
	      }
	      else
	      {
		     return View::make('profiles.any.information', compact('user'));  
	      }
	      
	}
	
	/*
	 * function information
	 * @param $id
	 * ---------------------------------------
	 * | Фото
	 * ---------------------------------------
	 */
	
	public function photos($id) {
	      
	      $user = Sentry::findUserById($id);
	      
	      if(Sentry::getUser()->id == $id || Sentry::getUser()->hasAccess('users.edit'))
	      {
		     return View::make('profiles.my.photos', compact('user'));  
	      }
	      else
	      {
		     return View::make('profiles.any.photos', compact('user'));  
	      }
	      
	}
	
	
	
	/*
	 * function information_about_save
	 * @param $id
	 * ---------------------------------------
	 * | 
	 * ---------------------------------------
	 */
	
	public function information_about_save($id) {
	      $rules = array(
		     'about' => 'required|min:50',
	      );
       
	      $messages = array(
		     'about.required' => 'Поле "О себе" обязательно для заполнения',
		     'about.min' => 'Поле "О себе" должно быть не менее 50 символов',
	      );
	      $input = Input::all();
	      $validation = Validator::make($input, $rules, $messages);
	        
	      if ($validation->passes())
	      {
		     $profile = Profile::find(Input::get('profile_id'));
		     $profile->about = Input::get('about');
		     $profile->save();
		     
		     return Redirect::back()->with('success', 'Изминения сохранены');
	      }
	      
	      return Redirect::back()->withInput()->withErrors($validation);
	}
	
	/*
	 * function information_thinking_save
	 * @param $id
	 * ---------------------------------------
	 * | 
	 * ---------------------------------------
	 */
	
	public function information_thinking_save($id) {
	      $rules = array(
		     'thinking' => 'required|min:10',
	      );
       
	      $messages = array(
		     'thinking.required' => 'Поле "О чем вы думаете" обязательно для заполнения',
		     'thinking.min' => 'Поле "О чем вы думаете" должно быть не менее 10 символов',
	      );
	      $input = Input::all();
	      $validation = Validator::make($input, $rules, $messages);
	        
	      if ($validation->passes())
	      {
		     $profile = Profile::find(Input::get('profile_id'));
		     $profile->thinking = Input::get('thinking');
		     $profile->save();
		     
		     return Redirect::back()->with('success', 'Изминения сохранены');
	      }
	      
	      return Redirect::back()->withInput()->withErrors($validation);
	}
	
	
	/*
	 * function user_profile_save_name
	 * @param $id
	 * ---------------------------------------
	 * | Сохранение имени в профиле
	 * ---------------------------------------
	 */
	
	public function user_profile_save_name() {
	      
	      $profile = Profile::find(Input::get('profile_id'));
	      $profile->name = Input::get('name');
	      $profile->save();
	      
	}
	
	
	/*
	 * function user_profile_save_telephone
	 * @param
	 * ---------------------------------------
	 * | Сохранение телефона в профиле
	 * ---------------------------------------
	 */
	
	public function user_profile_save_telephone() {
	      
	      $profile = Profile::find(Input::get('profile_id'));
	      $profile->telephone = Input::get('telephone');
	      $profile->save();
	      
	}
	
	/*
	 * function user_profile_save_city
	 * @param $id
	 * ---------------------------------------
	 * | Сохранение города в профиле
	 * ---------------------------------------
	 */
	
	public function user_profile_save_city() {
	      
	      $profile = Profile::find(Input::get('profile_id'));
	      $profile->city = Input::get('city');
	      $profile->adres = Input::get('adres');
	      $profile->save();
	      
	}
	
	
	/*
	 * function user_profile_save_bithday
	 * @param 
	 * ---------------------------------------
	 * | Сохранение даты рождения и возраста в профиле
	 * ---------------------------------------
	 */
	
	public function user_profile_save_bithday() {
	      
	      $str = explode(' ', Input::get('bithday'));
	      
	      if($str[1] == 'января')
	      {
		   $str[1] = '01';  
	      }
	      
	      if($str[1] == 'февраля')
	      {
		   $str[1] = '02';  
	      }
	      
	      if($str[1] == 'марта')
	      {
		   $str[1] = '03';  
	      }
	      
	      if($str[1] == 'апреля')
	      {
		   $str[1] = '04';  
	      }
	      
	      if($str[1] == 'мая')
	      {
		   $str[1] = '05';  
	      }
	      
	      if($str[1] == 'июня')
	      {
		   $str[1] = '06';  
	      }
	      
	      if($str[1] == 'июля')
	      {
		   $str[1] = '07';  
	      }
	      
	      if($str[1] == 'августа')
	      {
		   $str[1] = '08';  
	      }
	      
	      if($str[1] == 'сентября')
	      {
		   $str[1] = '09';  
	      }
	      
	      if($str[1] == 'октября')
	      {
		   $str[1] = '10';  
	      }
	      
	      if($str[1] == 'ноября')
	      {
		   $str[1] = '11';  
	      }
	      
	      if($str[1] == 'декабря')
	      {
		   $str[1] = '12';  
	      }
	      
	      $strD = implode('.', $str);
	      
	      
	      $date = strtotime($strD);
	      $bithday = date('Y-m-d', $date);
	      
	      //$birthday = strtotime($userBirthday); // Получаем unix timestamp нашего дня рождения
	      $years = date('Y') - date('Y',$date); // Вычисляем возраст БЕЗ учета текущего месяца и дня
	      $now = time(); // no comments
	      $nowBirthday = mktime(0,0,0,date('m',$date),date('d',$date),date('Y')); // Получаем день рождение пользователя в этом году
	      if ($nowBirthday > $now) {
		 $years --; // Если дня рождения ещё не было то вычитаем один год
	      }
	      
	      $profile = Profile::find(Input::get('profile_id'));
	      $profile->bithday = $bithday;
	      $profile->age = $years;
	      $profile->save();
	      
	      $age = $years . DateFormat::num2word($years, array(' год', ' года', ' лет'));
	     
	      return Response::json(['success' => true, 'age' => $age ], 200);
	      
	}
	
	
	/*
	 * function user_profile_save_gender
	 * @param
	 * ---------------------------------------
	 * | Сохранение пола в профиле
	 * ---------------------------------------
	 */
	
	public function user_profile_save_gender() {
	      $profile = Profile::find(Input::get('profile_id'));
	      $profile->gender = Input::get('gender');
	      $profile->save();
	}
	
	/*
	 * function user_profile_save_gender_search
	 * @param 
	 * ---------------------------------------
	 * | Сохранение я ищу в профиле
	 * ---------------------------------------
	 */
	
	public function user_profile_save_gender_search() {
	      $profile = Profile::find(Input::get('profile_id'));
	      $profile->gender_search = Input::get('gender_search');
	      $profile->save();
	}
	
	
	/*
	 * function user_profile_save_point
	 * @param 
	 * ---------------------------------------
	 * | Сохранение координат в профиле
	 * ---------------------------------------
	 */
	
	public function user_profile_save_point() {
	      
	      $profile = Profile::find(Input::get('profile_id'));
	      $profile->lat = Input::get('lat');
	      $profile->lng = Input::get('lng');
	      $profile->autopoint = 1;
	      $profile->save();
	      
	}
	
	
	/*
	 * function user_profile_save_work
	 * @param
	 * ---------------------------------------
	 * | Сохранение места работы в профиле
	 * ---------------------------------------
	 */
	
	public function user_profile_save_work() {
	      $rules = array(
		     'company' => 'required',
		     'office' => 'required',
		     'city' => 'required',
	      );
       
	      $messages = array(
		     'company.required' => 'Поле "Компания" обязательно для заполнения',
		     'office.required' => 'Поле "Должность" обязательно для заполнения',
		     'city.required' => 'Поле "Город" обязательно для заполнения',
	      );
	      $input = Input::all();
	      $validation = Validator::make($input, $rules, $messages);
	        
	      if ($validation->passes())
	      {
		     $work = array('company'    => Input::get('company'),
				   'office'     => Input::get('office'),
				   'city'       => Input::get('city'),
				   'year_start' => Input::get('year_start'),
				   'year_end'   => Input::get('year_end'),
				   );
		     
		     $profile = Profile::find(Input::get('profile_id'));
		     $profile->work = json_encode($work);
		     $profile->save();
		     
		     return Redirect::back()->with('success', 'Изминения сохранены');
	      }
	      
	      return Redirect::back()->withInput()->withErrors($validation);
	}
	
	
	
	/*
	 * function user_profile_save_vuz
	 * @param $id
	 * ---------------------------------------
	 * | Сохранение ВУЗа в профиле
	 * ---------------------------------------
	 */
	
	public function user_profile_save_vuz() {
	      
	      $rules = array(
		     'name' => 'required',
		     'specialty' => 'required',
		     'city' => 'required',
	      );
       
	      $messages = array(
		     'name.required' => 'Поле "Название ВУЗа" обязательно для заполнения',
		     'specialty.required' => 'Поле "Специальность" обязательно для заполнения',
		     'city.required' => 'Поле "Город" обязательно для заполнения',
	      );
	      $input = Input::all();
	      $validation = Validator::make($input, $rules, $messages);
	        
	      if ($validation->passes())
	      {
		     $vuz = array( 'name'       => Input::get('name'),
				   'specialty'  => Input::get('specialty'),
				   'city'       => Input::get('city'),
				   'year_start' => Input::get('year_start'),
				   'year_end'   => Input::get('year_end'),
				   );
		     
		     $profile = Profile::find(Input::get('profile_id'));
		     $profile->vuz = json_encode($vuz);
		     $profile->save();
		     
		     return Redirect::back()->with('success', 'Изминения сохранены');
	      }
	      
	      return Redirect::back()->withInput()->withErrors($validation);
	}
	
	
	/*
	 * function user_profile_save_school
	 * @param
	 * ---------------------------------------
	 * | Сохранение школы в профиле
	 * ---------------------------------------
	 */
	
	public function user_profile_save_school() {
	      $rules = array(
		     'name' => 'required',
	      );
       
	      $messages = array(
		     'name.required' => 'Поле "Название ВУЗа" обязательно для заполнения',
	      );
	      $input = Input::all();
	      $validation = Validator::make($input, $rules, $messages);
	        
	      if ($validation->passes())
	      {
		     $school = array(  'name'       => Input::get('name'),
				       'year_start' => Input::get('year_start'),
				       'year_end'   => Input::get('year_end'),
				   );
		     
		     $profile = Profile::find(Input::get('profile_id'));
		     $profile->school = json_encode($school);
		     $profile->save();
		     
		     return Redirect::back()->with('success', 'Изминения сохранены');
	      }
	      
	      return Redirect::back()->withInput()->withErrors($validation);
	}
	
	
	
	/*
	 * function profiles
	 * @param 
	 * ---------------------------------------
	 * | Анкеты
	 * ---------------------------------------
	 */
	
	public function profiles() {
	      
	      
	      if(Input::has('mygender'))
	      {
		   
		   $q = Profile::leftJoin('users', 'users.id', '=', 'profiles.user_id');
		   
		   if(Input::get('gender') != '')
		   {
		     $q->where('gender', Input::get('gender'));
		   }
		   
		   
		   if(Input::get('city') != '')
		   {
		      $q->where('city', 'like', '%'.Input::get('city').'%');
		   }
		   
		   
		    $age = Input::get('age');
		      
		     if($age['min'] == '')
		     {
			    $age['min'] = 18;
		     }
		     
		     if($age['max'] == '')
		     {
			    $age['max'] = 100;
		     }
		  
		   
		   if(!array_search('', $age))
		   {
		     $q->whereBetween('age', $age);
		     $q->orderBy('age', 'asc');
		   }
		   
		   $q->orderBy('created_at', 'desc');
		   
		   $users = $q->paginate(24);
		   
	      }
	      
	      else
	      {
	          $users = Profile::leftJoin('users', 'users.id', '=', 'profiles.user_id')->orderBy('created_at', 'desc')->paginate(24);
	      }
	      
	      //print '<pre>' . htmlspecialchars(print_r(Input::get('age'), true)) . '</pre>';
	      return View::make('profiles.profiles', compact('users'));
	}
	
	
	/*
	 * function information
	 * @param $id
	 * ---------------------------------------
	 * | Друзья
	 * ---------------------------------------
	 */
	
	public function friends($id) {
	      
	      $user = Sentry::findUserById($id);
	      
	      $friendsMe = Friend::where('user_one', $id)->orWhere('user_two', $id)->get();
	      
	      $friends = array();
	      
	      foreach($friendsMe as $friend)
	      {
		     if($friend->user_one == $id)
		     {
			   $friends[] =  Sentry::findUserById($friend->user_two);
		     }
		     else
		     {
			    $friends[] =  Sentry::findUserById($friend->user_one);
		     }
	      }
	      
	     
	      if(Sentry::getUser()->id == $id || Sentry::getUser()->hasAccess('users.edit'))
	      {
		     return View::make('profiles.my.friends', compact('user', 'friends'));  
	      }
	      else
	      {
		     return View::make('profiles.any.friends', compact('user', 'friends'));  
	      }
	      
	}
	
	
	
	/*
	 * function friends_add
	 * @param $id
	 *-----------------------------------
	 *|Добавить в друзья 
	 *-----------------------------------
	 */
	
	function friendsAdd($id) {
	      
	      $myuser = Sentry::getUser();
	      
	      $addReq = new FriendReq;
	      $addReq->user_from = $myuser->id;
	      $addReq->user_to = $id;
	      $addReq->save();
	      
	      return Redirect::back()->with('success', 'Предложение дружбы отправлено. Ожидайте подтверждения.');
	}
	
	/*
	 * function applyFriend
	 * @param $id
	 *-----------------------------------
	 *|Принять дружбу
	 *-----------------------------------
	 */
	
	function applyFriend($id) {
             
	     $myuser = Sentry::getUser();
	     
	      FriendReq::where('user_from', $id)->where('user_to', $myuser->id)->delete();
	     
	      $applyReq = new Friend;
	      $applyReq->user_one = $id;
	      $applyReq->user_two = $myuser->id;
	      $applyReq->save();
	     
             return Redirect::back()->with('success', 'Предложение дружбы вами одобрено.');
	}
	
	/*
	 * function cencelFriend
	 * @param $id
	 *-----------------------------------
	 *|Отклонить дружбу
	 *-----------------------------------
	 */
	
	function cencelFriend($id) {
	      
	      $myuser = Sentry::getUser();
	      
	      $cancel = FriendReq::where('user_from', $myuser->id)->where('user_to', $id)->delete();
	     
              return Redirect::back()->with('success', 'Вы отменили заявку.');
	}
	
	/*
	 * function delFriend
	 * @param $id
	 *-----------------------------------
	 *|Удаление из друзей
	 *-----------------------------------
	 */
	
	function delFriend($id) {
	      
	      $myuser = Sentry::getUser();
	      
	      Friend::whereRaw('(user_one='.$myuser->id.' AND user_two='.$id.') OR (user_one='.$id.' AND user_two='.$myuser->id.')')->delete();
	      
	      return Redirect::back()->with('success', 'Пользователь удален из ваших друзей.'); 
	}
	
	
	/*
	 * function profilesJson
	 * @param 
	 * ---------------------------------------
	 * | Профили на карте
	 * ---------------------------------------
	 */
	
	public function profilesJson() {
	      $users = Sentry::findAllUsers();
	
	      $items = array();
	      foreach($users as $user)
	      {
		     if($user->profile['gender'] == 1)
		     {
			    $icon = '/themes/site/img/menm.png';
		     }
		     else
		     {
			    $icon = '/themes/site/img/womenm.png';
		     }
		     
		     if (!Sentry::check())
		     {
			    $ovrls = View::make('profiles.ovrlsNotlogin', compact('user'))->render();
		     }
		     else
		     {
			     $ovrls = View::make('profiles.ovrls', compact('user'))->render();
		     }
		      $items[] = array('lat' => $user->profile['lat'],
				       'lng' => $user->profile['lng'],
				       'icon' => $icon,
				       'ovrls' => $ovrls,
				       );
	      }
	
	 
	 return Response::json($items);
	}
	
	
	
	/*
	 * function settings
	 * @param $id
	 * ---------------------------------------
	 * | Настройки
	 * ---------------------------------------
	 */
	
	public function settings($id) {
	      
	      $user = Sentry::findUserById($id);
	      
	      if(Sentry::getUser()->id != $id)
	      {
		     if(Sentry::getUser()->hasAccess('users.edit')){
			    
		     }
		     else
		     {
			    App::abort(404);
		     }
		     
		    
	      }
	      //if(Sentry::getUser()->id == $id || Sentry::getUser()->hasAccess('users.edit'))
	      
	      return View::make('profiles.settings', compact('user'));
	}
	
	/*
	 * function settings_save
	 * @param $id
	 * ---------------------------------------
	 * | Сохранение настроек
	 * ---------------------------------------
	 */
	
	public function settings_save($id) {
	      
	       $rules = array(
		     'oldpass' => 'required',
		     'newpass' => 'required|min:6|confirmed',
		     'newpass_confirmation' => 'required|min:6',
	      );
       
	      $messages = array(
		     'oldpass.required' => 'Поле "Старый пароль" обязательно для заполнения',
		     'newpass.required' => 'Поле "Новый пароль" обязательно для заполнения',
		     'newpass.min' => 'Поле "Новый пароль" должно быть минимум 6 знаков',
		     'newpass.confirmed' => 'Не совпадение паролей',
		     'newpass_confirmation.required' => 'Поле "Повторить пароль" обязательно для заполнения',
		     'newpass_confirmation.min' => 'Поле "Повторить пароль" должно быть минимум 6 знаков',
	      );
	      
	      $input = Input::all();
	      
	      $validation = Validator::make($input, $rules, $messages);
	        
	      if ($validation->passes())
	      {
		     
		      $user = Sentry::findUserById($id);
		      $user->password = $input['newpass_confirmation'];
		      $user->save();
		      
		      return Redirect::back()->with('success', 'Пароль изменен.'); 
		
	      }
	      
	      return Redirect::back()->withInput()->withErrors($validation);
	}
	
	
	/* public function create
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	
	public function create() {
	      
	      return View::make('admin.users.create');  
	}
	
	
	/* public function store
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	
	public function store() {
	      
	      $rules = array(
		     'name' => 'required',
		     'email' => 'required|email',
		     'password' => 'required|min:6',
		     'gender' => 'required',
		     'gender_search' => 'required',
		     'adres' => 'required',
	      );
	      
	      $names = array(
		     'name' => '"Заголовок"',
		     'email' => '"Почта"',
		     'password' => '"Пароль"',
		     'gender' => '"Пол"',
		     'gender_search' => '"Ищет пол"',
		     'adres' => '"Живет"',
	      );

	      
	      $validator = Validator::make($data = Input::all(), $rules);
	      $validator->setAttributeNames($names);
	      
	      if ($validator->fails())
	      {
		      return Redirect::back()->withErrors($validator)->withInput();
	      }
	      
	      
	      
	      $user = Sentry::createUser(array(
		     'email'    => Input::get('email'),
		     'password' => Input::get('password'),
		     'activated' => true,
	      ));
	  
	      $profile = new Profile;
	      $profile->user_id = $user->id;
	      $profile->name = Input::get('name');
	      $profile->city = Input::get('city');
	      $profile->adres = Input::get('adres');
	      $profile->telephone = Input::get('telephone');
	      $profile->gender = Input::get('gender');
	      $profile->gender_search = Input::get('gender_search');
	      $profile->lat = Input::get('lat');
	      $profile->lng = Input::get('lng');
	      $profile->save();
	      
	      
	       
	      $userGroup = Sentry::findGroupById(2);
	   
	      $user->addGroup($userGroup);
	      
	      return Redirect::route('admin.users.index')->with('success', 'Пользователь создан.');
	}
	
       
       /* public function destroy
	* @param $id
	*-----------------------------------
	*|
	*-----------------------------------
	*/
       
       public function destroy($id) {
	      
	      User::destroy($id);
	      Profile::where('user_id', $id)->delete();
	      Photo::where('user_id', $id)->delete();
	      Album::where('user_id', $id)->delete();
	      Article::where('user_id', $id)->delete();
	      Chat::where('user_from', $id)->delete();
	      Chat::where('user_to', $id)->delete();
	      ChatMessage::where('user_id', $id)->delete();
	      Comment::where('user_id', $id)->delete();
	      Friend::where('user_one', $id)->delete();
	      Friend::where('user_two', $id)->delete();
	      FriendReq::where('user_from', $id)->delete();
	      FriendReq::where('user_to', $id)->delete();
	      History::where('user_id', $id)->delete();
	      Page::where('user_id', $id)->delete();
	      
	      return Redirect::route('admin.users.index')->with('success', 'Пользователь удален.');
       }
       
       /* public function set_admin
	* @param $id
	*-----------------------------------
	*|
	*-----------------------------------
	*/
       
       public function set_admin() {
	      
	      $rules = array(
		     'email' => 'required|email',
	      );
	      
	      $names = array(
		     'email' => '"Почта пользователя"',
	      );
	      
	      $validator = Validator::make($data = Input::all(), $rules);
	      $validator->setAttributeNames($names);
	      
	      if ($validator->fails())
	      {
		     return Redirect::back()->withErrors($validator)->withInput();
	      }
	      
	      $user = User::where('email', $data['email'])->first();
	      
	      if(!$user)
	      {
		     return Redirect::back()->with('warning', 'Такого пользователя не существует.');
	      }
	      else
	      {
		     $userGroup = Sentry::findGroupById(2);
		     $user->removeGroup($userGroup);
		     
		     $adminGroup = Sentry::findGroupById(1);
		     $user->addGroup($adminGroup);
	      }
	      
	      return Redirect::back()->with('success', 'OK.');
	      
       }
	
	
	
	
	
	
	
	

}