<?php

class StarsController extends \BaseController {

	/**
	 * Display a listing of stars
	 *
	 * @return Response
	 */
	public function index()
	{
		$stars = Star::all();

		return View::make('stars.index', compact('stars'));
	}

	/**
	 * Show the form for creating a new star
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('stars.create');
	}

	/**
	 * Store a newly created star in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$today = new DateTime('today');
		
		$myuser = Sentry::getUser();
		$data = Input::all();
		$data['user_id'] = $myuser->id;
		
		$stars = Star::where('type', $data['type'])->where('type_id', $data['type_id'])->where('created_at', '>=', $today)->first();
		
		if(!$stars)
		{
			Star::create($data);
		}
		
	}

	/**
	 * Display the specified star.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($type, $type_id)
	{
		//$star = Star::findOrFail($type_id);
		$readonly = '';
		$myuser = Sentry::getUser();
		$rating = 0;
		
		$stars = Star::where('type', $type)->where('type_id', $type_id)->get();
		$stars_values = array();
		foreach($stars as $star)
		{
			$stars_values[] = $star->value;
		}
		$sum = array_sum($stars_values);
		$count = count($stars_values);
		if($count)
		{
			$rating = round($sum/$count);
		}
		
		
		
		switch($type)
		{
			case 'photo':
				
				$photo = Photo::find($type_id);
				
				if($photo->user_id == $myuser->id)
				{
					$readonly = 'readonly="readonly"';	
				}
				
			break;
		}
		
		
		
		return View::make('stars.show', compact('type', 'type_id', 'readonly', 'rating'));
	}

	/**
	 * Show the form for editing the specified star.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$star = Star::find($id);

		return View::make('stars.edit', compact('star'));
	}

	/**
	 * Update the specified star in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$star = Star::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Star::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$star->update($data);

		return Redirect::route('stars.index');
	}

	/**
	 * Remove the specified star from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Star::destroy($id);

		return Redirect::route('stars.index');
	}

}
