<?php

class UslugisController extends \BaseController {

	/**
	 * Display a listing of uslugis
	 *
	 * @return Response
	 */
	public function index()
	{
		$uslugis = Uslugi::all();

		return View::make('uslugis.index', compact('uslugis'));
	}

	/**
	 * Show the form for creating a new uslugi
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('uslugis.create');
	}

	/**
	 * Store a newly created uslugi in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Uslugi::$rules, Uslugi::$messages);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		Uslugi::create($data);

		return Redirect::route('admin.uslugis.index')->with('success', 'Услуга создана.');
	}

	/**
	 * Display the specified uslugi.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$uslugi = Uslugi::findOrFail($id);

		return View::make('uslugis.show', compact('uslugi'));
	}
	
	/*
	 * function view
	 * @param $slug
	 * ---------------------------------------
	 * | 
	 * ---------------------------------------
	 */
	
	public function view($slug) {
		
		 $uslugi = Uslugi::slug($slug);
		 
		 if (empty($uslugi->id))
		 {
			App::abort(404);
		 }
		//dd($uslugi);
		 
		 
		return View::make('uslugis.show', compact('uslugi'));
	}

	/**
	 * Show the form for editing the specified uslugi.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$uslugi = Uslugi::find($id);

		return View::make('uslugis.edit', compact('uslugi'));
	}

	/**
	 * Update the specified uslugi in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$uslugi = Uslugi::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Uslugi::$rules, Uslugi::$messages);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$uslugi->update($data);

		return Redirect::route('admin.uslugis.index')->with('success', 'Услуга обновлена.');
	}

	/**
	 * Remove the specified uslugi from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Uslugi::destroy($id);

		return Redirect::route('admin.uslugis.index')->with('success', 'Услуга удалена.');
	}
	
	/*
	 * function uslugiBlock
	 * @param
	 * ---------------------------------------
	 * | Разовые услуги и их стоимость
	 * ---------------------------------------
	 */
	
	public function uslugiBlock() {
		
		$uslugis = Uslugi::take(4)->get();

		return View::make('uslugis.uslugiBlock', compact('uslugis'));
	}
	
	
	/*
	 * function konsultantStilist
	 * @param $id
	 * ---------------------------------------
	 * | 
	 * ---------------------------------------
	 */
	
	public function konsultantStilist() {
		
		return View::make('uslugis.konsultantStilist');
	}
	
	
	/*
	 * function ronsultantPsiholog
	 * @param $id
	 * ---------------------------------------
	 * | 
	 * ---------------------------------------
	 */
	
	public function konsultantPsiholog() {
		
		if (Sentry::check()){
			$user = Sentry::getUser();
		
			if($user->profile->vip == 1 || $user->profile->onepay == 1)
			{
				return View::make('uslugis.konsultantPsihologPayed');
			}
			else
			{
				return View::make('uslugis.konsultantPsiholog');
			}
		}
		else
		{
			return View::make('uslugis.konsultantPsiholog');
		}
		
		
		
		
	}
	

}
