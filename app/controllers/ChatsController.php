<?php

use Jenssegers\Date\Date;

class ChatsController extends \BaseController {

	/**
	 * Display a listing of chats
	 *
	 * @return Response
	 */
	public function index()
	{
		$chats = Chat::all();

		return View::make('chats.index', compact('chats'));
	}

	/**
	 * Show the form for creating a new chat
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('chats.create');
	}

	/**
	 * Store a newly created chat in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Chat::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		Chat::create($data);

		return Redirect::route('chats.index');
	}

	/**
	 * Display the specified chat.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$chat = Chat::findOrFail($id);

		return View::make('chats.show', compact('chat'));
	}

	/**
	 * Show the form for editing the specified chat.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$chat = Chat::find($id);

		return View::make('chats.edit', compact('chat'));
	}

	/**
	 * Update the specified chat in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$chat = Chat::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Chat::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$chat->update($data);

		return Redirect::route('chats.index');
	}

	/**
	 * Remove the specified chat from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Chat::destroy($id);

		return Redirect::route('chats.index');
	}
	
	
	/*
	 * function loadChat
	 * @param
	 * ---------------------------------------
	 * | Загрузка чата на карту
	 * ---------------------------------------
	 */
	
	public function loadChat() {
	      
	      $user1 = Sentry::getUser();
	      
	      $user2 = Sentry::findUserById(Input::get('id'));
	      
	      
	      $chat = Chat::whereRaw('(user_from = '.$user1->id.' AND user_to = '.$user2->id.') OR (user_from = '.$user2->id.' AND user_to = '.$user1->id.')')->first();
	      
	      
	      if(!$chat)
	      {
		$chat = Chat::create(['user_from' => $user1->id, 'user_to' => $user2->id, 'flags' => 0]);
	      }
	      
	      return Response::json([ 'users' => View::make('chats.chatUser', compact('user1', 'user2'))->render(),
				      'forma' => View::make('chats.forma', compact('chat'))->render(),
				     ]);
	      
	}
	
	/*
	 * function sendMessageAjax
	 * @param 
	 * ---------------------------------------
	 * | Добавляем запись в чат
	 * ---------------------------------------
	 */
	
	public function sendMessageAjax() {
		
		$data = Input::all();
		
		$chat = ChatMessage::create($data);
		
		return Response::json(['chat' => View::make('chats.chatMe', compact('chat'))->render() ]);
		
		
	}
	
	/*
	 * function loadMessageAjax
	 * @param $id
	 * ---------------------------------------
	 * | 
	 * ---------------------------------------
	 */
	
	public function loadMessageAjax($id) {
		
		$user = Sentry::getUser();
		
		$chats = ChatMessage::where('chat_id', $id)->get();
		
		$updateChats = ChatMessage::where('chat_id', $id)->where('user_id', '<>', $user->id)->get();
		
		foreach($updateChats as $updateChat)
		{
			$updateChat->flags = 0;
			$updateChat->save();
		}
		
		return Response::json(['chat' => View::make('chats.loadMessage', compact('chats'))->render() ]);
		
	}
	
	/*
	 * function messages
	 * @param $id
	 * ---------------------------------------
	 * | Сообщения пользователя
	 * ---------------------------------------
	 */
	
	public function messages($id) {
		
	      $user = Sentry::findUserById($id);
	      
	      $chats = Chat::where('user_from', $user->id)->orWhere('user_to', $user->id)->get();
	      
	      if(Sentry::getUser()->id != $id)
	      {
		     App::abort(404);
	      }
	      
	      
		
	      return View::make('profiles.my.messages', compact('user', 'chats'));
	}
	
	
	/*
	 * function sendME
	 * @param $id
	 * ---------------------------------------
	 * | 
	 * ---------------------------------------
	 */
	
	public function sendME() {
		
		$user_from = Sentry::getUser()->id;
		$user_to = Input::get('user_to');
		
		$chat = Chat::whereRaw('(user_from = '.$user_from.' AND user_to = '.$user_to.') OR (user_from = '.$user_to.' AND user_to = '.$user_from.')')->first();
	      
	      
		if(!$chat)
		{
		  $chat = Chat::create(['user_from' => $user_from, 'user_to' => $user_to, 'flags' => 0]);
		}
		
		return Response::json(['chat' => $chat->id ], 200);
	}
	
	

}
