<?php

class HistoriesController extends \BaseController {

	/**
	 * Display a listing of histories
	 *
	 * @return Response
	 */
	public function index()
	{
		$histories = History::orderBy('updated_at', 'desc')->paginate(10);

		return View::make('histories.index', compact('histories'));
	}
	
	
	/*
	 * function histories
	 * @param $id
	 * ---------------------------------------
	 * | 
	 * ---------------------------------------
	 */
	
	public function histories() {
		
		$histories = History::orderBy('updated_at', 'desc')->paginate(10);

		return View::make('histories.histories', compact('histories'));
	}

	/**
	 * Show the form for creating a new history
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('histories.create');
	}

	/**
	 * Store a newly created history in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), History::$rules, History::$messages);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		History::create($data);

		return Redirect::route('admin.histories.index')->with('success', 'История создана.');
	}

	/**
	 * Display the specified history.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($slug)
	{
		$history = History::slug($slug);
		
		if (empty($history->id))
		{
			App::abort(404);
		}
		
		Event::fire('show.history', array($history));

		return View::make('histories.show', compact('history'));
	}

	/**
	 * Show the form for editing the specified history.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$history = History::find($id);

		return View::make('histories.edit', compact('history'));
	}

	/**
	 * Update the specified history in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$history = History::findOrFail($id);

		$validator = Validator::make($data = Input::all(), History::$rules, History::$messages);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$history->update($data);

		return Redirect::route('admin.histories.index')->with('success', 'История обновлена.');
	}

	/**
	 * Remove the specified history from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		History::destroy($id);

		return Redirect::route('admin.histories.index')->with('success', 'История удалена.');
	}

}
