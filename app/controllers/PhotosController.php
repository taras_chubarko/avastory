<?php

class PhotosController extends \BaseController {

	/**
	 * Display a listing of photos
	 *
	 * @return Response
	 */
	public function index()
	{
		$photos = Photo::all();

		return View::make('photos.index', compact('photos'));
	}
	
	/*
	 * function album
	 * @param $id
	 * ---------------------------------------
	 * | Фотоальбом
	 * ---------------------------------------
	 */
	
	public function album($id, $slug) {
		
		$user = Sentry::findUserById($id);
		
		$album = Album::where('user_id', $id)->where('slug', $slug)->first();
		
		$photos = Photo::where('user_id', $id)->where('album_id', $album->id)->get();
		
		if(Sentry::getUser()->id == $id || Sentry::getUser()->hasAccess('users.edit'))
		{
		       return View::make('profiles.my.photoAlbum', compact('user', 'album', 'photos'));  
		}
		else
		{
		       return View::make('profiles.any.photoAlbum', compact('user', 'album', 'photos'));  
		}
	}

	/**
	 * Show the form for creating a new photo
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('photos.create');
	}

	/**
	 * Store a newly created photo in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Photo::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		Photo::create($data);

		return Redirect::route('photos.index');
	}

	/**
	 * Display the specified photo.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$photo = Photo::findOrFail($id);
		
		$prev = '';
		$next = '';
		
		$prev = Photo::where('album_id', $photo->album_id)->where('id', '<', $id)->orderBy('id', 'desc')->first();
		$next = Photo::where('album_id', $photo->album_id)->where('id', '>', $id)->orderBy('id', 'asc')->first();
		
	        return Response::json(['photo' => View::make('photos.show', compact('photo', 'prev', 'next'))->render()], 200);
	}

	/**
	 * Show the form for editing the specified photo.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$photo = Photo::find($id);

		return View::make('photos.edit', compact('photo'));
		
	}

	/**
	 * Update the specified photo in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$photo = Photo::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Photo::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$photo->update($data);

		return Redirect::route('photos.index');
	}

	/**
	 * Remove the specified photo from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Photo::destroy($id);

		return Redirect::route('photos.index');
	}
	
	/* print '<pre>' . htmlspecialchars(print_r($input, true)) . '</pre>';
	 * function nextprev
	 * @param $id
	 *-----------------------------------
	 *| Прокрутка фото
	 *-----------------------------------
	 */
	
	function nextprev($id) {
		
		$photo = Photo::findOrFail($id);
		
		$prev = '';
		$next = '';
		
		$prev = Photo::where('album_id', $photo->album_id)->where('id', '<', $id)->orderBy('id', 'desc')->first();
		$next = Photo::where('album_id', $photo->album_id)->where('id', '>', $id)->orderBy('id', 'asc')->first();

	        return Response::json(['photo' => View::make('photos.photo', compact('photo', 'prev', 'next'))->render(),
				        'comments' => View::make('photos.comment', compact('photo'))->render(),
				       ], 200);
		
	}

}
