<?php

class PagesController extends \BaseController {

	/**
	 * Display a listing of pages
	 *
	 * @return Response
	 */
	public function index()
	{
		$pages = Page::all();

		return View::make('pages.index', compact('pages'));
	}

	/**
	 * Show the form for creating a new page
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('pages.create');
	}

	/**
	 * Store a newly created page in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Page::$rules, Page::$messages);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		Page::create($data);

		return Redirect::route('admin.pages.index')->with('success', 'Страница сохранена.');
	}

	/**
	 * Display the specified page.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$page = Page::findOrFail($id);
		
		return View::make('pages.show', compact('page'));
	}
	
	/*
	 * function view
	 * @param $slug
	 * ---------------------------------------
	 * | 
	 * ---------------------------------------
	 */
	
	public function view($slug) {
		
		 $page = Page::where('slug', '=', $slug)->first();
		 
		 if ( is_null($page)  || $page->status == 0 )
		 {
			App::abort(404);
		 }
		 
		 ob_start();
		print eval('?>' . $page->body);
		$body = ob_get_contents();
		ob_end_clean();
		 
		 return View::make('pages.show', compact('page', 'body'));
	}

	/**
	 * Show the form for editing the specified page.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$page = Page::find($id);

		return View::make('pages.edit', compact('page'));
	}

	/**
	 * Update the specified page in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$page = Page::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Page::$rules, Page::$messages);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$page->update($data);

		return Redirect::route('admin.pages.index')->with('success', 'Страница обновлена.');
	}

	/**
	 * Remove the specified page from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Page::destroy($id);

		return Redirect::route('admin.pages.index')->with('success', 'Страница удалена.');;
	}
	
	/*
	 * function irinaCarousel
	 * @param
	 * ---------------------------------------
	 * | 
	 * ---------------------------------------
	 */
	
	public function irinaCarousel() {
		
		return View::make('site.carouseIrina');
	}

}
