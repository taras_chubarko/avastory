<?php

class FileController extends BaseController {
    
    
    /*
     * function uploadAvatar
     * @param $id
     * ---------------------------------------
     * | Загрузка аватара
     * ---------------------------------------
     */
    
    public function uploadAvatar($id) {
       
         $destinationPath = public_path().'/uploads/users/avatar/';
         
        if(Input::hasFile('avatar')){
 
        $file = Input::file('avatar'); 

	    $filename = Transliteration::clean_filename($file->getClientOriginalName());
	    if (File::exists($destinationPath.$filename))
	    {
		$fexplode = explode('.'.$file->getClientOriginalExtension(), $filename);
		$filename = $fexplode[0].'_'.mt_rand (). '.' . $file->getClientOriginalExtension();
		$uploadSuccess = Input::file('avatar')->move($destinationPath, $filename);
	    }
	    else
	    {
		$uploadSuccess = Input::file('avatar')->move($destinationPath, $filename);
	    }
	    
            if( $uploadSuccess )
	    {
               $profile = Profile::find($id);
               $profile->avatar = $filename;
               $profile->save();
	       
	       $imgBig = '<img src="'.route('user.avatar.load', array($filename, 200, 200)).'" alt="'.$profile->name.'" class="ava-big img-circle">';
	       $imgSmal = '<img src="'.route('user.avatar.load', array($filename, 50, 50)).'" alt="'.$profile->name.'" class="ava-smal">';
	      
               
               return Response::json(['success' => true, 'file' => $filename, 'imgBig' => $imgBig, 'imgSmal' => $imgSmal], 200);
           
           } else {
           
               return Response::json(['error' => 'Ошибка загрузки файла'], 400);
           }
            
        }
        
    }
    
    
    /*
     * function avatar
     * @param $id
     * ---------------------------------------
     * | Аватар
     * ---------------------------------------
     */
    
    public function avatar($id, $w=100, $h=100) {
        $user = Sentry::findUserById($id);
        $name = $user->profile->avatar;
	
	if(empty($user->profile->avatar))
	{
	  switch ($user->profile->gender)
	  {
	       case 1:
		    $name = 'noava2.png';
	       break;
	  
	       case 2:
		    $name = 'noava1.png';
	       break;    
	  }
	}
	
        $caheimage = Image::cache(function($image) use ($name, $w, $h){
        return $image->make('uploads/users/avatar/'.$name)->fit($w, $h);
        }, 10, true);
    
        return $caheimage->response('jpg');

    }
    
    public function avatar_load($name, $w=100, $h=100) {
      
        $caheimage = Image::cache(function($image) use ($name, $w, $h){
        return $image->make('uploads/users/avatar/'.$name)->fit($w, $h);
        }, 10, true);
      
        return $caheimage->response('jpg');

    }
    
    /*
     * function uploadFon
     * @param $id
     * ---------------------------------------
     * | Загрузка фона
     * ---------------------------------------
     */
    
    public function uploadFon($id) {
       
         $destinationPath = public_path().'/uploads/users/fon/';
         
        if(Input::hasFile('fon')){
 
        $file = Input::file('fon'); 

	    $filename = Transliteration::clean_filename($file->getClientOriginalName());
	    if (File::exists($destinationPath.$filename))
	    {
		$fexplode = explode('.'.$file->getClientOriginalExtension(), $filename);
		$filename = $fexplode[0].'_'.mt_rand (). '.' . $file->getClientOriginalExtension();
		$uploadSuccess = Input::file('fon')->move($destinationPath, $filename);
	    }
	    else
	    {
		$uploadSuccess = Input::file('fon')->move($destinationPath, $filename);
	    }
	    
            if( $uploadSuccess )
	    {
               $profile = Profile::find($id);
               $profile->fon = $filename;
               $profile->save();
	       
               return Response::json(['success' => true, 'file' => $filename], 200);
           
           } else {
           
               return Response::json(['error' => 'Ошибка загрузки файла'], 400);
           }
            
        }
        
    }
    
    /*
     * function filemanager
     * @param 
     * ---------------------------------------
     * | 
     * ---------------------------------------
     */
    
    public function filemanager($name, $w, $h, $position) {
        
	$caheimage = Image::cache(function($image) use ($name, $w, $h, $position){
        return $image->make('filemanager/userfiles/'.$name)->fit($w, $h, null, $position);
        }, 10, true);
      
        return $caheimage->response('jpg');
     
    }
    
    
    /*
     * function uploadUslugiImage
     * @param $id
     * ---------------------------------------
     * | Загрузка картинки в услуги
     * ---------------------------------------
     */
    
    public function uploadImage($folder) {
       
         $destinationPath = public_path().'/uploads/'.$folder.'/';
         
//        if(Input::hasFile('image_ajax')){
//      
//        $file = Input::file('image_ajax'); 
//       
//	    $filename = Transliteration::clean_filename($file->getClientOriginalName());
//	    if (File::exists($destinationPath.$filename))
//	    {
//		$fexplode = explode('.'.$file->getClientOriginalExtension(), $filename);
//		$filename = $fexplode[0].'_'.mt_rand (). '.' . $file->getClientOriginalExtension();
//		$uploadSuccess = Input::file('image_ajax')->move($destinationPath, $filename);
//	    }
//	    else
//	    {
//		$uploadSuccess = Input::file('image_ajax')->move($destinationPath, $filename);
//	    }
//	    
//            if( $uploadSuccess )
//	    {
//	       $img = '<img src="'.route('image.folder', array($folder, $filename, 100, 100, 'center')).'" alt="..." class="img-thumbnail">';
//               return Response::json(['success' => true, 'file' => $filename, 'img' => $img ], 200);
//           
//           } else {
//           
//               return Response::json(['error' => 'Ошибка загрузки файла'], 400);
//           }
//            
//        }

        if(Input::hasFile('file_data')){
      
        $file = Input::file('file_data'); 
       
	    $filename = Transliteration::clean_filename($file->getClientOriginalName());
	    if (File::exists($destinationPath.$filename))
	    {
		$fexplode = explode('.'.$file->getClientOriginalExtension(), $filename);
		$filename = $fexplode[0].'_'.mt_rand (). '.' . $file->getClientOriginalExtension();
		$uploadSuccess = Input::file('file_data')->move($destinationPath, $filename);
	    }
	    else
	    {
		$uploadSuccess = Input::file('file_data')->move($destinationPath, $filename);
	    }
	    
            if( $uploadSuccess )
	    {
               return Response::json(['result' => View::make('site.upload-image', compact('folder', 'filename'))->render()], 200);
           
           } else {
           
               return Response::json(['error' => 'Ошибка загрузки файла'], 400);
           }
            
        }
        
    }
    
    public function imageFolder($folder, $name, $w, $h, $position) {
        
	$caheimage = Image::cache(function($image) use ($folder, $name, $w, $h, $position){
        return $image->make('uploads/'.$folder.'/'.$name)->fit($w, $h, null, $position);
        }, 10, true);
      
        return $caheimage->response('jpg');
    }
    
    /*
     * function uploadPhotos
     * @param $id
     * ---------------------------------------
     * | 
     * ---------------------------------------
     */
    
    public function uploadPhotos() {
	  $destinationPath = public_path().'/uploads/photos/';
         
	  if(Input::hasFile('upload_photo')){
      
	  $files = Input::file('upload_photo');
	  
	  foreach($files as $key => $file)
	  {
	       
     
	      $filename = Transliteration::clean_filename($file->getClientOriginalName());
	       if (File::exists($destinationPath.$filename))
	       {
		    $fexplode = explode('.'.$file->getClientOriginalExtension(), $filename);
		    $filename = $fexplode[0].'_'.mt_rand (). '.' . $file->getClientOriginalExtension();
		    $uploadSuccess = $files[$key]->move($destinationPath, $filename);
		    
		    $photo = new Photo;
		    $photo->user_id = Input::get('user_id');
		    $photo->album_id = Input::get('album_id');
		    $photo->photo = $filename;
		    $photo->save();
	       }
	       else
	       {
		    $uploadSuccess = $files[$key]->move($destinationPath, $filename);
	       }
	       
	       if($uploadSuccess)
	       {
		    $filenames[] = $filename;
	       }
	  }
	  
	  return Response::json(['file' => $filenames ], 200);

        }
    }
    
    
    
    
    
}