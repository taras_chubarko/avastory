<?php

//print '<pre>' . htmlspecialchars(print_r($article, true)) . '</pre>';
/*------------------------------------------------------
 * Событие просмотра сатьи
 *------------------------------------------------------
 */

Event::listen('show.article', function  ($article) {
    
    $today = new DateTime('today');
		
    $ip = Request::getClientIp();
		
    $look = Look::where('type', 'article')->where('type_id', $article->id)->where('ip', $ip)->where('created_at', '>=', $today)->first();
		
    if(!$look)
    {
        $lk = new Look;
        $lk->type    = 'article';
        $lk->type_id = $article->id;
        $lk->ip      = $ip;
        $lk->save();
    }
});

/*------------------------------------------------------
 * Событие просмотра истории пар
 *------------------------------------------------------
 */

Event::listen('show.history', function  ($article) {
    
    $today = new DateTime('today');
		
    $ip = Request::getClientIp();
		
    $look = Look::where('type', 'history')->where('type_id', $article->id)->where('ip', $ip)->where('created_at', '>=', $today)->first();
		
    if(!$look)
    {
        $lk = new Look;
        $lk->type    = 'history';
        $lk->type_id = $article->id;
        $lk->ip      = $ip;
        $lk->save();
    }
});

