<?php

class Nav {
    
    public static function item($route, $label='', $class='', $params=array(), $a=array()) {
        
        $class .= (Route::getCurrentRoute()->getAction()['as']==$route) ? ' active' : '';
        
        $tag = $class ? '<li class="'.trim($class).'">' : '<li>';
        
        $append = isset($a['append']) ? $a['append'].'&ensp;' : '';
        $prepend = isset($a['prepend']) ? '&ensp;'.$a['prepend'] : '';
        
        //return $tag.HTML::link(URL::route($route, $params),$label).'</li>';
        return $tag.'<a href="'.route($route, $params).'">'. $append . $label . $prepend .'</a>';
    }
  
  
  
  
  
}
