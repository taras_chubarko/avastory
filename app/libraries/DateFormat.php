<?php

//namespace Date;

class DateFormat{
    
    /*
     * function textDate
     * @param $id
     * ---------------------------------------
     * | 1 Февраля 2015 г.
     * ---------------------------------------
     */
    
    public static function textDate($date) {
        
        $timestamp = strtotime($date);
        
        $out = date('d F Y г.', $timestamp);
        
        return strtr($out, trans('date.month_declensions'));
        
    }
    
    
    /*
     * function godText
     * @param $num
     * ---------------------------------------
     * | 'год', 'года', 'лет'
     * ---------------------------------------
     */
    
    public static function num2word($num, $words) {
        
        $num = $num % 100;
        if ($num > 19) {
            $num = $num % 10;
        }
        switch ($num) {
            case 1: {
                return($words[0]);
            }
            case 2: case 3: case 4: {
                return($words[1]);
            }
            default: {
                return($words[2]);
            }
        }
        
    }
    
    

    
}


