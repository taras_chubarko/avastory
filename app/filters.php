<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function($request)
{
	//
});


App::after(function($request, $response)
{
	//
});

Route::filter('auth', function()
{
	if (!Sentry::check()) return Redirect::guest('/');
});

Route::filter('admin', function()
{
    $user = Sentry::getUser();
    $admin = Sentry::findGroupByName('Администратор');
    if (!$user->inGroup($admin))
    {
    	return View::make('messages.403');
    }
});

Route::filter('mylk', function()
{
    $user = Sentry::getUser();
    $segment = Request::segment(2);
    
    if ( $segment != $user->id)
    {
    	return View::make('messages.403');
    }
	
});


/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

//Route::filter('auth', function()
//{
//	if (Auth::guest())
//	{
//		if (Request::ajax())
//		{
//			return Response::make('Unauthorized', 401);
//		}
//		else
//		{
//			return Redirect::guest('login');
//		}
//	}
//});
//
//
//Route::filter('auth.basic', function()
//{
//	return Auth::basic();
//});

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest', function()
{
	if (Sentry::check())
	{
		// Logged in successfully - redirect based on type of user
	    $user = Sentry::getUser();
	    $admin = Sentry::findGroupByName('Администратор');
	    $users = Sentry::findGroupByName('Пользователь');
	    if ($user->inGroup($admin)) return Redirect::intended('admin');
	    elseif ($user->inGroup($users)) return Redirect::intended('/');
	}
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

//Route::filter('csrf', function()
//{
//	$token = Request::ajax() ? Request::header('X-CSRF-Token') : Input::get('_token');
//	
//	if (Session::token() != $token)
//	{
//		throw new Illuminate\Session\TokenMismatchException;
//	}
//});
//Route::filter('csrf', function()
//{
//	if (Session::token() != Input::get('_token'))
//	{
//		throw new Illuminate\Session\TokenMismatchException;
//	}
//});
Route::filter('csrf', function()
{
  if (Request::getMethod() !== 'POST' && Session::token() != Input::get('_token'))
  {
      throw new Illuminate\Session\TokenMismatchException;
  }
});
