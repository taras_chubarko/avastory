<?php

return array(
             
             'site_name' => 'Avastory.ru',
             
             'sort' => array(
                   -10 => -10,
                    -9 => -9,
                    -8 => -8,
                    -7 => -7,
                    -6 => -6,
                    -5 => -5,
                    -4 => -4,
                    -3 => -3,
                    -2 => -2,
                    -1 => -1,
                     0 => 0,
                     1 => 1,
                     2 => 2,
                     3 => 3,
                     4 => 4,
                     5 => 5,
                     6 => 6,
                     7 => 7,
                     8 => 8,
                     9 => 9,
                    10 => 10,
                ),
             
             'gender' => array(
                    1 => 'Мужской',
                    2 => 'Женский',
              ),
             
             'gender_search' => array(
                    1 => 'Мужчину',
                    2 => 'Женщину',
              ),
             
             );