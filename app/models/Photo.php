<?php

class Photo extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
		// 'title' => 'required'
	];

	// Don't forget to fill this array
	protected $fillable = [];
        
        public function likes()
	{
	    return $this->hasMany('Like', 'type_id', 'id')->where('type', 'photo');
	}
        
        public function comments()
	{
	    return $this->hasMany('Comment', 'type_id', 'id')->where('type', 'photo');
	}
     
}