<?php

class Album extends \Eloquent {

	protected $fillable = [];
        
        protected $guarded = ['_token'];
        
        public static $sluggable = array(
		'build_from' => 'name',
		'save_to'    => 'slug',
	);
        
        public static $rules = array(
		'name'     => 'required|min:2',
	);
	
	public static $messages = array(
               
	       'name.required'        => 'Поле "Название альбома" обязательно для заполнения',
	       'name.min'             => 'Количество символов в поле "Заголовок" должно быть не менее 2.',
        );
        
        public function photo()
	{
	    return $this->hasOne('Photo', 'album_id', 'id');
	}
        
        public function photos()
	{
	    return $this->hasMany('Photo');
	}

}