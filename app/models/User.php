<?php

//use Illuminate\Auth\UserTrait;
//use Illuminate\Auth\UserInterface;
//use Illuminate\Auth\Reminders\RemindableTrait;
//use Illuminate\Auth\Reminders\RemindableInterface;
//use Cartalyst\Sentry\Users\Eloquent\User as SentryUserModel;
use Cartalyst\Sentry\Users\Eloquent\User as SentryModel;
use Jenssegers\Date\Date;
//class User extends SentryUserModel implements UserInterface, RemindableInterface {
class User extends SentryModel {

	//use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	//protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');
	
	public static $rules = array(
		'email' => 'required|email',
		'password' => 'required|min:6|confirmed',
		'password_confirmation' => 'required|min:6',
		'name' => 'required',
		'gender' => 'required',
		'gender_search' => 'required',
		'adres' => 'required',
		'telephone' => 'required',
		//'pravila' => 'required',
	);
	
	public static $messages = array(
               
	       'email.required'                 => 'Поле "Почта" обязательно для заполнения',
	       'password.required'              => 'Поле "Пароль" обязательно для заполнения',
	       'password_confirmation.required' => 'Поле "Повтор пароля" обязательно для заполнения',
	       'password.confirmed'             => 'Поле "Повтор пароля" несовпадает',
	       'name.required'                  => 'Поле "Имя" обязательно для заполнения',
	       
        );
	
	public function profile()
	{
	    return $this->hasOne('Profile','user_id');
	}
	
	   
	public function albums()
	{
	    return $this->hasMany('Album','user_id');
	}
	
	public function photos()
	{
	    return $this->hasMany('Photo');
	}
	
	public function friendsReq()
	{
	    return $this->hasMany('FriendReq', 'user_to');
	}
	
	public function isOnline($id)
	{
	    
	    $online = Online::where('user_id', $id)->first();
	    
	    if($online)
	    {
		return true;
	    }
	    else
	    {
		return false;
	    }
	   
	}
	
	/* public function status
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	
	public function status($id) {
	    
	    if($id == 1)
	    {
		return 'активный';
	    }
	    else
	    {
		return 'заблокирован';
	    }
	    
	    
	}
	
	
	
	
	

}
