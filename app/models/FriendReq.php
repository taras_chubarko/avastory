<?php

class FriendReq extends \Eloquent {
	
	protected $fillable = [];
	
	
	protected $table = 'friends_req';
	
	public function profile()
	{
	    return $this->hasOne('Profile','user_id', 'user_from');
	}
	
	
}