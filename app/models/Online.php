<?php

class Online extends \Eloquent {
	protected $fillable = [];
	
	public $table = 'sessions';
	
	public $timestamps = false;
	
	public function scopeGuests($query)
	{
	    return $query->whereNull('user_id');
	}
	
	public function scopeRegistered($query)
	{
	    return $query->whereNotNull('user_id')->with('user');
	}
	
	public function scopeUpdateCurrent($query)
	{
	    return $query->where('id', Session::getId())->update(array(
		'user_id' => Sentry::check() ? Sentry::getUser()->id : null
	    ));
	}
	
	public function user()
	{
	    //return $this->belongsTo('Cartalyst\Sentry\Users\EloquentUser'); # Sentry 3
	    // return $this->belongsTo('Cartalyst\Sentry\Users\Eloquent\User'); # Sentry 2
	    return $this->belongsTo('User');
	}
	
	
}