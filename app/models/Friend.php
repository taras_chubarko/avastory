<?php

class Friend extends \Eloquent {
	
	protected $fillable = [];
	
	//public $timestamps = false;
	
	protected $guarded = [];
	
	//protected $primaryKey = 'user_id';
	public $incrementing = false;
	
	
	/*
	 * function friendButton
	 * @param $id
	 * ---------------------------------------
	 * | Кнопка добавить в друзья
	 * ---------------------------------------
	 */
	
	public static function friendButton($id) {
	      
	      $button = '';
	      
	      $myuser = Sentry::getUser();
	      
	      $friend = Friend::whereRaw('(user_one='.$myuser->id.' AND user_two='.$id.') OR (user_one='.$id.' AND user_two='.$myuser->id.')')->count();
	      
	      if($friend == 1)
	      {
		 $button = '<a class="btn btn-sm btn-info btn-rounded" href="javascript:">Этот пользователь ваш друг</a>';
	      }
	      else
	      {
		 $fromReq = FriendReq::where('user_from', $id)->where('user_to', $myuser->id)->count();
		 $toReq = FriendReq::where('user_from', $myuser->id)->where('user_to', $id)->count();
		 
		 if($fromReq == 1)
		 {
			$button = '<a class="btn btn-sm btn-info btn-rounded" href="'.route('friends.apply', $id).'">Принять приглашение в друзья</a>';
		 }
		 elseif($toReq == 1)
		 {
			$button = '<a class="btn btn-sm btn-warning btn-rounded" href="'.route('friends.cencel', $id).'">Отменить приглашение</a>';
		 }
		 else
		 {
			$button = '<a class="btn btn-sm btn-success btn-rounded" href="'. route('friends.add', $id). '">Пригласить в друзья</a>';
		 }
		 
	      }
	      
	      return $button;
	}
	
	
	public function scopeFriends($query, $user)
	{
	  return $query->where('user_one', $user->id)->orWhere('user_two', $user->id)->get();
	}
	
	public function profile()
	{
	    return $this->hasOne('Profile','user_id', 'user_two');
	}
	
	
}