<?php

//print '<pre>' . htmlspecialchars(print_r($count, true)) . '</pre>';
class Chat extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
		// 'title' => 'required'
	];

	// Don't forget to fill this array
	protected $fillable = [];
        
        protected $guarded = ['_token'];
        
        public function userFrom()
	{
	    return $this->hasOne('Profile', 'user_id', 'user_from');
	}
        
        public function userTo()
	{
	    return $this->hasOne('Profile', 'user_id', 'user_to');
	}
        
        
        /*
	 * function countAllNew
	 * @param 
	 * ---------------------------------------
	 * | Все новые сообщения для пользователя
	 * ---------------------------------------
	 */
	
	public static function countAllNew() {
		
                $user = Sentry::getUser();
		
		$chats = Chat::where('user_from', $user->id)->orWhere('user_to', $user->id)->get();
                
		$chatId = array();
		
                foreach($chats as $chat)
                {
                    $chatId[] = $chat->id;
                }
                
               $count = ChatMessage::whereIn('chat_id', $chatId)->where('flags', 1)->where('user_id', '<>', $user->id)->count();
		
                return $count;
	}
        
        
        /*
         * function countChatNew
         * @param $id
         * ---------------------------------------
         * | Все новые сообщения в переписке
         * ---------------------------------------
         */
        
        public static function countChatNew($id) {
            
            $user = Sentry::getUser();
            
            $count = ChatMessage::where('chat_id', $id)->where('flags', 1)->where('user_id', '<>', $user->id)->count();
            
            return $count;
        }
        
        /*
         * function lastNewMessages
         * @param 
         * ---------------------------------------
         * | Последние новые сообщения пользователю
         * ---------------------------------------
         */
        
        public static function lastNewMessages() {
            
            $user = Sentry::getUser();
		
	    $chats = Chat::where('user_from', $user->id)->orWhere('user_to', $user->id)->get();
               
	    $chatId = array();  
	        
            foreach($chats as $chat)
            {
                $chatId[] = $chat->id;
            }
            
	    $сhatss = array();  
	    
            $сhatGroups = ChatMessage::whereIn('chat_id', $chatId)->where('flags', 1)->where('user_id', '<>', $user->id)->groupBy('chat_id')->get(array('chat_id'));
            
            foreach($сhatGroups as $сhatGroup)
            {
                $сhatss[] =  ChatMessage::where('chat_id', $сhatGroup->chat_id)->orderBy('created_at', 'desc')->first();
            }
            
            //print '<pre>' . htmlspecialchars(print_r($сhatss, true)) . '</pre>';
            return $сhatss;
        }
        
        
        
        
        
        

}