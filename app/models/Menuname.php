<?php

class Menuname extends \Eloquent {

	// Don't forget to fill this array
	protected $fillable = [];
        
        protected $table = 'menu';
        
        protected $guarded = ['_token'];
        
        public $timestamps = false;
        
        public static $rules = array(
	    'name'     => 'required|min:2',
	);
        
        public static $names = array(
	    'name'     => '"Название меню"',
	);

}