<?php

class Profile extends \Eloquent {
	
	protected $fillable = [];
	
	public $timestamps = false;
	
	protected $guarded = array('id');
	
	public function user()
	{
		return $this->belongsTo('User');
	}
	
	public function works()
	{
		$items = array();
		
		$works = json_decode($this->work, true);
		
		foreach($works as $key => $work)
		{
			$items[strtr($key, trans('site.work'))] = $work;
		}
		//print '<pre>' . htmlspecialchars(print_r($items, true)) . '</pre>';
		return $items;
	}
	
	
	public function worksArray()
	{
		
		$works = json_decode($this->work, true);
		
		return (object) $works;
	}
	
	public function vuzs()
	{
		$items = array();
		
		$vuzs = json_decode($this->vuz, true);
		
		foreach($vuzs as $key => $vuz)
		{
			$items[strtr($key, trans('site.vuz'))] = $vuz;
		}
		//print '<pre>' . htmlspecialchars(print_r($items, true)) . '</pre>';
		return $items;
	}
	
	
	public function vuzArray()
	{
		
		$vuzs = json_decode($this->vuz, true);
		
		return (object) $vuzs;
	}
	
	public function schools()
	{
		$items = array();
		
		$schools = json_decode($this->school, true);
		
		foreach($schools as $key => $school)
		{
			$items[strtr($key, trans('site.school'))] = $school;
		}
		//print '<pre>' . htmlspecialchars(print_r($items, true)) . '</pre>';
		return $items;
	}
	
	
	public function schoolArray()
	{
		
		$schools = json_decode($this->school, true);
		
		return (object) $schools;
	}
	
	public function photos()
	{
	    return $this->hasMany('Photo', 'user_id', 'user_id');
	}
	
	
	public function stars($id)
	{
	    //return $this->hasMany('Star','user_id');
	    
	    $rating = 0;
		
	    $photos = Photo::where('user_id', $id)->get();
	    $photosArr = array();
	    foreach($photos as $photo)
	    {
		$photosArr[] = $photo->id;
	    }
	    
	    $stars = Star::where('type', 'photo')->whereIn('type_id', $photosArr)->get();
	   
	    $stars_values = array();
	    foreach($stars as $star)
	    {
		    $stars_values[] = $star->value;
	    }
	    $sum = array_sum($stars_values);
	    $count = count($stars_values);
	    if($count)
	    {
		$rating = round($sum/$count);
	    }
	    
	    //print '<pre>' . htmlspecialchars(print_r($rating, true)) . '</pre>';
	    return $rating;
	}
	
	
}