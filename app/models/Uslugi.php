<?php

class Uslugi extends \Eloquent {
	
        // Don't forget to fill this array
	protected $fillable = [];
        
        protected $guarded = ['_token', 'image_ajax'];
        
        public static $sluggable = array(
		'build_from' => 'name',
		'save_to'    => 'slug',
	);
        
        public static $rules = array(
		'name'     => 'required|min:2',
		'body'      => 'required|min:10',
	);
	
	public static $messages = array(
               
	       'name.required'        => 'Поле "Заголовок" обязательно для заполнения',
	       'name.min'             => 'Количество символов в поле "Заголовок" должно быть не менее 2.',
	       'body.required'         => 'Поле "Содержание" обязательно для заполнения',
	       'body.min'              => 'Количество символов в поле "Содержание" должно быть не менее 10',
        );
        
        public function scopeSlug($query, $slug)
	{
	  return $query->whereSlug($slug)->first();
	}

}