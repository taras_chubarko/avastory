<?php
use Faker\Factory as Faker;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
Online::updateCurrent();

Route::when('*', 'csrf', ['POST', 'PUT', 'PATCH', 'DELETE']);

# Admin Routes
Route::group(['before' => 'auth|admin'], function()
{
        Route::group(array('prefix' => 'admin'), function()
        {
            //Route::get('users', array('as' => 'admin.users', 'uses' => 'UsersController@users'));
	    //Route::get('roles', array('as' => 'admin.roles', 'uses' => 'UsersController@roles'));
	    Route::resource('pages', 'PagesController');
	    Route::resource('uslugis', 'UslugisController');
	    Route::resource('articles', 'ArticlesController');
	    Route::resource('histories', 'HistoriesController');
	    Route::resource('albums', 'AlbumsController');
	    Route::resource('photos', 'PhotosController');
	    Route::resource('comments', 'CommentsController');
	    Route::resource('users', 'UsersController');
	    Route::resource('menu', 'MenunamesController');
	    Route::post('/set/admin', array('as' => 'admin.set.admin', 'uses' => 'UsersController@set_admin'));
        });
});

# Registration
Route::group(['before' => 'guest'], function()
{
     Route::post('/user/login', array('as' => 'user.login', 'uses' => 'UsersController@login'));
     Route::get('/user/register', array('as' => 'user.register', 'uses' => 'UsersController@register'));
     Route::post('/user/register', array('as' => 'user.register.post', 'uses' => 'UsersController@register_post'));
     Route::get('/user/activate/{activationCode}', array('as' => 'user.activate', 'uses' => 'UsersController@activate'));
     
	
});

# Авторизованый
Route::group(['before' => 'auth'], function()
{
	Route::get('/user/logout', array('as' => 'user.logout', 'uses' => 'UsersController@user_logout'));
	
	Route::get('/user/id-{id}', array('as' => 'user.id', 'uses' => 'UsersController@information'));
	Route::get('/user/id-{id}/settings', array('as' => 'user.settings', 'uses' => 'UsersController@settings'));
	Route::post('/user/id-{id}/settings', array('as' => 'user.settings.save', 'uses' => 'UsersController@settings_save'));
	Route::get('/user/id-{id}/information', array('as' => 'user.information', 'uses' => 'UsersController@information'));
	Route::post('/user/id-{id}/information/about/save', array('as' => 'user.information.about.save', 'uses' => 'UsersController@information_about_save'));
	Route::post('/user/id-{id}/information/thinking/save', array('as' => 'user.information.thinking.save', 'uses' => 'UsersController@information_thinking_save'));
	Route::post('/user/profile/save/name', array('as' => 'user.profile.save.name', 'uses' => 'UsersController@user_profile_save_name'));
	Route::post('/user/profile/save/telephone', array('as' => 'user.profile.save.telephone', 'uses' => 'UsersController@user_profile_save_telephone'));
	Route::post('/user/profile/save/city', array('as' => 'user.profile.save.city', 'uses' => 'UsersController@user_profile_save_city'));
	Route::post('/user/profile/save/bithday', array('as' => 'user.profile.save.bithday', 'uses' => 'UsersController@user_profile_save_bithday'));
	Route::post('/user/profile/save/gender', array('as' => 'user.profile.save.gender', 'uses' => 'UsersController@user_profile_save_gender'));
	Route::post('/user/profile/save/gender-search', array('as' => 'user.profile.save.gender.search', 'uses' => 'UsersController@user_profile_save_gender_search'));
	Route::post('/user/profile/save/point', array('as' => 'user.profile.save.point', 'uses' => 'UsersController@user_profile_save_point'));
	Route::post('/user/profile/save/work', array('as' => 'user.profile.save.work', 'uses' => 'UsersController@user_profile_save_work'));
	Route::post('/user/profile/save/vuz', array('as' => 'user.profile.save.vuz', 'uses' => 'UsersController@user_profile_save_vuz'));
	Route::post('/user/profile/save/school', array('as' => 'user.profile.save.school', 'uses' => 'UsersController@user_profile_save_school'));
	
	Route::get('/user/id-{id}/friends', array('as' => 'user.friends', 'uses' => 'UsersController@friends'));
	Route::get('/friends/add/{id}', array('as' => 'friends.add', 'uses' => 'UsersController@friendsAdd'));
	Route::get('/friends/apply/{id}', array('as' => 'friends.apply', 'uses' => 'UsersController@applyFriend'));
	Route::get('/friends/cencel/{id}', array('as' => 'friends.cencel', 'uses' => 'UsersController@cencelFriend'));
	Route::get('/friends/del/{id}', array('as' => 'friends.del', 'uses' => 'UsersController@delFriend'));
	
	Route::post('/upload/avatar/{id}', array('as' => 'upload.avatar', 'uses' => 'FileController@uploadAvatar'));
	Route::post('/upload/fon/{id}', array('as' => 'upload.fon', 'uses' => 'FileController@uploadFon'));
	Route::controller('filemanager', 'FilemanagerLaravelController');
	Route::post('/upload/{folder}', array('as' => 'upload.image', 'uses' => 'FileController@uploadImage'));
	Route::post('/upl/photos', array('as' => 'upload.photos', 'uses' => 'FileController@uploadPhotos'));
	
	Route::get('/profiles/all', array('as' => 'profiles', 'uses' => 'UsersController@profiles'));
	
	Route::get('/user/id-{id}/photos', array('as' => 'user.photos', 'uses' => 'AlbumsController@photos'));
	Route::get('/user/id-{id}/photos/album/{slug}', array('as' => 'user.album', 'uses' => 'PhotosController@album'));
	Route::resource('albums', 'AlbumsController');
	Route::resource('photos', 'PhotosController');
	Route::get('photos/{id}/nextprev', array('as' => 'photos.nextprev', 'uses' => 'PhotosController@nextprev'));
        
	Route::resource('comments', 'CommentsController');
	
	Route::get('/user/id-{id}/messages', array('as' => 'user.messages', 'uses' => 'ChatsController@messages'));
	Route::resource('histories', 'HistoriesController', array('only' => array('create', 'store')));
	Route::post('/chats/sendme/ajax', array('as' => 'chats.sendme.ajax', 'uses' => 'ChatsController@sendME'));
	
	Route::post('/pay/success', array('as' => 'pay.success', 'uses' => 'HomeController@success'));
	Route::post('/pay/fail', array('as' => 'pay.fail', 'uses' => 'HomeController@fail'));
	Route::post('/pay/pending', array('as' => 'pay.pending', 'uses' => 'HomeController@pending'));
	
	Route::post('/star/add', array('as' => 'star.add', 'uses' => 'StarsController@store'));
	
});

Route::get('/', array('as' => 'front', 'uses' => 'HomeController@showWelcome'));
Route::get('/image/users/{id}/{w}/{h}', array('as' => 'user.avatar', 'uses' => 'FileController@avatar'));
Route::get('/image/users/load/{name}/{w}/{h}', array('as' => 'user.avatar.load', 'uses' => 'FileController@avatar_load'));
Route::any('/{slug}', array('as' => 'pages.view', 'uses' => 'PagesController@view'));
Route::get('/image/filemanager/{name}/{w}/{h}/{position}', array('as' => 'image.filemanager', 'uses' => 'FileController@filemanager'));
Route::get('/image/{folder}/{name}/{w}/{h}/{position}', array('as' => 'image.folder', 'uses' => 'FileController@imageFolder'));

Route::get('/uslugi/{slug}', array('as' => 'uslugis.view', 'uses' => 'UslugisController@view'));
Route::get('/article/{slug}', array('as' => 'articles.show', 'uses' => 'ArticlesController@show'));
Route::get('/articles/all', array('as' => 'articles', 'uses' => 'ArticlesController@articles'));
Route::get('/history/{slug}', array('as' => 'histories.show', 'uses' => 'HistoriesController@show'));
Route::get('/histories/all', array('as' => 'histories', 'uses' => 'HistoriesController@histories'));

Route::get('/profiles/json', array('as' => 'profiles.json', 'uses' => 'UsersController@profilesJson'));

Route::get('/profiles/load/chat', array('as' => 'profiles.load.chat', 'uses' => 'ChatsController@loadChat'));
Route::post('/chats/send/message/ajax', array('as' => 'chats.send.message.ajax', 'uses' => 'ChatsController@sendMessageAjax'));
Route::post('/chats/load/{id}/ajax', array('as' => 'chats.load.message.ajax', 'uses' => 'ChatsController@loadMessageAjax'));
Route::post('/set/like', array('as' => 'set.like', 'uses' => 'StatisticsController@setLike'));

require app_path().'/menu.php';