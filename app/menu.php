<?php
HTML::macro('activeState', function($url)
{
    return Request::is($url) ? 'active' : '';
});