<div class="row row-sm">

<div class="col-lg-3 col-md-4 col-sm-6"></div>
<div class="col-lg-3 col-md-4 col-sm-6">
      <div class="panel b-a oneays">
        <div class="panel-heading text-center bg-info no-border">
          <h4 class="text-u-c m-b-none">Одна консультация</h4>
          <h2 class="m-t-none">
            <span class="text-2x text-lt">100</span>
            <span class="text-xs">руб.</span>
          </h2>
        </div>
        <div class="txt">
            Вы можете преобрести одну консультацию психолога, и воспользваться ей в течении <br>24 часов.
        </div>
        <div class="panel-footer text-center">
        
        @if(Sentry::check())
        <form id="payment" name="payment" method="post" action="https://sci.interkassa.com/" enctype="utf-8">
            <input type="hidden" name="ik_co_id" value="5586d1133b1eaf0e0a8b4568" />
            <input type="hidden" name="ik_pm_no" value="ID_4233" />
            <input type="hidden" name="ik_am" value="100.00" />
            <input type="hidden" name="ik_cur" value="RUB" />
            <input type="hidden" name="ik_desc" value="Одна консультация 100 руб." />
            <input type="hidden" name="ik_x_type" value="onepay" />
            <input class="btn btn-info font-bold m" type="submit" value="Купить">
        </form>
        @else
                  Для покупки необходимо авторизоваться.
        @endif    
        </div>
      </div>
    </div>
        
<div class="col-lg-3 col-md-4 col-sm-6">
      <div class="panel b-a vips">
        <div class="panel-heading text-center bg-info no-border">
          <h4 class="text-u-c m-b-none">VIP статус</h4>
          <h2 class="m-t-none">
            <span class="text-2x text-lt">1500</span>
            <span class="text-xs">руб.</span>
          </h2>
        </div>
        <div class="txt">Купив статус VIP вы всегда сможете пользоваться консультацией психолога, а также другими привилегиями сайта.</div>
        <div class="panel-footer text-center">
          @if(Sentry::check())
          <form id="payment" name="payment" method="post" action="https://sci.interkassa.com/" enctype="utf-8">
            <input type="hidden" name="ik_co_id" value="5586d1133b1eaf0e0a8b4568" />
            <input type="hidden" name="ik_pm_no" value="ID_4233" />
            <input type="hidden" name="ik_am" value="1500.00" />
            <input type="hidden" name="ik_cur" value="RUB" />
            <input type="hidden" name="ik_x_type" value="vips" />
            <input type="hidden" name="ik_desc" value="VIP статус 1500 руб." />
            
            <input class="btn btn-info font-bold m" style="background:#27C24C" type="submit" value="Купить">
        </form>
         @else
                  Для покупки необходимо авторизоваться.
         @endif   
        </div>
      </div>
    </div>
<div class="col-lg-3 col-md-4 col-sm-6"></div>

</div>