@extends('site')

@section('description')
   <meta name="description" content="">
@stop

@section('keywords')
    <meta name="keywords" content="">
@stop

@section('title')
    <title>Услуги | {{ Config::get('site.site_name') }}</title>
@stop

@section('google')
    
@stop

@section('script')

@stop

@section('content')
                
<div id="pages">

<div style="margin:20px;" class="panel panel-default">
  <div class="panel-heading">
    <h1 class="panel-title">Услуги</h1>
  </div>
  <div class="panel-body">
    <a href="{{ route('admin.uslugis.create') }}" class="btn btn-success m-b-20"><i class="fa fa-plus"></i> Создать</a>
    @if($uslugis->count())
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Заголовок</th>
                    <th>Путь</th>
                    <th>Дата</th>
                    <th>Статус</th>
                    <th>Действия</th>
                </tr>
            </thead>
            <tbody>
                @foreach($uslugis as $uslugi)
                <tr>
                    <td>{{ $uslugi->id }}</td>
                    <td><a href="{{ route('uslugis.view', $uslugi->slug) }}">{{ $uslugi->name }}</a></td>
                    <td>{{ $uslugi->slug }}</td>
                    <td>{{ $uslugi->created_at }}</td>
                    <td>{{ ($uslugi->status == 1) ? 'опубликовано' : 'не опубликовано' }}</td>
                    <td>
                        <a href="{{ route('admin.uslugis.edit', $uslugi->id) }}" title="Редактировать" class="btn btn-sm btn-icon btn-info"><i class="fa fa-pencil"></i></a>
                        <a href="javascript:" data-toggle="modal" data-target="#deleteUslugi-{{ $uslugi->id }}" title="Удалить" class="btn btn-sm btn-icon btn-danger"><i class="fa fa-trash-o"></i></a>
                        @include('uslugis.delete', $uslugi)
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    @else
        <div style="margin:20px;">Нет страниц.</div>
    @endif
  </div>
</div>

</div>
@stop