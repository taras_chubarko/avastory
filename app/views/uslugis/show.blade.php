@extends('site')

@section('description')
   <meta name="description" content="{{ $uslugi->description or '' }}">
@stop

@section('keywords')
    <meta name="keywords" content="{{ $uslugi->keywords or '' }}">
@stop

@section('title')
    <title>{{ ($uslugi->title) ? $uslugi->title : $uslugi->name }} | {{ Config::get('site.site_name') }}</title>
@stop

@section('google')
    
@stop

@section('script')

@stop

@section('content')
                
<div id="page">
    <div class="panel-body">
        <div class="bg-light lter b-b wrapper-md ng-scope">
            <h1 class="m-n font-thin h3">Услуги</h1>
             @if(Sentry::check())   
                @if(Sentry::getUser()->hasAccess('dashboard.doc'))
                    <a href="{{ route('admin.uslugis.edit', $uslugi->id) }}" title="Редактировать" class="btn btn-sm btn-icon btn-info pull-right" style="margin-top:-25px;"><i class="fa fa-pencil"></i></a>
                @endif
             @endif
        </div>
        <div class="blog-post">
            <div class="panel">
                <div class="wrapper-lg">
                    <h2 class="m-t-none">{{ $uslugi->name }}</h2>
                    <div>
                      <img src="{{ route('image.folder', array('uslugi', $uslugi->image, 350, 350, 'center')) }}" class="img-thumbnail m-r-md pull-left">
                      {{ $uslugi->body }}
                    </div>
                    <div class="line line-lg b-b b-light"></div>
                    <div class="text-danger">
                    @if($uslugi->price)
                      Стоимость услуги <i class="fa fa-rouble text-muted"></i>: {{ $uslugi->price }}
                     @endif
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</div>
@stop