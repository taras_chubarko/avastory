@if($uslugis->count())
<div id="uslugiBlock" class="panel panel-default">
  <div class="panel-heading">
    <h2 class="panel-title text-center color-46C2E9">Разовые услуги и их стоимость</h2>
  </div>
  <div class="panel-body">
    <div class="row">
     @foreach($uslugis as $uslugi)
            <div class="col-sm-6 col-md-3">
              <div class="thumbnail">
                <a href="{{ route('uslugis.view', $uslugi->slug) }}"><img src="{{ route('image.folder', array('uslugi', $uslugi->image, 200, 200, 'center')) }}" alt="..." class="m-t-sm"></a>
                <div class="caption">
                  <a class="h5" href="{{ route('uslugis.view', $uslugi->slug) }}">{{ $uslugi->name }}</a>
                    <div class="text-danger">
                    @if($uslugi->price)
                      {{ $uslugi->price }}
                    @endif
                    </div>
                </div>
              </div>
            </div>
     @endforeach
    </div>
  </div>
</div>
@endif