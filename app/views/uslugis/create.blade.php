@extends('site')

@section('description')
   <meta name="description" content="">
@stop

@section('keywords')
    <meta name="keywords" content="">
@stop

@section('title')
    <title>Услуги создать | {{ Config::get('site.site_name') }}</title>
@stop

@section('google')
    
@stop

@section('script')
{{ HTML::script('themes/site/js/jquery.ui.widget.js') }}
{{ HTML::script('themes/site/js/jquery.fileupload.js') }}
{{ HTML::script('themes/site/js/bootstrap-filestyle.js') }}
{{ HTML::script('/tinymce/tinymce.min.js') }}
{{ HTML::script('/tinymce/tinymce_editor.js') }}
<script type="text/javascript">
    editor_config.selector = ".body";
    tinymce.init(editor_config);
    $(document).ready(function() {
        //$.fn.uploadImage('uslugi');
        admin.article('uslugi');
    });
</script>
@stop

@section('content')
                
<div id="pages">
<div style="margin:20px;" class="panel panel-default">
  <div class="panel-heading">
    <h1 class="panel-title">Создать услугу</h1>
  </div>
{{ Form::open(array('route' => 'admin.uslugis.store', 'role' => 'form', 'id' => 'form-create-uslugis')) }}
  <div class="panel-body">
    {{ Form::hidden('user_id', Sentry::getUser()->id) }}
    <!-- Nav tabs -->
    <ul class="nav nav-tabs">
      <li class="active"><a href="#home" data-toggle="tab">Основное</a></li>
      <li><a href="#meta" data-toggle="tab">Мета</a></li>
    </ul>
    
    <!-- Tab panes -->
    <div class="tab-content">
      <div class="tab-pane active" id="home">
            
            <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                {{ Form::label('name', 'Заголовок') }}
                {{ Form::text('name', null, array('class' => 'form-control')) }}
            </div>
                
            <div class="form-group {{ $errors->has('body') ? 'has-error' : '' }}">
                {{ Form::label('body', 'Содержание') }}
                {{ Form::textarea('body', null, array('class' => 'form-control body', 'rows' => 10)) }}
            </div>
                
            <div class="form-group">
                {{ Form::label('image_ajax', 'Картинка') }}
                <div class="image-upload"></div>
                <input id="input-image" type="file">
            </div>
                
            <div class="form-group">
                {{ Form::label('price', 'Стоимость услуги') }}
                {{ Form::text('price', null, array('class' => 'form-control')) }}
                <p class="help-block">Например: 1 час – 2000 руб</p>
            </div>
                
            <div class="form-group">
                {{ Form::label('slug', 'Путь') }}
                {{ Form::text('slug', null, array('class' => 'form-control')) }}
            </div>
                
            <div class="checkbox">
                <label class="i-checks m-b-md">
                    {{ Form::hidden('status', 0) }}
                    {{ Form::checkbox('status', 1, true) }} <i></i> Опубликовано
                </label>
            </div>
            
      </div>
      <div class="tab-pane" id="meta">
            
            <div class="form-group">
                {{ Form::label('title', 'Заголовок') }}
                {{ Form::text('title', null, array('class' => 'form-control')) }}
            </div>
                
            <div class="form-group">
                {{ Form::label('keywords', 'Ключенвые слова') }}
                {{ Form::text('keywords', null, array('class' => 'form-control')) }}
            </div>
                
            <div class="form-group">
                {{ Form::label('description', 'Описание') }}
                {{ Form::textarea('description', null, array('class' => 'form-control', 'rows' => 5)) }}
            </div>
                
      </div>
    </div>

  </div><!--panel-body-->
  <div class="panel-footer">
    {{ HTML::decode(Form::button('<i class="fa fa-floppy-o"></i> Сохранить', array('class' => 'btn btn-dark', 'type' => 'submit'))) }}
  </div>
{{ Form::close() }}
</div>
</div>
@stop