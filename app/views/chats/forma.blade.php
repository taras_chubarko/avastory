<div id="chat" class="panel panel-default">
    <div class="panel-heading">Переписка <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button></div>
        <div class="panel-body">
            
        </div>
        <footer class="panel-footer">
          <!-- chat form -->
          <div>
            <a class="pull-left thumb-xs avatar"><img height="50" width="50" src="{{ route('user.avatar', array(Sentry::getUser()->id, 50, 50)) }}"></a>
            
             {{ Form::open(array('route' => 'chats.send.message.ajax', 'role' => 'form', 'id' => 'sendMessageForm', 'class' => 'form m-b-none m-l-xl ng-pristine ng-valid')) }}
              <div class="input-group">
                {{ Form::hidden('chat_id', $chat->id) }}
                {{ Form::hidden('user_id', Sentry::getUser()->id) }}
                {{ Form::hidden('flags', 1) }}
                {{ Form::text('message', null, array('class' => 'form-control', 'placeholder' => 'Напишите что нибуть')) }}
                <span class="input-group-btn">
                  <!--<button id="sendMessage" type="button" class="btn btn-default">ОТПРАВИТЬ</button>-->
                 {{ Form::submit('ОТПРАВИТЬ', array('class' => 'btn btn-default', 'id' => 'sendMessage')) }}
                </span>
              </div>
             {{ Form::close() }}   
          </div>
    </footer>
</div> 
    
    
    
    
    
    
    
    
    