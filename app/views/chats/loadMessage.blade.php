@if($chats->count())
    @foreach($chats as $chat)
    
    @if(Sentry::getUser()->id == $chat->user_id)
    <div class="m-b">
        <a class="pull-left thumb-sm avatar" href=""><img height="50" width="50" src="{{ route('user.avatar', array($chat->user_id, 50, 50)) }}"></a>
        
        <div class="m-l-xxl">
          <div class="pos-rlt wrapper b b-light r r-2x">
            <span class="arrow left pull-up"></span>
            <p class="m-b-none">{{ $chat->message }}</p>
          </div>
          <small class="text-muted">{{ LocalizedCarbon::instance($chat->created_at)->diffForHumans() }}</small>
        </div>
            
    </div>
        
    @else
            
    <div class="m-b">
      <a class="pull-right thumb-sm avatar" href=""><img height="50" width="50" src="{{ route('user.avatar', array($chat->user_id, 50, 50)) }}"></a>
      
      <div class="m-r-xxl">
        <div class="pos-rlt wrapper bg-primary r r-2x">
          <span class="arrow right pull-up arrow-primary"></span>
          <p class="m-b-none">{{ $chat->message }}</p>
        </div>
        <small class="text-muted">{{ LocalizedCarbon::instance($chat->created_at)->diffForHumans() }}</small>
      </div>
        
    </div>
        
    @endif
    @endforeach
@endif