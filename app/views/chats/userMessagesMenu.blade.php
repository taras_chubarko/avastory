@if(Chat::countAllNew() == 0)
<a  href="{{ route('user.messages', $user->id) }}">
@else
<a class="dropdown-toggle" href="javascript:" data-toggle="dropdown" aria-expanded="false">
@endif
    <i class="icon-envelope fa-fw"></i>
    <span class="visible-xs-inline">Сообщения</span>
    @if(Chat::countAllNew() > 0)
        <span class="badge badge-sm up bg-danger pull-right-xs">{{ Chat::countAllNew() }}</span>
    @endif
</a>
@if(Chat::countAllNew() > 0)
<div class="dropdown-menu w-xl animated fadeInUp">
    <div class="panel bg-white">
      <div class="panel-heading b-light bg-light">
        <strong>У вас <span>{{ Chat::countAllNew() }}</span> сообщения</strong>
      </div>
      <div class="list-group">
        
        @foreach(Chat::lastNewMessages() as $chat)
        <div class="media list-group-item" style="font-size:12px;">
          <span class="pull-left thumb-sm">
            <img class="img-circle" alt="" src="{{ route('user.avatar', array($chat->user_id, 50, 50)) }}">
            <span class="badge badge-sm up bg-danger pull-right-xs">{{ Chat::countChatNew($chat->chat_id) }}</span> 
          </span>
          <span class="media-body block m-b-none">
             {{ $chat->message }}<br>
            <small class="text-muted">{{ LocalizedCarbon::instance($chat->created_at)->diffForHumans() }}</small>
          </span>
        </div>
        @endforeach 
        
            
      </div>
      <div class="panel-footer text-sm">
        <a data-toggle="class:show animated fadeInRight" href="{{ route('user.messages', $user->id) }}">Смотреть все сообщения</a>
      </div>
    </div>
</div>
@endif