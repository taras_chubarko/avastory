<div id="user-1">
    <span class="badge bg-success">в сети</span>
    
    <img height="150" width="150" src="{{ route('user.avatar', array($user1->id, 150, 150)) }}" class="img-circle">
    
    <div class="btn-group btn-group-justified users-btn">
      <a href="{{ route('user.photos', $user1->id) }}" title="Фотографий" class="btn btn-primary"><i class="fa fa-camera"></i> {{ $user1->photos->count() }}</a>
      <a href="{{ route('user.id', $user1->id) }}" title="Смотреть профиль" class="btn btn-success"><i class="fa fa-eye"></i></a>
    </div>
        
</div>
    
<div id="user-2">

    <span class="badge bg-success">в сети</span>
        
    <img height="150" width="150" src="{{ route('user.avatar', array($user2->id, 150, 150)) }}" class="img-circle">
    
    <div class="btn-group btn-group-justified users-btn">
      <a href="{{ route('user.photos', $user2->id) }}" title="Фотографий" class="btn btn-primary"><i class="fa fa-camera"></i> {{ $user2->photos->count() }}</a>
      <a href="{{ route('user.id', $user2->id) }}" title="Смотреть профиль" class="btn btn-success"><i class="fa fa-eye"></i></a>
    </div>
        
</div>