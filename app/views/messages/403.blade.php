@extends('site')

@section('title')
    <title>403 | {{ Config::get('site.site_name') }}</title>
@stop

@section('content')

<div id="p404" class="container-fluid">
   <div class="col-sm-4 col-sm-offset-4">
  
  <div class="text-center m-b-lg">
    
    <h1 class="h animated bounceInDown">
      403
    </h1>
    <p>Доступ запрещен.</p>
  </div>
  
  <div class="list-group m-b-sm bg-white m-b-lg">
    
    <a class="list-group-item" href="/">
      <i class="glyphicon glyphicon-home text-muted"></i>
      На главную
      <i class="glyphicon glyphicon-chevron-right text-muted pull-right"></i>
    </a>
    
    <a class="list-group-item" href="#">
      <i class="glyphicon glyphicon-earphone text-muted"></i>
       Позвоните нам
       <i class="glyphicon glyphicon-chevron-right text-muted pull-right"></i>
      <span class="badge pull-right">021-215-584</span>
    </a>
    
  </div>
  
</div>

</div>
  
@stop

