@if ( Session::has('errors') )
<div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4>Ошибка!</h4>
    <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
<div class="over"></div>
@endif

@if ( Session::has('success') )
<div class="alert alert-success">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4>Внимание!</h4>
    {{ Session::get('success') }}  
</div>
<div class="over"></div>    
@endif

@if ( Session::has('warning') )
<div class="alert alert-warning">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4>Внимание!</h4>
                {{ Session::get('warning') }}
</div>
<div class="over"></div>
@endif

@if ( Session::has('error') )
<div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4>Ошибка!</h4>
                {{ Session::get('error') }}
</div>
<div class="over"></div>   
@endif

@if ( Session::has('info') )
<div class="alert alert-info">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4>Внимание!</h4>
        {{ Session::get('info') }}
</div>
<div class="over"></div>
@endif
