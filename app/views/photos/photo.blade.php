@if($prev)
    <a href="javascript:" data-id="{{ $prev->id }}" class="prev"><i class="fa fa-angle-left"></i></a>
@endif
@if($next)
    <a href="javascript:" data-id="{{ $next->id }}" class="next"><i class="fa fa-angle-right"></i></a>
@endif
<header>

    {{ App::make('StarsController')->show('photo', $photo->id) }}
    
</header>
<div class="imagePhoto">
    <span class="helper"></span>
    <img src="/uploads/photos/{{ $photo->photo }}" class="img-responsive">
</div>