<div id="photo-comments" class="photo-comments-{{ $photo->id }}">
   @include('comments.photo', array('comments' => $photo->comments ))
</div>
<div id="photo-comments-form">
    <div class="clearfix">
        <a class="pull-left thumb-sm avatar">
            <img class="img-circle" alt="{{ Sentry::getUser()->profile->name }}" src="{{ route('user.avatar', array(Sentry::getUser()->id, 200, 200)) }}">
        </a>
        <div class="m-l-xxl">
          {{ Form::open(array('route' => 'comments.store', 'id' => 'comments-store-ajax', 'role' => 'form', 'class' => 'm-b-none ng-pristine ng-valid')) }}
            {{ Form::hidden('type', 'photo') }}
            {{ Form::hidden('type_id', $photo->id) }}
            {{ Form::hidden('parent_id', 0) }}
            <div class="input-group">
              {{ Form::text('comment', null, array('class' => 'form-control', 'placeholder' => 'Напишите ваш коммент...')) }}
              <span class="input-group-btn">
                {{ Form::submit('Отправить', array('class' => 'btn btn-button')) }}
              </span>
            </div>
          {{ Form::close() }}
        </div>
    </div>
</div>