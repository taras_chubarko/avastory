<div id="loadPhotoAjax">

<div class="pan">
    <a class="close" href="javascript:"><i class="icon-close"></i></a>
    <div class="middle">
	<div class="middle-container">
	    <div class="middle-content">
                @include('photos.photo', array($photo, $prev, $next))
            </div><!-- .content -->
	</div><!-- .container-->
	<aside class="middle-right-sidebar">
             @include('photos.comment', array($photo))
        </aside><!-- .right-sidebar -->
    </div>    
</div>
    <div class="clearfix"></div>
<div class="ovr"></div>
</div>