@extends('site')

@section('description')
   <meta name="description" content="{{ $page->description }}">
@stop

@section('keywords')
    <meta name="keywords" content="{{ $page->keywords }}">
@stop

@section('title')
    <title>{{ ($page->title) ? $page->title : $page->name }} | {{ Config::get('site.site_name') }}</title>
@stop

@section('script')

@stop

@section('content')
                
<div id="page">
    <div class="panel-body">
        <div class="bg-light lter b-b wrapper-md ng-scope">
            <h1 class="m-n font-thin h3">{{ $page->name }}</h1>
             @if(Sentry::check())   
                @if(Sentry::getUser()->hasAccess('dashboard.doc'))
                    <a href="{{ route('admin.pages.edit', $page->id) }}" title="Редактировать" class="btn btn-sm btn-icon btn-info pull-right" style="margin-top:-25px;"><i class="fa fa-pencil"></i></a>
                @endif
             @endif
        </div>
        <div class="blog-post">
            <div class="panel">
                {{ $body }}
            </div>
        </div>
    </div>
</div>
@stop