@extends('site')

@section('description')
   <meta name="description" content="">
@stop

@section('keywords')
    <meta name="keywords" content="">
@stop

@section('title')
    <title>Страници | {{ Config::get('site.site_name') }}</title>
@stop

@section('script')

@stop

@section('content')
                
<div id="pages">

<div style="margin:20px;" class="panel panel-default">
  <div class="panel-heading">
    <h1 class="panel-title">Страници</h1>
  </div>
  <div class="panel-body">
    <a href="{{ route('admin.pages.create') }}" class="btn btn-success"><i class="fa fa-plus"></i> Создать</a>
    @if($pages->count())
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Заголовок</th>
                    <th>Путь</th>
                    <th>Дата</th>
                    <th>Статус</th>
                    <th>Действия</th>
                </tr>
            </thead>
            <tbody>
                @foreach($pages as $page)
                <tr>
                    <td>{{ $page->id }}</td>
                    <td><a href="{{ route('pages.view', $page->slug) }}">{{ $page->name }}</a></td>
                    <td>{{ $page->slug }}</td>
                    <td>{{ $page->created_at }}</td>
                    <td>{{ ($page->status == 1) ? 'опубликовано' : 'не опубликовано' }}</td>
                    <td>
                        <a href="{{ route('admin.pages.edit', $page->id) }}" title="Редактировать" class="btn btn-sm btn-icon btn-info"><i class="fa fa-pencil"></i></a>
                        <a href="javascript:" data-toggle="modal" data-target="#deletePage-{{ $page->id }}" title="Удалить" class="btn btn-sm btn-icon btn-danger"><i class="fa fa-trash-o"></i></a>
                        @include('pages.delete', $page)
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    @else
        <div style="margin:20px;">Нет страниц.</div>
    @endif
  </div>
</div>

</div>
@stop