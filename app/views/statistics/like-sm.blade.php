
<a class="btn btn-default btn-sm like like-sm like-{{ $type->id }}" href="javascript:" data-type_id="{{ $type->id }}" data-type="{{ $type_name }}">
    <i class="icon-like"></i> Нравится
</a>
@if($type->likes->count())   
<span class="label bg-light dk like-count">+{{ $type->likes->count() }}</span>
@endif