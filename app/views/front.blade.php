@extends('site')

@section('description')
   <meta name="description" content="">
@stop

@section('keywords')
    <meta name="keywords" content="">
@stop

@section('title')
    <title>Сайт знакомств | {{ Config::get('site.site_name') }}</title>
@stop

@section('google')
    {{ HTML::script('https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places') }}
@stop

@section('script')
    {{ HTML::script('themes/site/js/infobox.js') }}
    {{ HTML::script('themes/site/js/site.js') }}
@stop

@section('content')

<div id="map-canvas"></div>

@include('site.i18')

@stop

