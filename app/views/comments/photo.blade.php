@if($comments->count())
<div class="m-b b-l m-l-md streamline">
        
        @foreach($comments as $comment)
        <div>
          <a class="pull-left thumb-sm avatar m-l-n-md">
            <img alt="{{ $comment->user->profile->name }}" class="img-circle" src="{{ route('user.avatar', array($comment->user->id, 50, 50)) }}">
          </a>
          <div class="m-l-lg panel b-a">
            <div class="panel-heading pos-rlt b-b b-light">
              <span class="arrow left"></span>
              <a href="{{ route('user.id', $comment->user->id) }}">{{ $comment->user->profile->name }}</a>
             
              <span class="text-muted m-l-sm pull-right">
                <i class="fa fa-clock-o"></i>
                {{ date('d.m.Y в H:i', strtotime($comment->created_at)) }}
              </span>
            </div>
            <div class="panel-body">
              <div>{{ $comment->comment }}</div>
            </div>
          </div>
        </div>
        @endforeach
</div>
@else
    Нет комментариев.
@endif