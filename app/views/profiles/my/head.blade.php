<div id="ubf" style="background:url({{ ($user->profile->fon) ? '/uploads/users/fon/'.$user->profile->fon : 'http://lorempixel.com/1900/800/' }}) center center; background-size:cover">
    <div class="wrapper-lg bg-white-opacity">
     
     <div id="progress" class="progress-xs progress ng-isolate-scope hidden" type="info">
        <div role="progressbar"  class="progress-bar progress-bar-info"></div>
     </div>
     
     <div id="load-fon" data-toggle="tooltip" data-placement="bottom" title="Обновить фото обложки">
                    {{ Form::file('fon', array('class' => 'filestyle', 'data-buttonName' => 'btn-clear', 'data-input'=>'false', 'data-buttonText' => '', 'data-badge' => 'false', 'data-iconName' => 'glyphicon glyphicon-camera', 'id' => 'fon-image')) }}
     </div>
     
      <div class="row m-t">
        <div class="col-sm-7">
          <a class="pull-left m-r" href="">
            <div class="easyPieChart">
                    <img class="ava-big img-circle" alt="{{ $user->profile->name }}" src="{{ route('user.avatar', array($user->id, 200, 200)) }}">
            </div>
          </a>     
          <div id="load-avatar" data-toggle="tooltip" data-placement="top" title="Обновить фотографию профиля">
                    {{ Form::file('avatar', array('class' => 'filestyle', 'data-buttonName' => 'btn-clear', 'data-input'=>'false', 'data-buttonText' => '', 'data-badge' => 'false', 'data-iconName' => 'glyphicon glyphicon-camera', 'id' => 'avatar-image')) }}
          </div>
          <div class="clear m-b">
            <div class="m-b m-t-sm names">
              <span class="h3">{{ $user->profile->name }}</span>
              <small class="m-l">{{ $user->profile->adres }}</small>
            </div>
            <p class="m-b">
              <a class="btn btn-sm btn-bg btn-rounded btn-default btn-icon" href=""><i class="fa fa-twitter"></i></a>
              <a class="btn btn-sm btn-bg btn-rounded btn-default btn-icon" href=""><i class="fa fa-facebook"></i></a>
              <a class="btn btn-sm btn-bg btn-rounded btn-default btn-icon" href=""><i class="fa fa-google-plus"></i></a>
            </p>
            
          </div>
        </div>
        <div class="col-sm-5">
          
        </div>
      </div>
    </div>
  </div><!------------>