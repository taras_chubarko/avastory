@extends('site')

@section('description')
   <meta name="description" content="">
@stop

@section('keywords')
    <meta name="keywords" content="">
@stop

@section('title')
    <title>{{ $user->profile->name }} | {{ Config::get('site.site_name') }}</title>
@stop

@section('script')
{{ HTML::script('themes/site/js/jquery.ui.widget.js') }}
{{ HTML::script('themes/site/js/jquery.fileupload.js') }}
{{ HTML::script('themes/site/js/bootstrap-filestyle.js') }}
{{ HTML::script('themes/site/js/jquery.easy-pie-chart.js') }}
{{ HTML::script('themes/site/js/myprofile.js') }}
<script type="application/x-javascript">
$(document).ready(function() {
 
  var map;
  var myLatlng = new google.maps.LatLng(parseFloat({{ $user->profile->lat }}), parseFloat({{ $user->profile->lng }}));
     
  function myUserMap() {
          
          var mapOptions = {
            zoom: 13,
            center: myLatlng
          };
          
          map = new google.maps.Map(document.getElementById('profile-map'), mapOptions);
          
          if (genderID == 2)
          {
              var image = '/themes/site/img/womenm.png';
          }
          else{
               var image = '/themes/site/img/menm.png';
          }
         
          var marker = new google.maps.Marker({
               position: myLatlng,
               map: map,
               title: 'Я тащуть от твоей мышки',
               draggable: true,
               icon: image
          });
          
          google.maps.event.addListener(marker, 'dragend', function (event) {
               var lat = this.getPosition().lat();
               var lng = this.getPosition().lng();
               
               $.post('/user/profile/save/point', { lat:lat, lng:lng, profile_id:profileID } );
               
          });
           
     }
     
     google.maps.event.addDomListener(window, 'load', myUserMap);
     
     $('a[href="#kontakt"]').on('shown.bs.tab', function(e){
        google.maps.event.trigger(map, 'resize');
        map.setCenter(myLatlng);
    });
     
     
     
});
</script>
@stop

@section('content')

<div id="profile">
 @include('profiles.my.head', $user)
 @include('profiles.menu', $user)
              <!------------>
              
              <div class="padder">
                <div class=" padder-v">
                    <div id="information" class="panel panel-default">
                        <div class="panel-heading">
                          <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i> Информация</h3>
                        </div>
                        <div class="panel-body">
                           
                           <div class="pull-left"> <!-- required for floating -->
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs tabs-left">
                                  <li class="active"><a href="#about" data-toggle="tab">Информация о вас</a></li>
                                  <li><a href="#kontakt" data-toggle="tab">Контактная информация</a></li>
                                  <li><a href="#work" data-toggle="tab">Образование и работа</a></li>
                                </ul>
                            </div>
                            
                            <div class="pull-left">
                                <!-- Tab panes -->
                                <div class="tab-content">
                                  <div class="tab-pane active" id="about">
                                    <div class="rws">
                                        <h3>О себе</h3>
                                        @if(!$user->profile->about)
                                            <button class="btn btn-default" title="Добавить" data-toggle="modal" data-target="#addAboutModal"><i class="fa fa-plus"></i></button>
                                            @include('profiles.information.addAbout', $user)
                                        @else
                                            <button class="btn btn-default" title="Редактировать" data-toggle="modal" data-target="#editAboutModal"><i class="fa fa-pencil"></i></button>
                                            @include('profiles.information.editAbout', $user)
                                        @endif
                                        <div class="body">
                                            {{ $user->profile->about or '' }}
                                        </div>
                                    </div>
                                    <div class="rws">
                                        <h3>О чем вы думаете</h3>
                                        @if(!$user->profile->thinking)
                                            <button class="btn btn-default" title="Добавить" data-toggle="modal" data-target="#addThinkingModal"><i class="fa fa-plus"></i></button>
                                            @include('profiles.information.addThinking', $user)
                                        @else
                                            <button class="btn btn-default" title="Редактировать" data-toggle="modal" data-target="#editThinkingModal"><i class="fa fa-pencil"></i></button>
                                            @include('profiles.information.editThinking', $user)
                                        @endif
                                        <div class="body">
                                           {{ $user->profile->thinking or '' }}
                                        </div>
                                    </div>
                                  </div>
                                  <div class="tab-pane" id="kontakt">
                                    <div class="rws">
                                        <h3>Контактная информация</h3>
                                        <div class="body">
                                         <ul class="lists">
                                          <li><label class="pull-left">Ваше имя</label> <div class="name pull-left">{{ $user->profile->name or 'не заполнено' }}</div> <a href="javascript:" data-profire-id="{{ $user->profile->id }}" class="btn btn-default pull-right editName" title="Редактировать" ><i class="fa fa-pencil"></i></a><div class="clearfix"></li>
                                          <li><label class="pull-left">Мобильный телефон</label> <div class="telephone pull-left">{{ $user->profile->telephone or 'не заполнено' }}</div> <a href="javascript:" data-profire-id="{{ $user->profile->id }}" class="btn btn-default pull-right editTelephone" title="Редактировать"><i class="fa fa-pencil"></i></a></li>
                                          <li><label class="pull-left">Город</label> <div class="city pull-left">{{ ($user->profile->city) ? $user->profile->city : 'не заполнено' }}</div> <a href="javascript:" data-profire-id="{{ $user->profile->id }}" class="btn btn-default pull-right editCity" title="Редактировать"><i class="fa fa-pencil"></i></a></li>
                                         </ul>
                                        </div>
                                    </div>
                                    <div class="rws">
                                        <h3>Общая информация</h3>
                                        <div class="body">
                                         <ul class="lists">
                                          <li><label class="pull-left">Дата рождения</label> <div class="bithday pull-left">{{ ($user->profile->bithday) ? DateFormat::textDate($user->profile->bithday) : 'не заполнено' }}</div> <a href="javascript:" data-profire-id="{{ $user->profile->id }}" class="btn btn-default pull-right editBithday" title="Редактировать" ><i class="fa fa-pencil"></i></a></li>
                                          <li><label class="pull-left">Возраст</label> <div class="age pull-left">{{ ($user->profile->age) ? $user->profile->age . DateFormat::num2word($user->profile->age, array(' год', ' года', ' лет')) : 'не заполнено' }}</div></li>
                                          <li><label class="pull-left">Пол</label> <div class="gender pull-left">{{ Config::get('site.gender.'.$user->profile->gender) }}</div> <a href="javascript:" data-profire-id="{{ $user->profile->id }}" class="btn btn-default pull-right editGender" title="Редактировать" ><i class="fa fa-pencil"></i></a></li>
                                          <li><label class="pull-left">Я ищу</label> <div class="gender_search pull-left">{{ Config::get('site.gender_search.'.$user->profile->gender_search) }}</div> <a href="javascript:" data-profire-id="{{ $user->profile->id }}" class="btn btn-default pull-right editGenderSearch" title="Редактировать" ><i class="fa fa-pencil"></i></a></li>
                                         </ul>
                                        </div>
                                    </div>
                                    <div class="rws">
                                      <h3>Карта</h3>
                                      <div class="body">
                                       @if($user->profile->autopoint == 0)
                                        <div class="alert alert-warning">Пожалуйста укажите ваше точное место на карте, иначе ваш профиль будет показываться не корректно.</div>
                                       @endif
                                       
                                       <div id="profile-map">
                                        
                                       </div>
                                       
                                      </div>
                                    </div>
                                  </div>
                                  <div class="tab-pane" id="work">
                                   <div class="rws">
                                     <h3>Работа</h3>
                                        @if(!$user->profile->work)
                                            <button class="btn btn-default" title="Добавить" data-toggle="modal" data-target="#addWorkModal"><i class="fa fa-plus"></i></button>
                                            @include('profiles.information.addWork', $user)
                                        @else
                                            <button class="btn btn-default" title="Редактировать" data-toggle="modal" data-target="#editWorkModal"><i class="fa fa-pencil"></i></button>
                                            @include('profiles.information.editWork', $user)
                                        @endif
                                      <div class="body">
                                       @if($user->profile->work)
                                       <ul class="lists">
                                        @foreach( $user->profile->works() as $key => $value)
                                         <li><label>{{ $key }}</label> {{ $value }}</li>
                                         @endforeach
                                       </ul>
                                       @endif
                                      </div>
                                   </div>
                                   <div class="rws">
                                     <h3>ВУЗ</h3>
                                      @if(!$user->profile->vuz)
                                            <button class="btn btn-default" title="Добавить" data-toggle="modal" data-target="#addVuzModal"><i class="fa fa-plus"></i></button>
                                            @include('profiles.information.addVuz', $user)
                                        @else
                                            <button class="btn btn-default" title="Редактировать" data-toggle="modal" data-target="#editVuzModal"><i class="fa fa-pencil"></i></button>
                                            @include('profiles.information.editVuz', $user)
                                        @endif
                                      <div class="body">
                                      @if($user->profile->vuz)
                                       <ul class="lists">
                                         @foreach( $user->profile->vuzs() as $key => $value)
                                          <li><label>{{ $key }}</label> {{ $value }}</li>
                                          @endforeach
                                        </ul>
                                      @endif
                                      </div>
                                   </div>
                                   <div class="rws">
                                     <h3>Школа</h3>
                                        @if(!$user->profile->school)
                                            <button class="btn btn-default" title="Добавить" data-toggle="modal" data-target="#addSchoolModal"><i class="fa fa-plus"></i></button>
                                            @include('profiles.information.addSchool', $user)
                                        @else
                                            <button class="btn btn-default" title="Редактировать" data-toggle="modal" data-target="#editSchoolModal"><i class="fa fa-pencil"></i></button>
                                            @include('profiles.information.editSchool', $user)
                                        @endif
                                      <div class="body">
                                      @if($user->profile->school)
                                        <ul class="lists">
                                         @foreach( $user->profile->schools() as $key => $value)
                                          <li><label>{{ $key }}</label> {{ $value }}</li>
                                          @endforeach
                                        </ul>
                                       @endif
                                      </div>
                                   </div> 
                                  </div>
                                </div>
                            </div>
                                
                        </div>
                    </div>
                </div>
              </div><!------------>
</div>
@stop