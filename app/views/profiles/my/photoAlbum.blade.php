@extends('site')

@section('description')
   <meta name="description" content="">
@stop

@section('keywords')
    <meta name="keywords" content="">
@stop

@section('title')
    <title>{{ $user->profile->name }} | {{ Config::get('site.site_name') }}</title>
@stop

@section('script')
{{ HTML::script('themes/site/js/jquery.ui.widget.js') }}
{{ HTML::script('themes/site/js/jquery.iframe-transport.js') }}
{{ HTML::script('themes/site/js/jquery.fileupload.js') }}
{{ HTML::script('themes/site/js/bootstrap-filestyle.js') }}
{{ HTML::script('themes/site/js/jquery.easy-pie-chart.js') }}
{{ HTML::script('themes/site/js/fileinput.min.js') }}
{{ HTML::script('themes/site/js/fileinput_locale_ru.js') }}

@stop

@section('style')
{{ HTML::style('themes/site/css/fileinput.min.css') }}
@stop

@section('content')

<div id="profile">
 @include('profiles.my.head', $user)
 @include('profiles.menu', $user)
<div class="padder">
   <div class="padder-v">
       <div id="photos" class="panel panel-default">
           <div class="panel-heading">
             <h3 class="panel-title"><i class="glyphicon glyphicon-camera"></i> {{ $album->name }}</h3>
           </div>
           <div class="panel-body">
             <ul class="photos-ul">
                <li>
                     <div class="panel b-a add-foto">
                        <!--<a href="javascript:"><i class="icon-plus"></i><br> Добавить фото</a>-->
                        {{ Form::file('upload_photo[]', array('class' => 'filestyle', 'data-buttonName' => 'btn-clear', 'data-input'=>'false', 'data-buttonText' => 'Добавить фото', 'data-badge' => 'false', 'data-iconName' => 'icon-plus', 'id' => 'upload_photo', 'accept' => 'image/*', 'multiple')) }}
                    </div>
                </li>
                @if($photos->count())
                    @foreach($photos as $photo)
                        <li>
                            <div class="panel b-a">
                                <a href="javascript:" class="edit-photo btn btn-rounded btn-sm btn-icon btn-default"><i class="icon-pencil"></i></a>
                                <a href="javascript:" class="loadPhotoAjax" data-photo-id="{{ $photo->id }}"><img src="{{ route('image.folder', array('photos', $photo->photo, 198, 198, 'center')) }}" alt="{{ $photo->alt }}" title="{{ $photo->title or $album->name }}"></a>
                                <footer class="bottom bg-gd-dk text-white">
                                    <a href="javascript:" class="loadPhotoAjax" data-photo-id="{{ $photo->id }}"><i class="icon-bubbles"></i> Комментарии {{ $photo->comments->count() }}</a>&ensp;
                                    @include('statistics.like-link', array('type' => $photo, 'type_name' => 'photo')) 
                                </footer>
                            </div>
                        </li>
                    @endforeach
                @endif
             </ul>
           </div>
       </div><!--#photos-->
   </div>
 </div><!------------>
 <!------------>
</div>
<script>
    $(document).ready(function() {
         
        function uploadPhoto() {
            $('#upload_photo').fileupload({
               url: '/upl/photos',
               //autoUpload: false,
               dataType: 'json',
               singleFileUploads:false,
              // maxFileSize: 1024,
               //limitMultiFileUploadSize: 1024,
               formData: {user_id:{{ $user->id }} ,album_id:{{ $album->id }} },
               done: function (e, data) {
                    //console.log(data);
                    $('#progress1').addClass('hidden');
                    location.reload();
                    e.preventDefault();
               },
               progressall: function (e, data) {
                   //$('.progress').removeClass('hidden');
                   var progress = parseInt(data.loaded / data.total * 100, 10);
                   $('#progress1 .progress-bar').css(
                       'width',
                       progress + '%'
                   );
               },
               fail: function(e, data) {
                    alert('Ошибка загрузки!');
               }
          })
          .bind('fileuploadadd', function (e, data) {
             $('.photos-ul').before('<div type="info" class="progress-xs progress ng-isolate-scope" id="progress1"><div class="progress-bar progress-bar-info" role="progressbar"></div></div>');
             //console.log(data);
          });
        }
        uploadPhoto();
        
        
        
    });
</script>
@stop