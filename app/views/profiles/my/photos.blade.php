@extends('site')

@section('description')
   <meta name="description" content="">
@stop

@section('keywords')
    <meta name="keywords" content="">
@stop

@section('title')
    <title>{{ $user->profile->name }} | {{ Config::get('site.site_name') }}</title>
@stop


@section('script')
{{ HTML::script('themes/site/js/jquery.ui.widget.js') }}
{{ HTML::script('themes/site/js/jquery.fileupload.js') }}
{{ HTML::script('themes/site/js/bootstrap-filestyle.js') }}
{{ HTML::script('themes/site/js/jquery.easy-pie-chart.js') }}
{{ HTML::script('themes/site/js/myprofile.js') }}
@stop

@section('content')

<div id="profile">
 @include('profiles.my.head', $user)
 @include('profiles.menu', $user)
<div class="padder">
   <div class="padder-v">
       <div id="photos" class="panel panel-default">
           <div class="panel-heading">
             <h3 class="panel-title"><i class="glyphicon glyphicon-camera"></i> Мои альбомы</h3>
           </div>
           <div class="panel-body">
             <a style="margin-bottom:20px;" class="btn btn-default pull-right" href="javascript:" data-toggle="modal" data-target="#addAlbum">Добавить альбом</a>
              @include('albums.create',  $user)
              <div class="clearfix"></div>
              <div id="albums">
               
               @if($user->albums->count())
                <div class="row">
                 @foreach($user->albums as $album)
                  <div class="col-md-3">
                     <div class="panel b-a">
                       <div class="panel-heading no-border bg-primary" style="height: 40px;">
                         <span class="text-lt thinking" data-content="{{ $album->name }}" data-placement="top" data-toggle="popover" data-container="body">
                          {{ $album->name }}
                         </span>
                       </div>
                       <div class="item m-l-n-xxs m-r-n-xxs">
                         <a href="{{ route('user.album', array($user->id, $album->slug)) }}"><img src="{{ route('image.folder', array('photos', ($album->photo) ? $album->photo->photo : 'no_image.jpg', 247, 247, 'center')) }}" class="img-full"></a>
                       </div>
                       <div class="hbox text-center b-b b-light text-sm">          
                         <a href="{{ route('user.album', array($user->id, $album->slug)) }}" class="col padder-v text-muted b-r b-light">
                           <i class="icon-camera block m-b-xs fa-2x"></i>
                           <span>{{ $album->photos()->count() }}</span>
                         </a>
                         <a href="javascript:" data-toggle="modal" data-target="#editAlbum-{{ $album->id }}" class="col padder-v text-muted b-r b-light">
                           <i class="icon-pencil block m-b-xs fa-2x"></i>
                           <span>Изменить</span>
                         </a>
                         <a href="javascript:" data-toggle="modal" data-target="#deleteAlbum-{{ $album->id }}" class="col padder-v text-muted">
                           <i class="icon-trash  block m-b-xs fa-2x"></i>
                           <span>Удалить</span>
                         </a>
                       </div>
                     </div>
                  </div>
                  @include('albums.edit', $album)
                  @include('albums.delete', $album)
                 @endforeach
                </div>
               @else
                 <div class="noresult">
                  <i class="glyphicon glyphicon-camera"></i>
                  <p>Нет альбомов.</p>
                 </div>
               @endif
               
               
              </div>
           </div>
       </div><!--#photos-->
   </div>
 </div><!------------>
 <!------------>
</div>
@stop