@extends('site')

@section('description')
   <meta name="description" content="">
@stop

@section('keywords')
    <meta name="keywords" content="">
@stop

@section('title')
    <title>{{ $user->profile->name }} | {{ Config::get('site.site_name') }}</title>
@stop

@section('google')
    
@stop

@section('script')
{{ HTML::script('themes/site/js/jquery.ui.widget.js') }}
{{ HTML::script('themes/site/js/jquery.fileupload.js') }}
{{ HTML::script('themes/site/js/bootstrap-filestyle.js') }}
{{ HTML::script('themes/site/js/jquery.easy-pie-chart.js') }}
{{ HTML::script('themes/site/js/myprofile.js') }}
@stop

@section('content')

<div id="profile">
 @include('profiles.my.head', $user)
 @include('profiles.menu', $user)
 
 <!------------>
  <div class="padder">
    <div class="padder-v">
        <div class="panel panel-default">
            <div class="panel-heading">Сообщения</div>
            <div class="panel-body p-none">
                @if($chats->count())
                    <div id="userMessages">
                        <div class="umLeft">
                            <ul class="chatList">
                                @foreach($chats as $chat)
                                    @if($chat->user_to == $user->id)
                                        <li>
                                            <a class="userLoadMessages" data-chat-id="{{ $chat->id }}"  data-user-id="{{ $chat->userFrom->user_id }}" heref="javascript:">
                                                <img class="img-circle" height="50" width="50" src="{{ route('user.avatar', array($chat->userFrom->user_id, 50, 50)) }}">
                                                {{ $chat->userFrom->name }} 
                                                
                                                @if(Chat::countChatNew($chat->id) > 0)
                                                    <span class="badge badge-sm bg-danger pull-right-xs">{{ Chat::countChatNew($chat->id) }}</span>
                                                @endif
                                            </a>
                                        </li>
                                    @endif
                                    @if($chat->user_from == $user->id)
                                        <li>
                                            <a class="userLoadMessages" data-chat-id="{{ $chat->id }}" data-user-id="{{ $chat->userTo->user_id }}"  heref="javascript:">
                                                <img class="img-circle" height="50" width="50" src="{{ route('user.avatar', array($chat->userTo->user_id, 50, 50)) }}">
                                                {{ $chat->userTo->name }} 
                                                
                                                @if(Chat::countChatNew($chat->id) > 0)
                                                    <span class="badge badge-sm bg-danger pull-right-xs">{{ Chat::countChatNew($chat->id) }}</span>
                                                @endif
                                            </a>
                                        </li>
                                    @endif
                                @endforeach
                            </ul> 
                        </div>
                        <div class="umRight">
                            <p class="text-center">Нажмите на пользователя чтобы посмотреть переписку.</p>
                        </div>
                    </div>
                @else
                 <div class="noresult">
                    <i class="icon-envelope"></i>
                    <p>Нет сообщений.</p>
                 </div>
                @endif
            </div>
        </div>
    </div>
  </div><!------------>
</div>
@stop