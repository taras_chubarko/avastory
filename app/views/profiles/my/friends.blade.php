@extends('site')

@section('description')
   <meta name="description" content="">
@stop

@section('keywords')
    <meta name="keywords" content="">
@stop

@section('title')
    <title>{{ $user->profile->name }} | {{ Config::get('site.site_name') }}</title>
@stop


@section('script')
{{ HTML::script('themes/site/js/jquery.ui.widget.js') }}
{{ HTML::script('themes/site/js/jquery.fileupload.js') }}
{{ HTML::script('themes/site/js/bootstrap-filestyle.js') }}
{{ HTML::script('themes/site/js/jquery.easy-pie-chart.js') }}
{{ HTML::script('themes/site/js/myprofile.js') }}
<script type="application/x-javascript">
$(document).ready(function() {
    
    $('.sendME').on('click', function(e){
    e.preventDefault();
        var id = $(this).data('user-id');
        $.ajax({
            type: "POST",
            url: '/chats/sendme/ajax',
            data: {user_to:id},
            success: function(data)
            {
              location.href='/?chat='+data.chat+'&user_to='+id;
            }
        });
    
    });
});
</script>
@stop

@section('content')

<div id="profile">
 @include('profiles.my.head', $user)
 @include('profiles.menu', $user)

<div class="padder">
   <div class="padder-v">
       <div id="friends" class="panel panel-default">
           <div class="panel-heading">
             <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i> Друзья</h3>
           </div>
           <div class="panel-body">
              
              <div class="pull-left"> <!-- required for floating -->
                   <!-- Nav tabs -->
                   <ul class="nav nav-tabs tabs-left">
                     <li class="active"><a href="#friendsAll" data-toggle="tab">Все</a></li>
                     <li><a href="#friendsAdd" data-toggle="tab">Заявки
                      @if($user->friendsReq->count() > 0)
                       <span class="badge bg-danger"> {{ $user->friendsReq->count() }}</span>
                      @endif
                     </a></li>
                   </ul>
               </div>
               
               <div class="pull-left">
                   <!-- Tab panes -->
                   <div class="tab-content">
                     <div class="tab-pane active" id="friendsAll">
                       <a href="{{ route('profiles') }}" class="btn btn-default pull-right">Найти друзей</a>
                       <div class="clearfix"></div>
                       
                       @if($friends)
                        <ul id="friends-list">
                        @foreach($friends as $friend1)
                         <li>
                          <footer>
                             <a class="btn btn-link del" href="{{ route('friends.del', $friend1->id) }}">Удалить</a>
                             <a class="btn btn-link mes sendME" href="javascript:" data-user-id="{{  $friend1->id }}">Написать</a>
                          </footer>
                          <a class="pull-left m-r ava" href="{{ route('user.id', $friend1->id) }}">
                            <img class="ava-big img-circle" alt="{{ $friend1->profile->name }}" src="{{ route('user.avatar', array($friend1->id, 100, 100)) }}">
                          </a>
                          <h4><a href="{{ route('user.id', $friend1->id) }}">{{ $friend1->profile->name }}</a></h4>
                           
                         </li>
                         @endforeach
                        </ul>
                       @else
                        <div class="noresult">
                         <i class="icon-user-following"></i>
                         <p>Нет друзей.</p>
                        </div>
                       @endif
                       
                     </div>
                      
                     <div class="tab-pane" id="friendsAdd">
                       
                       @if($user->friendsReq->count() > 0)
                        
                       <ul id="friends-list">
                        @foreach($user->friendsReq as $friend)
                         <li>
                         <footer>
                             <a class="btn btn-link del" href="{{ route('friends.apply', $friend->profile->user_id) }}">Приять</a>
                             <a class="btn btn-link mes" href="{{ route('friends.cencel', $friend->profile->user_id) }}">Игнорировать</a>
                          </footer>
                          
                          <a class="pull-left m-r ava" href="">
                            <img class="ava-big img-circle" alt="{{ $friend->profile->name }}" src="{{ route('user.avatar', array($friend->profile->user_id, 100, 100)) }}">
                          </a>
                           
                          <h4><a href="{{ route('user.id', $friend->profile->user_id) }}">{{ $friend->profile->name }}</a></h4>
                          
                         </li>
                         @endforeach
                        </ul>
                       @else
                        Нет заявок (.
                       @endif 
                        
                     </div>
                   </div>
               </div>
                
           </div>
       </div><!--#information-->
   </div>
 </div><!------------>
 
 <!------------>
</div>
@stop