@extends('site')

@section('description')
   <meta name="description" content="">
@stop

@section('keywords')
    <meta name="keywords" content="">
@stop

@section('title')
    <title>{{ $user->profile->name }} | {{ Config::get('site.site_name') }}</title>
@stop

@section('google')
    
@stop

@section('script')
{{ HTML::script('themes/site/js/jquery.ui.widget.js') }}
{{ HTML::script('themes/site/js/jquery.fileupload.js') }}
{{ HTML::script('themes/site/js/bootstrap-filestyle.js') }}
{{ HTML::script('themes/site/js/jquery.easy-pie-chart.js') }}
{{ HTML::script('themes/site/js/myprofile.js') }}
@stop

@section('content')

<div id="profile">
 @include('profiles.my.head', $user)
 @include('profiles.menu', $user)
 <!------------>
  <div class="padder">
    <div class="padder-v">
        <div class="panel panel-default">
            <div class="panel-heading">Настройки пароля</div>
            <div class="panel-body">
               {{ Form::open(array('route' => array('user.settings.save', $user->id), 'role' => 'form', 'class' => 'form', 'id' => 'form-user-settings')) }}
               
                    <div class="form-group">
                        <label for="oldpass">Старый пароль</label>
                        {{ Form::password('oldpass', array('class' => 'form-control')) }}
                    </div>
                        
                    <div class="form-group">
                        <label for="newpass">Новый пароль</label>
                        {{ Form::password('newpass', array('class' => 'form-control')) }}
                    </div>
                        
                    <div class="form-group">
                        <label for="newpass_confirmation">Повторить пароль</label>
                        {{ Form::password('newpass_confirmation', array('class' => 'form-control')) }}
                    </div>
               
                     {{ Form::submit('Сохранить', array('class' => 'btn btn-primary')) }}
               
               {{ Form::close() }}
            </div>
        </div>
    </div>
  </div><!------------>
</div>
@stop