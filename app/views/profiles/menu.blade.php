<div class="wrapper bg-white b-b">
    <ul class="nav nav-pills nav-sm">
        {{-- Nav::item('user.id','Хроника', null, array('id' => $user->id), array('append' => '<i class="icon-moustache"></i>')) --}}
        {{ Nav::item('user.id','Информация', null, array('id' => $user->id), array('append' => '<i class="icon-emoticon-smile"></i>')) }}
        {{ Nav::item('user.friends','Друзья', null, array('id' => $user->id), array('append' => '<i class="icon-user-following"></i>', 'prepend' => '<span class="badge">'.Friend::friends($user)->count().'</span>')) }}
        {{ Nav::item('user.photos','Фото', null, array('id' => $user->id), array('append' => '<i class="icon-camera"></i>', 'prepend' => '<span class="badge">'.$user->photos()->count().'</span>')) }}
        @if(Sentry::getUser()->id == $user->id)
        {{ Nav::item('user.messages','Сообщения', null, array('id' => $user->id), array('append' => '<i class="icon-envelope"></i>', 'prepend' => '<span class="badge">'.Chat::countAllNew().'</span>')) }}
        @endif
    </ul>
</div>