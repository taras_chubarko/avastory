@extends('site')

@section('description')
   <meta name="description" content="">
@stop

@section('keywords')
    <meta name="keywords" content="">
@stop

@section('title')
    <title>{{ $user->profile->name }} | {{ Config::get('site.site_name') }}</title>
@stop

@section('script')
{{ HTML::script('themes/site/js/jquery.ui.widget.js') }}
{{ HTML::script('themes/site/js/jquery.fileupload.js') }}
{{ HTML::script('themes/site/js/bootstrap-filestyle.js') }}
{{ HTML::script('themes/site/js/jquery.easy-pie-chart.js') }}
{{ HTML::script('themes/site/js/myprofile.js') }}
@stop

@section('content')

<div id="profile">
 @include('profiles.any.head', $user)
 @include('profiles.menu', $user)
<div id="anyFriends">
 @if($friends)
<ul>
@foreach($friends as $friend)
 <li>
   <a class="ava" href="{{ route('user.id', $friend->id) }}">
     <img class="ava-big img-circle" alt="{{ $friend->profile->name }}" src="{{ route('user.avatar', array($friend->id, 100, 100)) }}">
   </a>
   <div class="name"><a href="{{ route('user.id', $friend->id) }}">{{ $friend->profile->name }}</a></div>
 </li>
@endforeach
</ul>
 <div class="clearfix"></div>
@else
 <div class="noresult">
    <i class="icon-user-following"></i>
    <p>Нет друзей.</p>
 </div>
@endif
 
</div>

 <!------------>
</div>
@stop