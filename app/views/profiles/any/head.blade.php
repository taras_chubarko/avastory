<div id="ubf" style="background:url({{ ($user->profile->fon) ? '/uploads/users/fon/'.$user->profile->fon : 'http://lorempixel.com/1900/800/' }}) center center; background-size:cover">
    <div class="wrapper-lg bg-white-opacity">
      <div class="row m-t">
        <div class="col-sm-7">
          <a class="pull-left m-r" href="">
            <img class="ava-big img-circle" alt="{{ $user->profile->name }}" src="{{ route('user.avatar', array($user->id, 200, 200)) }}">
          </a>
          <div class="clear m-b">
            <div class="m-b m-t-sm names">
              <span class="h3">{{ $user->profile->name }}</span>
              <small class="m-l">{{ $user->profile->adres }}</small>
            </div>
            <p class="m-b">
              <a class="btn btn-sm btn-bg btn-rounded btn-default btn-icon" href=""><i class="fa fa-twitter"></i></a>
              <a class="btn btn-sm btn-bg btn-rounded btn-default btn-icon" href=""><i class="fa fa-facebook"></i></a>
              <a class="btn btn-sm btn-bg btn-rounded btn-default btn-icon" href=""><i class="fa fa-google-plus"></i></a>
            </p>
               
               {{ Friend::friendButton($user->id) }}
               
          </div>
        </div>
        <div class="col-sm-5">
          
        </div>
      </div>
    </div>
</div><!------------>