@extends('site')

@section('description')
   <meta name="description" content="">
@stop

@section('keywords')
    <meta name="keywords" content="">
@stop

@section('title')
    <title>{{ $user->profile->name }} | {{ Config::get('site.site_name') }}</title>
@stop

@section('script')

@stop

@section('content')
                
<div id="profile">

  @include('profiles.any.head', $user)
  @include('profiles.menu', $user)
  
  <div class="padder">
    <div class=" padder-v">
      <p>Векж убяквюэ окюррырэт абхоррэант ут. Ты еюж нобёз клита кончюлату, векж оратио долорэж кончэтытюр йн. Эпикюре ыпикурэи ометтантур ут хаж, пожжэ алёэнюм квюаэчтио еюж ку, дикырыт дэфянятйоныс зыд ку. Квюот льабятюр нолюёжжэ квуй ад, нэ хаж ныморэ пэрпэтюа. Эож квуым чент волуптюа ад.</p>
      
      <p>Ку векж аюдиам пробатуж мныжаркхюм. Вим жольюта пэрчыквюэрёж йн. Эа мэль магна долорэж тинкидюнт, эю омнеж элььэефэнд альиквуандо шэа, мэль йн дёко шапэрэт ёнэрмйщ. Квюо алёа емпэтюсъ жямиляквюы ут, вэниам тимэам торквюатоз прё нэ. Векж ку квюоджё квюаэчтио, мэль лебыр конвынёры эа.</p>
    </div>
  </div><!------------>
</div>
@stop