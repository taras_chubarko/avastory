@extends('site')

@section('description')
   <meta name="description" content="">
@stop

@section('keywords')
    <meta name="keywords" content="">
@stop

@section('title')
    <title>{{ $user->profile->name }} | {{ Config::get('site.site_name') }}</title>
@stop

@section('content')

<div id="profile">
 @include('profiles.any.head', $user)
 @include('profiles.menu', $user)
<div class="padder">
   <div class="padder-v">
       <div id="photos" class="panel panel-default">
           <div class="panel-heading">
             <h3 class="panel-title"><i class="glyphicon glyphicon-camera"></i> {{ $album->name }}</h3>
           </div>
           <div class="panel-body">
             <ul class="photos-ul">
                @if($photos->count())
                    @foreach($photos as $photo)
                        <li>
                            <div class="panel b-a">
                                <a href="javascript:" class="loadPhotoAjax" data-photo-id="{{ $photo->id }}"><img src="{{ route('image.folder', array('photos', $photo->photo, 198, 198, 'center')) }}" alt="{{ $photo->alt }}" title="{{ $photo->title or $album->name }}"></a>
                                <footer class="bottom bg-gd-dk text-white">
                                    <a href="javascript:" class="loadPhotoAjax" data-photo-id="{{ $photo->id }}"><i class="icon-bubbles"></i> Комментарии {{ $photo->comments->count() }}</a>&ensp;
                                    @include('statistics.like-link', array('type' => $photo, 'type_name' => 'photo')) 
                                </footer>
                            </div>
                        </li>
                    @endforeach
                @endif
             </ul>
           </div>
       </div><!--#photos-->
   </div>
 </div><!------------>
 <!------------>
</div>
@stop