<div class="ovrls">
    @if($user->isOnline($user->id))
        <span class="badge bg-success">в сети</span>
    @else
        <span class="badge bg-danger">не сети</span>
    @endif
    
    <img height="150" width="150" src="{{ route('user.avatar', array($user->id, 150, 150)) }}" class="img-circle">
    
    <div class="btn-group btn-group-justified users-btn">
        <a href="{{ route('user.photos', $user->id) }}" title="Фотографий" class="btn btn-primary"><i class="fa fa-camera"></i> {{ $user->photos->count() }}</a>
        @if(Sentry::getUser()->id != $user->id)
        <a href="javascript:" title="Начать переписку" data-user-id="{{ $user->id }}" class="btn btn-info start-mess"><i class="fa fa-comments-o"></i></a>
        @endif
        <a href="{{ route('user.id', $user->id) }}" title="Смотреть профиль" class="btn btn-success"><i class="fa fa-eye"></i></a>
    </div>
        
</div>