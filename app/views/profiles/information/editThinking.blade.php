<!-- Modal -->
<div class="modal fade" id="editThinkingModal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">О чем вы думаете</h4>
      </div>
        {{ Form::open(array('route' => array('user.information.thinking.save', $user->id), 'role' => 'form', 'id' => 'form')) }}
        <div class="modal-body">
            {{ Form::hidden('profile_id', $user->profile->id, array('class' => 'form-control')) }}
            <div class="form-group">
                {{ Form::textarea('thinking', $user->profile->thinking, array('class' => 'form-control', 'rows' => 5)) }}
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
          {{ Form::submit('Сохранить', array('class' => 'btn btn-primary')) }}
            
        </div>
        {{ Form::close() }}
    </div>
  </div>
</div>