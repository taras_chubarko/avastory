<!-- Modal -->
<div class="modal fade" id="editSchoolModal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Школа</h4>
      </div>
        {{ Form::open(array('route' => 'user.profile.save.school', 'role' => 'form')) }}
        <div class="modal-body">
            {{ Form::hidden('profile_id', $user->profile->id, array('class' => 'form-control')) }}
            <div class="form-group">
                {{ Form::label('name', 'Название школы') }}
                {{ Form::text('name', $user->profile->schoolArray()->name, array('class' => 'form-control')) }}
            </div>
            
            <div class="row">
                <div class="col-xs-6">
                    <div class="form-group">
                        {{ Form::label('year_start', 'Год начала учебы') }}
                        {{ Form::text('year_start', $user->profile->schoolArray()->year_start, array('class' => 'form-control')) }}
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="form-group">
                        {{ Form::label('year_end', 'Год окончания учебы') }}
                        {{ Form::text('year_end', $user->profile->schoolArray()->year_end, array('class' => 'form-control')) }}
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
          {{ Form::submit('Сохранить', array('class' => 'btn btn-primary')) }}
            
        </div>
        {{ Form::close() }}
    </div>
  </div>
</div>