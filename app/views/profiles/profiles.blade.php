@extends('site')

@section('description')
   <meta name="description" content="">
@stop

@section('keywords')
    <meta name="keywords" content="">
@stop

@section('title')
    <title>Анкеты | {{ Config::get('site.site_name') }}</title>
@stop

@section('google')
    
@stop

@section('script')
<script type="application/x-javascript">
$(document).ready(function() {
    
    $('.sendME').on('click', function(e){
    e.preventDefault();
        var id = $(this).data('user-id');
        $.ajax({
            type: "POST",
            url: '/chats/sendme/ajax',
            data: {user_to:id},
            success: function(data)
            {
              location.href='/?chat='+data.chat+'&user_to='+id;
            }
        });
    
    });
    
    $('.userrating').rating();
    
});
</script>
@stop

@section('content')
                
<div id="page">
    <div class="panel-body">
        <div class="bg-light lter b-b wrapper-md ng-scope">
            <h1 class="m-n font-thin h3">Анкеты</h1>
        </div>
        <div class="blog-post">
            <div class="panel">
                @if($users->count())
                    <div class="row">
                        @foreach($users as $user)
                            <div class="col-md-3">
                                <div class="panel b-a anketa">
                                    <div style="height: 40px;" class="panel-heading no-border bg-primary">
                                      <!--<button class="vip btn btn-danger"><i class="icon-badge"></i> VIP</button>-->
                                      <span  data-container="body" data-toggle="popover" data-placement="top" data-content="{{ $user->thinking or 'Не думаю' }}" class="text-lt thinking">{{ $user->thinking or '' }}</span>
                                    </div>
                                    <div class="item m-l-n-xxs m-r-n-xxs">
                                      <div class="top text-right padder m-t-xs w-full">
                                        <span class="ng-isolate-scope ng-valid">
                                           <input type="hidden" readonly="readonly" class="userrating" data-filled="fa fa-star fa-2x" data-empty="fa fa-star-o fa-2x" value="{{ $user->stars($user->user_id) }}"/>
                                        </span>
                                      </div>
                                      <div style="margin-top:-60px" class="center text-center w-full">
                                        
                                      </div>
                                      <div class="bottom wrapper bg-gd-dk text-white" style="width: 100%;">            
                                        <div class="text-u-c h3 m-b-sm text-primary-lter">{{ $user->name }}</div>
                                        <div>{{ ($user->age) ? $user->age . DateFormat::num2word($user->age, array(' год', ' года', ' лет')) : '' }}</div>
                                      </div>
                                      <img class="img-full" src="{{ route('user.avatar', array($user->user_id, 300, 300)) }}">
                                    </div>
                                    <div class="hbox text-center b-b b-light text-sm">          
                                      <a class="col padder-v text-muted b-r b-light" href="{{ route('user.photos', $user->user_id) }}">
                                        <i class="icon-camera block m-b-xs fa-2x"></i>
                                        <span>{{ ($user->photos->count() > 0) ? $user->photos->count() : 'Нет фоток' }}</span>
                                      </a>
                                      <a class="col padder-v text-muted b-r b-light sendME" href="javascript:" data-user-id="{{ $user->user_id }}">
                                        <i class="icon-bubbles block m-b-xs fa-2x"></i>
                                        <span>Написать</span>
                                      </a>
                                      <a class="col padder-v text-muted" href="{{ route('user.id', $user->user_id) }}">
                                        <i class="icon-eye  block m-b-xs fa-2x"></i>
                                        <span>Смотреть</span>
                                      </a>
                                    </div>
                                  </div>
                            </div>
                           
                        @endforeach
                    </div>
                     {{ $users->links() }}
                @else
                    Нет анкет. Или незаполнен параметр поиска.
                @endif
            </div>
        </div>
    </div>
</div>
@stop