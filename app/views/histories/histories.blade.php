@extends('site')

@section('description')
   <meta name="description" content="">
@stop

@section('keywords')
    <meta name="keywords" content="">
@stop

@section('title')
    <title>Истории пар | {{ Config::get('site.site_name') }}</title>
@stop

@section('script')

@stop

@section('content')
                
<div id="page">
    <div class="panel-body">
        <div class="bg-light lter b-b wrapper-md ng-scope">
            <h1 class="m-n font-thin h3 pull-left">Истории пар</h1>
            <a class="btn m-b-xs w-xs btn-default pull-right" href="{{ route('histories.create') }}">Добавить</a>
            <div class="clearfix"></div>
        </div>
        <div class="blog-post">
            <div class="panel">
                <div id="articlesAll" class="wrapper-lg">
                    @if($histories->count())
                        @foreach($histories as $history)
                            <div class="rows">
                                @if($history->image)
                                <div class="col-1">
                                    <a href="{{ route('histories.show', $history->slug) }}"><img src="{{ route('image.folder', array('histories', $history->image, 100, 100, 'center')) }}" class="img-thumbnail m-r-md pull-left"></a>
                                </div>
                                @endif
                                <div class="col-2">
                                    <h3 class="m-t-none"><a href="{{ route('histories.show', $history->slug) }}">{{ $history->name }}</a></h3>
                                    <div class="body">{{ str_limit($history->body, $limit = 500, $end = '...') }}</div>
                                </div>
                                <div class="col-3">
                                    <ul class="stats list-inline text-muted">
                                        <li><i class="icon-calendar"></i> {{ date('d.m.Y', strtotime($history->created_at)) }}</li>
                                        <li><i class="icon-user"></i> <a class="text-muted" href="{{ route('user.id', $history->user_id) }}">{{ $history->profile->name }}</a></li>
                                        <li><i class="icon-eye"></i> {{ $history->looks->count() }}</li>
                                        <li class="stat-like">
                                          @include('statistics.like-sm', array('type' => $history, 'type_name' => 'history'))
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        @endforeach
                    {{ $histories->links() }}
                    @else
                        Нет статей.
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@stop