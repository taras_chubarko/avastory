@extends('site')

@section('description')
   <meta name="description" content="">
@stop

@section('keywords')
    <meta name="keywords" content="">
@stop

@section('title')
    <title>Истории редактировать | {{ Config::get('site.site_name') }}</title>
@stop

@section('script')
{{ HTML::script('themes/site/js/jquery.ui.widget.js') }}
{{ HTML::script('themes/site/js/jquery.fileupload.js') }}
{{ HTML::script('themes/site/js/bootstrap-filestyle.js') }}
{{ HTML::script('/tinymce/tinymce.min.js') }}
{{ HTML::script('/tinymce/tinymce_editor.js') }}
<script type="text/javascript">
    editor_config.selector = ".body";
    tinymce.init(editor_config);
     $(document).ready(function() {
       admin.article('histories');
    });
</script>
@stop

@section('content')
                
<div id="pages">
<div style="margin:20px;" class="panel panel-default">
  <div class="panel-heading">
    <h1 class="panel-title">Редактировать историю</h1>
  </div>
{{ Form::open(array('route' => array('admin.histories.update', $history->id), 'method' => 'put', 'role' => 'form', 'id' => 'form-edit-history')) }}
  <div class="panel-body">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs">
      <li class="active"><a href="#home" data-toggle="tab">Основное</a></li>
      <li><a href="#meta" data-toggle="tab">Мета</a></li>
    </ul>
    
    <!-- Tab panes -->
    <div class="tab-content">
      <div class="tab-pane active" id="home">
            
            <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                {{ Form::label('name', 'Заголовок') }}
                {{ Form::text('name', $history->name, array('class' => 'form-control')) }}
            </div>
                
            <div class="form-group {{ $errors->has('body') ? 'has-error' : '' }}">
                {{ Form::label('body', 'Содержание') }}
                {{ Form::textarea('body', $history->body, array('class' => 'form-control body', 'rows' => 10)) }}
            </div>
                
            <div class="form-group">
                {{ Form::label('image_ajax', 'Картинка') }}
                <div class="image-upload">@include('site.upload-image', ['folder' => 'histories', 'filename' => $history->image])</div>
                <input id="input-image" type="file">
            </div>
                
            <div class="form-group">
                {{ Form::label('slug', 'Путь') }}
                {{ Form::text('slug', $history->slug, array('class' => 'form-control')) }}
            </div>
                
            <div class="checkbox">
                <label class="i-checks m-b-md">
                    {{ Form::hidden('status', 0) }}
                    {{ Form::checkbox('status', 1, $history->status) }} <i></i> Опубликовано
                </label>
            </div>
            
      </div>
      <div class="tab-pane" id="meta">
            
            <div class="form-group">
                {{ Form::label('title', 'Заголовок') }}
                {{ Form::text('title', $history->title, array('class' => 'form-control')) }}
            </div>
                
            <div class="form-group">
                {{ Form::label('keywords', 'Ключенвые слова') }}
                {{ Form::text('keywords', $history->keywords, array('class' => 'form-control')) }}
            </div>
                
            <div class="form-group">
                {{ Form::label('description', 'Описание') }}
                {{ Form::textarea('description', $history->description, array('class' => 'form-control', 'rows' => 5)) }}
            </div>
                
      </div>
    </div>

  </div><!--panel-body-->
  <div class="panel-footer">
    {{ HTML::decode(Form::button('<i class="fa fa-floppy-o"></i> Сохранить', array('class' => 'btn btn-dark', 'type' => 'submit'))) }}
  </div>
{{ Form::close() }}
</div>
</div>
@stop