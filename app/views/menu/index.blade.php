@extends('site')

@section('description')
   <meta name="description" content="">
@stop

@section('keywords')
    <meta name="keywords" content="">
@stop

@section('title')
    <title>Меню | {{ Config::get('site.site_name') }}</title>
@stop

@section('script')

@stop

@section('content')
                
<div id="pages">

<div style="margin:20px;" class="panel panel-default">
  <div class="panel-heading">
    <h1 class="panel-title">Меню</h1>
  </div>
  <div class="panel-body">
    <a href="{{ route('admin.menu.create') }}" class="btn btn-success m-b-20"><i class="fa fa-plus"></i> Создать</a>
    @if($menunames->count())
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th width="20">#</th>
                    <th>Название</th>
                    <th width="100">Действия</th>
                </tr>
            </thead>
            <tbody>
                @foreach($menunames as $item)
                <tr>
                    <td>{{ $item->id }}</td>
                    <td>{{ $item->name }}</td>
                    <td>
                        <a href="{{ route('admin.menu.edit', $item->id) }}" title="Редактировать" class="btn btn-sm btn-icon btn-info"><i class="fa fa-pencil"></i></a>
                        <a href="javascript:" data-toggle="modal" data-target="#deleteMenu-{{ $item->id }}" title="Удалить" class="btn btn-sm btn-icon btn-danger"><i class="fa fa-trash-o"></i></a>
                       
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    @else
        <div style="margin:20px;">Нет меню.</div>
    @endif
  </div>
</div>

</div>
@stop