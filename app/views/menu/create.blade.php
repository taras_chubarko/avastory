@extends('site')

@section('description')
   <meta name="description" content="">
@stop

@section('keywords')
    <meta name="keywords" content="">
@stop

@section('title')
    <title>Меню - создать | {{ Config::get('site.site_name') }}</title>
@stop

@section('script')

@stop

@section('content')
                
<div id="pages">

<div style="margin:20px;" class="panel panel-default">
    <div class="panel-heading">
        <h1 class="panel-title">Меню - создать</h1>
    </div>
    <div class="panel-body">
        {{ Form::open(array('route' => 'admin.menu.store', 'role' => 'form', 'class' => 'form')) }}
            <div class="form-group">
                <label for="name">Название меню</label>
                {{ Form::text('name', null, array('class' => 'form-control')) }}
            </div>
                
            {{ Form::submit('Сохранить', array('class' => 'btn btn-primary')) }}
        {{ Form::close() }}
    </div>
</div>

</div>
@stop