@extends('site')

@section('description')
   <meta name="description" content="">
@stop

@section('keywords')
    <meta name="keywords" content="">
@stop

@section('title')
    <title>Сайт знакомств | {{ Config::get('site.site_name') }}</title>
@stop

@section('script')
    {{ HTML::script('themes/site/js/infobox.js') }}
    {{ HTML::script('themes/site/js/logined.js') }}
@stop

@section('body_class')
front
@stop

@section('content')
<div id="front">

<div id="chatMap"></div>
    
<div id="map-canvas"></div>
    
</div>

@stop

