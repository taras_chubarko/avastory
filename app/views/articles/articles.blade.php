@extends('site')

@section('description')
   <meta name="description" content="">
@stop

@section('keywords')
    <meta name="keywords" content="">
@stop

@section('title')
    <title>Статьи | {{ Config::get('site.site_name') }}</title>
@stop

@section('script')

@stop

@section('content')
                
<div id="page">
    <div class="panel-body">
        <div class="bg-light lter b-b wrapper-md ng-scope">
            <h1 class="m-n font-thin h3">Статьи</h1>
        </div>
        <div class="blog-post">
            <div class="panel">
                <div id="articlesAll" class="wrapper-lg">
                    @if($articles->count())
                        @foreach($articles as $article)
                            <div class="rows">
                                @if($article->image)
                                <div class="col-1">
                                    <a href="{{ route('articles.show', $article->slug) }}"><img src="{{ route('image.folder', array('articles', $article->image, 100, 100, 'center')) }}" class="img-thumbnail m-r-md pull-left"></a>
                                </div>
                                @endif
                                <div class="col-2">
                                    <h3 class="m-t-none"><a href="{{ route('articles.show', $article->slug) }}">{{ $article->name }}</a></h3>
                                    <div class="body">{{ str_limit($article->body, $limit = 500, $end = '...') }}</div>
                                </div>
                                <div class="col-3">
                                    <ul class="stats list-inline text-muted">
                                        <li><i class="icon-calendar"></i> {{ date('d.m.Y', strtotime($article->created_at)) }}</li>
                                        <li><i class="icon-user"></i> <a class="text-muted" href="{{ route('user.id', $article->user_id) }}">{{ $article->profile->name }}</a></li>
                                        <li><i class="icon-eye"></i> {{ $article->looks->count() }}</li>
                                        <li class="stat-like">
                                          @include('statistics.like-sm', array('type' => $article, 'type_name' => 'article'))
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        @endforeach
                    {{ $articles->links() }}
                    @else
                        Нет статей.
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@stop