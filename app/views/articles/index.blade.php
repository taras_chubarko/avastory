@extends('site')

@section('description')
   <meta name="description" content="">
@stop

@section('keywords')
    <meta name="keywords" content="">
@stop

@section('title')
    <title>Статьи | {{ Config::get('site.site_name') }}</title>
@stop

@section('script')

@stop

@section('content')
                
<div id="pages">

<div style="margin:20px;" class="panel panel-default">
  <div class="panel-heading">
    <h1 class="panel-title">Статьи</h1>
  </div>
  <div class="panel-body">
    <a style="margin:0 20px 20px 0" href="{{ route('admin.articles.create') }}" class="btn btn-success"><i class="fa fa-plus"></i> Создать</a>
    @if($articles->count())
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Заголовок</th>
                    <th>Путь</th>
                    <th>Дата</th>
                    <th>Статус</th>
                    <th>Действия</th>
                </tr>
            </thead>
            <tbody>
                @foreach($articles as $article)
                <tr>
                    <td>{{ $article->id }}</td>
                    <td><a href="{{ route('articles.show', $article->slug) }}">{{ $article->name }}</a></td>
                    <td>{{ $article->slug }}</td>
                    <td width="160">{{ $article->created_at }}</td>
                    <td>{{ ($article->status == 1) ? 'опубликовано' : 'не опубликовано' }}</td>
                    <td width="100">
                        <a href="{{ route('admin.articles.edit', $article->id) }}" title="Редактировать" class="btn btn-sm btn-icon btn-info"><i class="fa fa-pencil"></i></a>
                        <a href="javascript:" data-toggle="modal" data-target="#deleteArticles-{{ $article->id }}" title="Удалить" class="btn btn-sm btn-icon btn-danger"><i class="fa fa-trash-o"></i></a>
                        @include('articles.delete', $article)
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {{ $articles->links() }}
    @else
        <div style="margin:20px;">Нет страниц.</div>
    @endif
  </div>
</div>

</div>
@stop