@extends('site')

@section('description')
   <meta name="description" content="{{ $article->description or '' }}">
@stop

@section('keywords')
    <meta name="keywords" content="{{ $article->keywords or '' }}">
@stop

@section('title')
    <title>{{ ($article->title) ? $article->title : $article->name }} | {{ Config::get('site.site_name') }}</title>
@stop

@section('script')

@stop

@section('content')
                
<div id="page">
    <div class="panel-body">
        <div class="bg-light lter b-b wrapper-md ng-scope">
            <h1 class="m-n font-thin h3">Статьи</h1>
             @if(Sentry::check())   
                @if(Sentry::getUser()->hasAccess('dashboard.doc'))
                    <a href="{{ route('admin.articles.edit', $article->id) }}" title="Редактировать" class="btn btn-sm btn-icon btn-info pull-right" style="margin-top:-25px;"><i class="fa fa-pencil"></i></a>
                @endif
             @endif
        </div>
        <div class="blog-post">
            <div class="panel">
                <div class="wrapper-lg">
                    <h2 class="m-t-none">{{ $article->name }}</h2>
                    <div>
                        @if($article->image)
                            <img src="{{ route('image.folder', array('articles', $article->image, 350, 350, 'center')) }}" class="img-thumbnail m-r-md pull-left">
                        @endif
                      {{ $article->body }}
                    </div>
                    <div class="line line-lg b-b b-light"></div>
                    <ul class="stats list-inline text-muted">
                        <li><i class="icon-calendar"></i> {{ date('d.m.Y', strtotime($article->created_at)) }}</li>
                        <li><i class="icon-user"></i> <a class="text-muted" href="{{ route('user.id', $article->user_id) }}">{{ $article->profile->name }}</a></li>
                        <li><i class="icon-eye"></i> {{ $article->looks->count() }}</li>
                        <li class="stat-like">
                          @include('statistics.like-sm', array('type' => $article, 'type_name' => 'article'))
                        </li>
                    </ul>
                </div>
                
            </div>
        </div>
    </div>
</div>
@stop