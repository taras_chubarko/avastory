<!-- Modal -->
<div class="modal fade" id="addAlbum" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Добавить альбом</h4>
      </div>
        {{ Form::open(array('route' => 'albums.store', 'role' => 'form', 'class' => 'form')) }}
            {{ Form::hidden('user_id', $user->id) }}
            <div class="modal-body">
              <div class="form-group">
                  <label for="name">Название альбома</label>
                  {{ Form::text('name', null, array('class' => 'form-control')) }}
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
              {{ Form::submit('Сохранить', array('class' => 'btn btn-primary')) }}
            </div>
        {{ Form::close() }}
    </div>
  </div>
</div>