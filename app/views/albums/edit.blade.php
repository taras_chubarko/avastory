<!-- Modal -->
<div class="modal fade" id="editAlbum-{{ $album->id }}" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Редактировать альбом</h4>
      </div>
        {{ Form::open(array('route' => array('albums.update', $album->id), 'role' => 'form', 'class' => 'form', 'method' => 'PUT')) }}
            {{ Form::hidden('user_id', $album->user_id) }}
            <div class="modal-body">
              <div class="form-group">
                  <label for="name">Название альбома</label>
                  {{ Form::text('name', $album->name, array('class' => 'form-control')) }}
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
              {{ Form::submit('Сохранить', array('class' => 'btn btn-primary')) }}
            </div>
        {{ Form::close() }}
    </div>
  </div>
</div>