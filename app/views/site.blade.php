<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">   
    <meta name="_token" content="{{ csrf_token() }}" />
    <meta name="interkassa-verification" content="ed1e17c759674259933b010cb2960c8b" />
    @yield('description')
    @yield('keywords')
    @yield('title')
    <!-- Bootstrap -->
    <!--<link href="css/bootstrap.min.css" rel="stylesheet">-->
    {{ HTML::style('http://fonts.googleapis.com/css?family=Roboto:900,500,300,700,400&subset=latin,cyrillic') }}
    {{ HTML::style('themes/site/css/bootstrap.css') }}
    {{ HTML::style('themes/site/css/animate.css') }}
    {{ HTML::style('themes/site/css/bootstrap.vertical-tabs.min.css') }}
    {{ HTML::style('themes/site/css/font-awesome.min.css') }}
    {{ HTML::style('themes/site/css/simple-line-icons.css') }}
    {{ HTML::style('themes/site/css/bootstrap-datetimepicker.min.css') }}
    {{ HTML::style('themes/site/css/font.css') }}
    {{ HTML::style('themes/site/css/app.css') }}
    {{ HTML::style('themes/site/css/fileinput.min.css') }}
    {{ HTML::style('themes/site/css/site.css') }}
    @yield('style')

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
     
    {{ HTML::script('https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js') }}
    {{ HTML::script('https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places') }}
    {{ HTML::script('themes/site/js/bootstrap.min.js') }}
    {{-- HTML::script('themes/site/js/modal.js') --}}
    {{-- HTML::script('themes/site/js/ui/jquery.ui.map.js') --}}
    {{-- HTML::script('themes/site/js/ui/jquery.ui.map.extensions.js') --}}
    
    {{ HTML::script('themes/site/js/jquery.maskedinput.min.js') }}
    {{ HTML::script('themes/site/js/jquery.validate.min.js') }}
    {{ HTML::script('themes/site/js/moment.min.js') }}
    {{ HTML::script('themes/site/js/locales.min.js') }}
    {{ HTML::script('themes/site/js/bootstrap-datetimepicker.js') }}
    {{ HTML::script('themes/site/js/jquery.form.min.js') }}
    {{ HTML::script('themes/site/js/jquery.slimscroll.js') }}
    {{ HTML::script('themes/site/js/bootstrap-rating.js') }}
    {{ HTML::script('themes/site/js/fileinput.min.js') }}
    {{ HTML::script('themes/site/js/fileinput_locale_ru.js') }}
    {{ HTML::script('themes/site/js/main.js') }}
    @if (Sentry::check())
        <script>
            var Lat = '{{ Sentry::getUser()->profile->lat }}';
            var Lng = '{{ Sentry::getUser()->profile->lng }}';
            var userID = '{{ Sentry::getUser()->id }}';
            var profileID = '{{ Sentry::getUser()->profile->id }}';
            var genderID = '{{ Sentry::getUser()->profile->gender }}';
         </script>
     @endif
     
     @yield('script')
     
  </head>
    
<body class="@yield('body_class')">
  @include('admin.menu')
  <div id="wrapper">
    <header id="header" class="container">
        <a id="logo" class="pull-left" href="/"></a>
        @if (!Sentry::check())
          @include('site.loginBlock')
        @else
          @include('site.userBlock', array('user' => Sentry::getUser()))
        @endif
    </header>
    
    <section id="main" class="container">
    @include('messages.alert')
    <div id="page" class="panel panel-default ng-scope">
            
            @if (!Sentry::check())
              @include('site.menu')
            @else
              @include('site.menuLogined')
            @endif
            
            @yield('content')
            
        </div>
          
    </section>

    <footer class="container">
        <nav id="botton-menu">
            <ul class="nav nav-pills">
                <li><a href="{{ route('pages.view', 'konfidencialnost') }}">Конфиденциальность</a></li>
                <li><a href="{{ route('pages.view', 'garantii') }}">Гарантии</a></li>
                <li><a href="{{ route('pages.view', 'polzovatelskoe-soglashenie') }}">Пользовательское соглашение</a></li>
                <li><a href="{{ route('pages.view', 'sluzhba-podderzhki') }}">Служба поддержки</a></li>
            </ul>
        </nav>
    </footer>
</div>
   
    @if (!Sentry::check())
      @include('site.registerBlock');
    @else
      
    @endif
    


</body>
</html>