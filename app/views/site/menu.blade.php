<div class="panel-heading panel-heading-clear">
    <a class="pull-left btn btn-success" data-toggle="modal" data-target="#registerModal" href="javascript:">Регистрация БЕСПЛАТНО</a>
    <nav id="top-menu" class="pull-right">
        <ul class="nav nav-pills">
        <li class="{{HTML::activeState('avastori')}}"><a href="{{ route('pages.view', 'avastori') }}"><i class="icon-home"></i> Авастори</a></li>
         <li class="dropdown">
             <a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-cup"></i> Услуги <span class="caret"></span></a>
            <ul class="dropdown-menu">
                <li><a href="{{ route('pages.view', 'organizaciya-vstrech-i-dosuga') }}">Организация встреч и досуга</a></li>
                <li><a href="{{ route('pages.view', 'psihologicheskaya-podderzhka') }}">Психологическая поддержка</a></li>
                <li><a href="{{ route('pages.view', 'nash-stilist') }}">Консультации стилиста</a></li>
            </ul>
        </li>
        <li class="{{ HTML::activeState('articles/all') }}"><a href="{{ route('articles') }}"><i class="icon-book-open"></i> Статьи</a></li>
        <li class="{{ HTML::activeState('histories/all') }}"><a href="{{ route('histories') }}"><i class="icon-users"></i> Истории пар</a></li>
    </ul>
    </nav>
    <div class="clearfix"></div>
</div>