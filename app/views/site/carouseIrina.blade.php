<div id="irinaCarousel" class="carousel fdi-Carousel slide">
    <!-- Carousel items -->
       <div class="carousel fdi-Carousel slide" id="eventCarousel" data-interval="0">
           <div class="carousel-inner onebyone-carosel">
               <div class="item active">
                   <div class="col-md-4">
                       <a href="#"><img src="{{ route('image.filemanager', array('1.jpg', 250, 250, 'center')) }}" class="img-responsive center-block"></a>
                   </div>
               </div>
               <div class="item">
                   <div class="col-md-4">
                       <a href="#"><img src="{{ route('image.filemanager', array('2.jpg', 250, 250, 'center')) }}" class="img-responsive center-block"></a>
                   </div>
               </div>
               <div class="item">
                   <div class="col-md-4">
                       <a href="#"><img src="{{ route('image.filemanager', array('3.jpg', 250, 250, 'top')) }}" class="img-responsive center-block"></a>
                   </div>
               </div>
               <div class="item">
                   <div class="col-md-4">
                       <a href="#"><img src="{{ route('image.filemanager', array('4.jpg', 250, 250, 'center')) }}" class="img-responsive center-block"></a>
                   </div>
               </div>
               <div class="item">
                   <div class="col-md-4">
                       <a href="#"><img src="{{ route('image.filemanager', array('5.jpg', 250, 250, 'center')) }}" class="img-responsive center-block"></a>
                   </div>
               </div>
           </div>
           <!--<a class="left carousel-control" href="#eventCarousel" data-slide="prev"></a>-->
           <!--<a class="right carousel-control" href="#eventCarousel" data-slide="next"></a>-->
           <a class="left carousel-control" href="#eventCarousel" data-slide="prev"><i class="glyphicon glyphicon-chevron-left"></i></a>
           <a class="right carousel-control" href="#eventCarousel" data-slide="next"><i class="glyphicon glyphicon-chevron-right"></i></a>
       </div>
       <!--/carousel-inner-->
</div><!--/myCarousel-->