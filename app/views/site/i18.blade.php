<!-- Modal -->
<div class="modal fade" id="myModal18" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title text-center" >Внимание</h4>
      </div>
      <div class="modal-body text-center">
            <p class="text-center" style="font-size: 18px;">Данный сайт содержит информацию, не предназначенную<br>
            для просмотра лицами не достигшими возраста 18 лет</p><br>
            <button type="button" class="btn btn-rose btn-lg" class="close" data-dismiss="modal" aria-hidden="true">Мне есть 18 лет</button><br><br>
            <a href="https://google.ru">Нет, мне ещё не исполнилось 18 лет</a>
      </div>
      
    </div>
  </div>
</div>