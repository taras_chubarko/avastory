<div id="block-user" class="pull-right">
    <ul class="nav navbar-nav navbar-right">
     <li class="dropdown" dropdown="">
        <a href="{{ route('user.friends',  $user->id) }}">
            <i class="icon-user-following fa-fw"></i>
            <span class="visible-xs-inline">Друзья</span>
            @if($user->friendsReq->count() > 0)
                <span class="badge badge-sm up bg-danger pull-right-xs">{{ $user->friendsReq->count() }}</span>
            @endif
        </a>
     </li>
     <li class="dropdown" dropdown="">
        @include('chats.userMessagesMenu', array($user))
     </li>
     <li class="dropdown" dropdown="">
        <a class="dropdown-toggle" href="javascript:" data-toggle="dropdown" aria-expanded="false">
            <span class="thumb-sm avatar pull-right m-t-n-sm m-b-n-sm m-l-sm">
                <img class="ava-smal" alt="{{ $user->profile->name }}" src="{{ route('user.avatar', array($user->id, 50, 50)) }}">
                <i class="on md b-white bottom"></i>
            </span>
            <span class="hidden-sm hidden-md">{{ $user->email }}</span>
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu animated fadeInRight w">
              <li><a href="{{ route('user.settings', $user->id) }}"><span>Настройки</span></a></li>
              <li><a href="{{ route('user.id', $user->id) }}">Профиль</a></li>
              <li class="divider"></li>
              <li><a href="{{ route('user.logout') }}">Выход</a></li>
        </ul>
     </li>
          
    </ul>
</div>