<div id="block-login" class="pull-right">
    {{ Form::open(array('route' => 'user.login', 'role' => 'form', 'class' => 'form-inline')) }}       
        <div class="form-group">
          <label for="email" class="sr-only">Почта</label>
          {{ Form::email('email', null, array('id' => 'email', 'class' => 'form-control', 'placeholder' => 'Почта')) }}
        </div>
        <div class="form-group">
          <label for="password" class="sr-only">Пароль</label>
          {{ Form::password('password', array('id' => 'password', 'class' => 'form-control', 'placeholder' => 'Пароль')) }}
        </div>
        <button class="btn btn-rose" type="submit"><i class="icon-login"></i> Войти</button>
    {{ Form::close() }}
</div>