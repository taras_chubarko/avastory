<!-- Modal -->
<div class="modal fade" id="registerModal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Регистрация</h4>
      </div>
        
      {{ Form::open(array('route' => 'user.register', 'role' => 'form', 'id' => 'form-register', 'class' => 'form')) }}
          <div class="modal-body">
                  
                  <div class="form-group pull-in clearfix">
                    <div class="col-sm-6">
                      <label>Я</label>&emsp;
                      <div id="gender-group" class="btn-group" data-toggle="buttons">
                       <label class="btn btn-default">
                         {{ Form::radio('gender', 1) }}<i class="icon-symbol-male"></i> Мужчина
                       </label>
                       <label class="btn btn-default">
                         {{ Form::radio('gender', 2) }}<i class="icon-symbol-female "></i> Женщина
                       </label>
                     </div>
                    </div>
                    <div class="col-sm-6">
                      <label>Ищу</label>&emsp;
                      <div id="gender-search-group" class="btn-group" data-toggle="buttons">
                       <label class="btn btn-default">
                         {{ Form::radio('gender_search', 1) }}<i class="icon-symbol-male"></i> Мужчину
                       </label>
                       <label class="btn btn-default">
                         {{ Form::radio('gender_search', 2) }}<i class="icon-symbol-female "></i> Женщину
                       </label>
                     </div>
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label>Имя</label>
                    <div class="input-group m-b">
                      <span class="input-group-addon"><i class="icon-user"></i></span>
                      {{ Form::text('name', null, array('class' => 'form-control')) }}
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label>Почта</label>
                    <div class="input-group m-b">
                      <span class="input-group-addon"><i class="icon-envelope-open"></i></span>
                       {{ Form::email('email', null, array('class' => 'form-control')) }}
                    </div>
                  </div>
                   
                  <div class="pull-in clearfix">
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label>Пароль</label>
                        <div class="input-group m-b">
                          <span class="input-group-addon"><i class="icon-lock"></i></span>
                          {{ Form::password('password', array('class' => 'form-control', 'id' => 'password')) }}
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label>Повторить пароль</label>
                          <div class="input-group m-b">
                            <span class="input-group-addon"><i class="icon-lock"></i></span>
                            {{ Form::password('password_confirmation', array('class' => 'form-control')) }}
                          </div>
                      </div>
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label>Я жииву в</label>
                    <div class="input-group m-b">
                      <span class="input-group-addon"><i class="icon-pointer"></i></span>
                      {{ Form::text('adres', null, array('class' => 'form-control', 'id' => 'geocomplete', 'placeholder' => 'Например: Москва')) }}
                      {{ Form::hidden('city', null) }}
                      {{ Form::hidden('lat', null) }}
                      {{ Form::hidden('lng', null) }}
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label>Телефон</label>
                    <div class="input-group m-b">
                      <span class="input-group-addon"><i class="icon-call-end"></i></span>
                      {{ Form::text('telephone', null, array('class' => 'form-control', 'id' => 'tel', 'placeholder' => '+X(XXX) XXXX XXX')) }}
                    </div>
                  </div>
                    
                  <div class="form-group">
                    <div class="checkbox">
                      <label class="i-checks">
                        {{ Form::checkbox('pravila', 1) }}<i></i> Я принимаю условия <a class="text-info" href="{{ route('pages.view', 'polzovatelskoe-soglashenie') }}">пользовательского соглашения</a>
                      </label>
                    </div>
                  </div>
          </div>
          <div class="modal-footer">
              {{ Form::submit('Регистрация', array('class' => 'btn btn-rose')) }}
          </div>
       {{ Form::close() }}
       
    </div>
  </div>
</div>