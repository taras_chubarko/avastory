<!-- Modal -->
<div class="modal fade" id="poiskModal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Я ищу</h4>
      </div>
        {{ Form::open(array('route' => 'profiles', 'id' => 'searchForm', 'method' => 'GET', 'role' => 'form', 'class' => 'form-validation ng-pristine ng-invalid ng-invalid-required ng-valid-pattern ng-valid-email ng-valid-validator')) }}
        <div class="modal-body">
            
                <div class="form-group pull-in clearfix">
                    <div class="col-sm-6">
                      <label>Я</label>&emsp;
                      <div class="btn-group" data-toggle="buttons">
                       <label class="btn btn-default">
                         {{ Form::radio('mygender', 1, (Sentry::getUser()->profile->gender == 1) ? true : false) }}<i class="icon-symbol-male"></i> Мужчина
                       </label>
                       <label class="btn btn-default">
                         {{ Form::radio('mygender', 2, (Sentry::getUser()->profile->gender == 2) ? true : false) }}<i class="icon-symbol-female "></i> Женщина
                       </label>
                     </div>
                    </div>
                    <div class="col-sm-6">
                      <label>Ищу</label>&emsp;
                      <div class="btn-group" data-toggle="buttons">
                       <label class="btn btn-default">
                         {{ Form::radio('gender', 1) }}<i class="icon-symbol-male"></i> Мужчину
                       </label>
                       <label class="btn btn-default">
                         {{ Form::radio('gender', 2) }}<i class="icon-symbol-female "></i> Женщину
                       </label>
                     </div>
                    </div>
                </div>
                
                <div class="form-group">
                    <label>Город</label>
                    <div class="input-group m-b">
                      <span class="input-group-addon"><i class="icon-pointer"></i></span>
                      {{ Form::text('city', null, array('id' => 'geocomplete2', 'class' => 'form-control', 'placeholder' => 'Например: Москва')) }}
                    </div>
                </div>
                
                <div class="form-group pull-in clearfix">
                    <div class="col-sm-6">
                      <label>Впзраст от</label>&emsp;
                       {{ Form::text('age[min]', null, array('class' => 'form-control', 'placeholder' => 'От')) }}
                    </div>
                    <div class="col-sm-6">
                      <label>до</label>&emsp;
                       {{ Form::text('age[max]', null, array('class' => 'form-control', 'placeholder' => 'До')) }}
                    </div>
                </div>
                
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
        {{ HTML::decode(Form::button('<i class="fa fa-search"></i> Поиск', array('class' => 'btn btn-primary', 'type' => 'submit'))) }}
        </div>
      {{ Form::close() }}
    </div>
  </div>
</div>
 <script type="application/x-javascript">
 $(document).ready(function() {
    
    var input = document.getElementById('geocomplete2');         
    var autocomplete = new google.maps.places.Autocomplete(input, {
        types: ["geocode"]
    });
    
    google.maps.event.addListener(autocomplete, 'place_changed', function(event) {
           findAddress();
    });
    
    function findAddress() {
          // Get the place details from the autocomplete object.
          var place = autocomplete.getPlace();
          //$('input[name="lat"]').val(place.geometry.location.lat());
          //$('input[name="lng"]').val(place.geometry.location.lng());
          for (var i = 0; i < place.address_components.length; i++) {
               
               var addressType = place.address_components[i].types[0];
               if (addressType == 'locality') {
                 $('#searchForm input[name="city"]').val(place.address_components[i].long_name);
               }
          }
     }
     
 });
 </script> 
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  