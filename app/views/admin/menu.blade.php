@if( Sentry::check() )
	
        @if(Sentry::getUser()->hasAccess('dashboard.view'))
		<style>
			#wrapper{margin-top: 30px;}
		</style>
                <nav id="admin-menu" class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                    <div class="container-fluid">
                     @include('admin.admin-menu', array('user' => Sentry::getUser()))
                   </div>
                </nav>
	@endif
@endif

