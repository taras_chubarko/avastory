<ul class="nav navbar-nav">
        <li><a  href="{{ route('front') }}"><i class="fa fa-home"></i> Главная</a></li>

        @if($user->hasAccess('dashboard.doc'))
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-file-text-o"></i> Документы <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="{{ route('admin.pages.index') }}">Страници</a></li>
            <li><a href="{{ route('admin.uslugis.index') }}">Услуги</a></li>
            <li><a href="{{ route('admin.articles.index') }}">Статьи</a></li>
            <li><a href="{{ route('admin.histories.index') }}">Истории пар</a></li>
          </ul>
        </li>
        @endif
        
        @if($user->hasAccess('dashboard.users'))
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-sitemap"></i> Структура <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="{{ route('admin.menu.index') }}">Меню</a></li>
          </ul>
        </li>
        @endif
       
        @if($user->hasAccess('dashboard.users'))
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-group"></i> Пользователи <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="{{ route('admin.users.index') }}">Пользователи</a></li>
          </ul>
        </li>
        @endif
        
        <li class="pull-right"><a  href="{{ route('user.logout') }}"><i class="fa fa-sign-out"></i> Выход</a></li>
</ul>