<!-- Modal -->
<div class="modal fade" id="deleteUser-{{ $user->id }}" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Удалить пользователя?</h4>
      </div>
       {{ Form::open(array('route' => array('admin.users.destroy', $user->id), 'method' => 'delete', 'role' => 'form')) }} 
            <div class="modal-body text-center">
              <i class="fa fa-warning text-danger" style="font-size: 72px;"></i><br>
              <p>Внимание! Вы пытаетесь удалить пользователя.<br> После удаления восстановыть будет нельзя.</p>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
              {{ HTML::decode(Form::button('<i class="fa fa-trash-o"></i> Удалить', array('class' => 'btn btn-danger', 'type' => 'submit'))) }}
            </div>
        {{ Form::close() }}
    </div>
  </div>
</div>