@extends('site')

@section('description')
   <meta name="description" content="">
@stop

@section('keywords')
    <meta name="keywords" content="">
@stop

@section('title')
    <title>Создать пользователя | {{ Config::get('site.site_name') }}</title>
@stop

@section('google')
    {{ HTML::script('https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places') }}
@stop

@section('script')
    {{ HTML::script('themes/site/js/infobox.js') }}
    {{ HTML::script('themes/site/js/site.js') }}
@stop

@section('content')
                
<div id="pages">

<div style="margin:20px;" class="panel panel-default">
  <div class="panel-heading">
    <h1 class="panel-title">Создать пользователя</h1>
  </div>
  <div class="panel-body">
    {{ Form::open(array('route' => 'admin.users.store', 'role' => 'form', 'class' => 'form')) }}
        
        <div class="form-group">
            <label for="name">Имя</label>
            {{ Form::text('name', null, array('class' => 'form-control')) }}
        </div>
            
        <div class="form-group">
            <label for="email">Почта</label>
            {{ Form::email('email', null, array('class' => 'form-control')) }}
        </div>
            
        <div class="form-group">
            <label for="password">Пароль</label>
            {{ Form::password('password', array('class' => 'form-control')) }}
        </div>
            
            
        <div class="form-group">
            <label for="adres">Живет</label>
            {{ Form::text('adres', null, array('class' => 'form-control', 'id' => 'geocomplete')) }}
        </div>
            
        <div class="form-group">
            <label for="telephone">Телефон</label>
            {{ Form::text('telephone', null, array('class' => 'form-control')) }}
        </div>
            
        <div class="form-group pull-in clearfix">
            <div class="col-sm-6">
              <label>Пол</label>&emsp;
              <div id="gender-group" class="btn-group" data-toggle="buttons">
               <label class="btn btn-default">
                 {{ Form::radio('gender', 1) }}<i class="icon-symbol-male"></i> Мужчина
               </label>
               <label class="btn btn-default">
                 {{ Form::radio('gender', 2) }}<i class="icon-symbol-female "></i> Женщина
               </label>
             </div>
            </div>
            <div class="col-sm-6">
              <label>Ищет пол</label>&emsp;
              <div id="gender-search-group" class="btn-group" data-toggle="buttons">
               <label class="btn btn-default">
                 {{ Form::radio('gender_search', 1) }}<i class="icon-symbol-male"></i> Мужчину
               </label>
               <label class="btn btn-default">
                 {{ Form::radio('gender_search', 2) }}<i class="icon-symbol-female "></i> Женщину
               </label>
             </div>
            </div>
        </div>
        {{ Form::hidden('city', null) }}   
        {{ Form::hidden('lat', null) }}
        {{ Form::hidden('lng', null) }}
        
        {{ Form::submit('Сохранить', array('class' => 'btn btn-primary')) }}
        
    {{ Form::close() }}
    
  </div>
</div>

</div>
@stop