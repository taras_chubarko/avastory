@extends('site')

@section('description')
   <meta name="description" content="">
@stop

@section('keywords')
    <meta name="keywords" content="">
@stop

@section('title')
    <title>Пользователи | {{ Config::get('site.site_name') }}</title>
@stop

@section('script')

@stop

@section('content')
                
<div id="pages">

<div style="margin:20px;" class="panel panel-default">
  <div class="panel-heading">
    <h1 class="panel-title">Пользователи</h1>
  </div>
  <div class="panel-body">
    <a href="{{ route('admin.users.create') }}" class="btn btn-success m-b-20"><i class="fa fa-plus"></i> Создать</a>
    
    @if($admins->count())
        <h2>Администраторы</h2>
            
        {{ Form::open(array('route' => 'admin.set.admin', 'role' => 'form', 'class' => 'form m-b-20')) }}
            <div class="form-group">
                <label for="email">Сделать пользователя админом</label>
                {{ Form::email('email', null, array('class' => 'form-control', 'placeholder' => 'Почта пользователя')) }}
            </div>
            {{ Form::submit('Применить', array('class' => 'btn btn-primary')) }}
        {{ Form::close() }}
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Почта / Логин</th>
                    <th>Дата</th>
                    <th>Статус</th>
                    <th>Действия</th>
                </tr>
            </thead>
            <tbody>
                @foreach($admins as $admin)
                <tr>
                    <td>{{ $admin->id }}</td>
                    <td><a href="javascript:">{{ $admin->email }}</a></td>
                    <td>{{ $admin->created_at }}</td>
                    <td>{{ $admin->status($admin->activated) }}</td>
                    <td>
                        <a href="{{ route('user.settings', $admin->id) }}" title="Редактировать" class="btn btn-sm btn-icon btn-info"><i class="fa fa-pencil"></i></a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    @endif
    
    
    
    @if($users->count())
        <h2>Пользователи</h2>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Почта / Логин</th>
                    <th>Дата</th>
                    <th>Статус</th>
                    <th>Действия</th>
                </tr>
            </thead>
            <tbody>
                @foreach($users as $user)
                <tr>
                    <td>{{ $user->id }}</td>
                    <td><a href="javascript:">{{ $user->email }}</a></td>
                    <td>{{ $user->created_at }}</td>
                    <td>{{ $user->status($user->activated) }}</td>
                    <td>
                        <a href="{{ route('user.settings', $user->id) }}" title="Редактировать" class="btn btn-sm btn-icon btn-info"><i class="fa fa-pencil"></i></a>
                        <a href="javascript:" data-toggle="modal" data-target="#deleteUser-{{ $user->id }}" title="Удалить" class="btn btn-sm btn-icon btn-danger"><i class="fa fa-trash-o"></i></a>
                       @include('admin.users.delete', $user)
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {{ $users->links() }}
    @else
        <div style="margin:20px;">Нет пользователей.</div>
    @endif
  </div>
</div>

</div>
@stop