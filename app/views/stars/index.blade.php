<div class="stars pull-right">
    <div class="pull-left" style="margin-right:10px;">
        <a class="staritem" href="javascript:" data-type="{{ $type }}" data-type_id="{{ $type_id }}" data-value="5"><i class="fa fa-star-o"></i></a>
        <a class="staritem" href="javascript:" data-type="{{ $type }}" data-type_id="{{ $type_id }}" data-value="4"><i class="fa fa-star-o"></i></a>
        <a class="staritem" href="javascript:" data-type="{{ $type }}" data-type_id="{{ $type_id }}" data-value="3"><i class="fa fa-star-o"></i></a>
        <a class="staritem" href="javascript:" data-type="{{ $type }}" data-type_id="{{ $type_id }}" data-value="2"><i class="fa fa-star-o"></i></a>
        <a class="staritem" href="javascript:" data-type="{{ $type }}" data-type_id="{{ $type_id }}" data-value="1"><i class="fa fa-star-o"></i></a>
    </div>
    <div class="pull-left">
        <a class="starplus" href="javascript:" data-type="{{ $type }}" data-type_id="{{ $type_id }}" data-value="1"><span>+5 <i class="fa fa-star-o"></i></span></a>
    </div>
    <div class="clearfix"></div>
</div>