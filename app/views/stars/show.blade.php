<div class="stars pull-right">
    <div class="pull-left" style="margin-right:10px;">
        <input type="hidden" {{ $readonly }} class="rating" data-type="{{ $type }}" data-type_id="{{ $type_id }}" data-filled="fa fa-star fa-3x" data-empty="fa fa-star-o fa-3x" value="{{ $rating }}"/>
    </div>
    
    <div class="clearfix"></div>
</div>
    
