<?php

return array(
             
    'email'                   => 'E-mail',
    'password'                => 'Password',
    'password_confirmation'   => 'Password confirmation',
    'first_name'              => 'Lirst name',
    'last_name'               => 'Last name',
    'register'                => 'Register',
    'enter'                   => 'Login',
    'fogot_password'          => 'Fogot password?',
    'thanks'                  => 'Thank you for registering on the site. Follow the instructions on your mail.',
    'user_est'                => 'A user with this email is already registered.',
    'register_on_site'        => 'Register on the site',
    'success_activation'      => 'You have successfully activated your profile.',
    'error_activation'        => 'Activation fails.',
    'warning_activation'      => 'User not found. Or already activated.',
    'info_activation'         => 'The user is already activated.',
    'success_login'           => 'You have successfully logged in!',
    'telefon'                 => 'Telefon',
    'save'                    => 'Save',
    'close'                   => 'Close',
    'add_more'                => 'Add more',
    'send'                    => 'Send',
);