<?php

return array(
             
    'email'                   => 'Почта',
    'password'                => 'Пароль',
    'password_confirmation'   => 'Повтор пароля',
    'first_name'              => 'Как Вас зовут?',
    'last_name'               => 'Ваша фамилия',
    'register'                => 'Регистрация',
    'enter'                   => 'Вход',
    'fogot_password'          => 'Забыли пароль?',
    'thanks'                  => 'Спасибо за регистрацию на сайте. Следуйте дальнейшим указаниям у Вас на почте.',
    'user_est'                => 'Пользователь с такой почтой уже зарегистрирован.',
    'register_on_site'        => 'Регистрация на сайте',
    'success_activation'      => 'Вы успешно активировали свой профиль.',
    'error_activation'        => 'Неудачная активация.',
    'warning_activation'      => 'Пользователь ненайден. Или уже активирован.',
    'info_activation'         => 'Пользователь уже активирован.',
    'success_login'           => 'Вы успешно авторизовались!',
    'telefon'                 => 'Телефон',
    'save'                    => 'Сохранить',
    'close'                   => 'Закрыть',
    'add_more'                => 'Добавить еще',
    'send'                    => 'Отправить',
);