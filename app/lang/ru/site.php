<?php

return array(
    
        'work' => array(
            'company'     => 'Компания',         
            'office'      => 'Должность',  
            'city'        => 'Город',  
            'year_start'  => 'Начал работать в',  
            'year_end'    => 'Уволился',  
        ),
        
        'vuz' => array(
            'name'        => 'Название ВУЗа',         
            'specialty'   => 'Специальность',  
            'city'        => 'Город',  
            'year_start'  => 'Год начала учебы',  
            'year_end'    => 'Год окончания учебы',  
        ),
        
        'school' => array(
            'name'        => 'Название школы',         
            'year_start'  => 'Год начала учебы',  
            'year_end'    => 'Год окончания учебы',  
        ),
    
    
);