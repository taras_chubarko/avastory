<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />    
    <title>Avastory.ru</title>

    <!-- Bootstrap -->
    <!--<link href="css/bootstrap.min.css" rel="stylesheet">-->
    <link href='http://fonts.googleapis.com/css?family=Roboto:900,500,300,700,400&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="css/bootstrap.css" type="text/css" />
    <link rel="stylesheet" href="css/animate.css" type="text/css" />
    <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css" />
    <link rel="stylesheet" href="css/simple-line-icons.css" type="text/css" />
    <link rel="stylesheet" href="css/font.css" type="text/css" />
    <link rel="stylesheet" href="css/app.css" type="text/css" />
    <link rel="stylesheet" href="css/site.css" type="text/css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  <div id="wrapper">
    <header id="header" class="container">
        <a id="logo" class="pull-left" href=""></a>
        <?php require __DIR__.'/blocks/login.php'; ?>
    </header>
    
    <section id="main" class="container">
        <div id="front" class="panel panel-default ng-scope">
            <div class="panel-heading panel-heading-clear">
                <a class="pull-left btn btn-success" data-toggle="modal" data-target="#registerModal" href="javascript:">Регистрация БЕСПЛАТНО</a>
                <nav id="top-menu" class="pull-right">
                    <ul class="nav nav-pills">
                    <li><a href=""><i class="icon-home"></i> Авастори</a></li>
                     <li class="dropdown">
                         <a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-cup"></i> Услуги <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="">Организация встреч и досуга</a></li>
                            <li><a href="">Психологическая поддержка</a></li>
                            <li><a href="">Консультации стилиста</a></li>
                        </ul>
                    </li>
                    <li><a href=""><i class="icon-book-open"></i> Статьи</a></li>
                    <li><a href=""><i class="icon-users"></i> Истории пар</a></li>
                </ul>
                </nav>
                <div class="clearfix"></div>
            </div>
            <div id="map-canvas"></div>
            
        </div>
        
    </section>
    <footer class="container">
        <nav id="botton-menu">
            <ul class="nav nav-pills">
                <li><a href="#">Конфиденциальность</a></li>
                <li><a href="#">Гарантии</a></li>
                <li><a href="#">Пользовательское соглашение</a></li>
                <li><a href="#">Служба поддержки</a></li>
            </ul>
        </nav>
        <div id="soc">
            <script type="text/javascript">(function() {
            if (window.pluso)if (typeof window.pluso.start == "function") return;
            if (window.ifpluso==undefined) { window.ifpluso = 1;
              var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
              s.type = 'text/javascript'; s.charset='UTF-8'; s.async = true;
              s.src = ('https:' == window.location.protocol ? 'https' : 'http')  + '://share.pluso.ru/pluso-like.js';
              var h=d[g]('body')[0];
              h.appendChild(s);
            }})();</script>
            <div class="pluso" data-background="transparent" data-options="big,round,line,horizontal,nocounter,theme=04" data-services="vkontakte,odnoklassniki,facebook,twitter,google"></div>
        </div>
    </footer>
</div>  
    <?php require __DIR__.'/blocks/register.php'; ?>
    <?php require __DIR__.'/blocks/18.php'; ?>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places"></script>
    <!--<script src="http://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places"></script>-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/infobox.js"></script>
    <script src="js/jquery.ui.map.js"></script>
    <script src="js/modal.js"></script>
    <script src="js/jquery.geocomplete.js"></script>
    <script src="js/site.js"></script>
  </body>
</html>