<!-- Modal -->
<div class="modal fade" id="poiskModal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Найти партнера</h4>
      </div>
      <form class="form-validation ng-pristine ng-invalid ng-invalid-required ng-valid-pattern ng-valid-email ng-valid-validator" name="form">
        <div class="modal-body">
            
                <div class="form-group pull-in clearfix">
                    <div class="col-sm-6">
                      <label>Я</label>&emsp;
                      <div class="btn-group" data-toggle="buttons">
                       <label class="btn btn-default">
                         <input type="radio" name="options" id="option1"><i class="icon-symbol-male"></i> Мужчина
                       </label>
                       <label class="btn btn-default">
                         <input type="radio" name="options" id="option2"><i class="icon-symbol-female "></i> Женщина
                       </label>
                     </div>
                    </div>
                    <div class="col-sm-6">
                      <label>Ищу</label>&emsp;
                      <div class="btn-group" data-toggle="buttons">
                       <label class="btn btn-default">
                         <input type="radio" name="options" id="option1"><i class="icon-symbol-male"></i> Мужчину
                       </label>
                       <label class="btn btn-default">
                         <input type="radio" name="options" id="option2"><i class="icon-symbol-female "></i> Женщину
                       </label>
                     </div>
                    </div>
                </div>
                
                <div class="form-group">
                    <label>Город</label>
                    <div class="input-group m-b">
                      <span class="input-group-addon"><i class="icon-pointer"></i></span>
                      <input  id="geocomplete" type="text" required=""  placeholder="Например: Москва" class="form-control ng-pristine ng-untouched ng-invalid ng-invalid-required ng-valid-pattern">
                    </div>
                </div>
                
                <div class="form-group pull-in clearfix">
                    <div class="col-sm-6">
                      <label>Впзраст от</label>&emsp;
                       <input type="text" required="" placeholder="От" class="form-control ng-pristine ng-untouched ng-invalid ng-invalid-required ng-valid-pattern">
                    </div>
                    <div class="col-sm-6">
                      <label>до</label>&emsp;
                      <input type="text" required="" placeholder="До" class="form-control ng-pristine ng-untouched ng-invalid ng-invalid-required ng-valid-pattern">
                    </div>
                </div>
                
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
          <button type="button" class="btn btn-primary"><i class="fa fa-search"></i> Поиск</button>
        </div>
      </form>
    </div>
  </div>
</div>