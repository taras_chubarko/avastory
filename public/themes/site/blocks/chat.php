<div id="chat" class="panel panel-default">
    <div class="panel-heading">Переписка</div>
        <div class="panel-body">
          <div class="m-b">
            <a class="pull-left thumb-sm avatar" href=""><img src="http://lorempixel.com/50/50/people/"></a>
            <div class="m-l-xxl">
              <div class="pos-rlt wrapper b b-light r r-2x">
                <span class="arrow left pull-up"></span>
                <p class="m-b-none">Квуым нолюёжжэ пробатуж ат квюо. Вим эа дектаж апыирёан.</p>
              </div>
              <small class="text-muted"><i class="fa fa-ok text-success"></i> 2 минуты назад</small>
            </div>
          </div>
          <div class="m-b">
            <a class="pull-right thumb-sm avatar" href=""><img src="http://lorempixel.com/50/50/people/"></a>
            <div class="m-r-xxl">
              <div class="pos-rlt wrapper bg-primary r r-2x">
                <span class="arrow right pull-up arrow-primary"></span>
                <p class="m-b-none">Ельлюд ёнанй дэбетиз квуй эа, йн ыльит юллюм мюкиуж зыд. Квуым коммодо такематыш ат ыюм, шэа июварыт ютроквюы эффякиантур ку. Витаэ щольыат иреуры эи шэа, вэл ёнанй губэргрэн ут. Кюм ан мацим латины алььтэрюм.<br>:)</p>
              </div>
              <small class="text-muted">1 минуту назад</small>
            </div>
          </div>                          
        </div>
        <footer class="panel-footer">
          <!-- chat form -->
          <div>
            <a class="pull-left thumb-xs avatar"><img src="http://lorempixel.com/50/50/people/"></a>
            <form class="m-b-none m-l-xl ng-pristine ng-valid">
              <div class="input-group">
                <input type="text" placeholder="Напишите что нибуть" class="form-control">
                <span class="input-group-btn">
                  <button type="button" class="btn btn-default">ОТПРАВИТЬ</button>
                </span>
              </div>
            </form>
          </div>
    </footer>
</div>