<!-- Modal -->
<div class="modal fade" id="registerModal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Регистрация</h4>
      </div>
      <form name="form">
          <div class="modal-body">
                  
                  <div class="form-group pull-in clearfix">
                    <div class="col-sm-6">
                      <label>Я</label>&emsp;
                      <div class="btn-group" data-toggle="buttons">
                       <label class="btn btn-default">
                         <input type="radio" name="options" id="option1"><i class="icon-symbol-male"></i> Мужчина
                       </label>
                       <label class="btn btn-default">
                         <input type="radio" name="options" id="option2"><i class="icon-symbol-female "></i> Женщина
                       </label>
                     </div>
                    </div>
                    <div class="col-sm-6">
                      <label>Ищу</label>&emsp;
                      <div class="btn-group" data-toggle="buttons">
                       <label class="btn btn-default">
                         <input type="radio" name="options" id="option1"><i class="icon-symbol-male"></i> Мужчину
                       </label>
                       <label class="btn btn-default">
                         <input type="radio" name="options" id="option2"><i class="icon-symbol-female "></i> Женщину
                       </label>
                     </div>
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label>Имя</label>
                    <div class="input-group m-b">
                      <span class="input-group-addon"><i class="icon-user"></i></span>
                      <input type="text" required="" ng-pattern="/^[a-zA-Z0-9]{4,10}$/" ng-model="user.name" class="form-control ng-pristine ng-untouched ng-invalid ng-invalid-required ng-valid-pattern">
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label>Почта</label>
                    <div class="input-group m-b">
                      <span class="input-group-addon"><i class="icon-envelope-open"></i></span>
                      <input type="email" required="" ng-model="user.email" class="form-control ng-pristine ng-untouched ng-valid-email ng-invalid ng-invalid-required">
                    </div>
                  </div>
                   
                  <div class="form-group pull-in clearfix">
                    <div class="col-sm-6">
                      <label>Пароль</label>
                      <div class="input-group m-b">
                        <span class="input-group-addon"><i class="icon-lock"></i></span>
                        <input type="password" required="" ng-model="password" name="password" class="form-control ng-pristine ng-untouched ng-invalid ng-invalid-required">   
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <label>Повторить пароль</label>
                      <div class="input-group m-b">
                        <span class="input-group-addon"><i class="icon-lock"></i></span>
                        <input type="password" ui-validate-watch=" 'password' " ui-validate=" '$value==password' " ng-model="confirm_password" required="" name="confirm_password" class="form-control ng-pristine ng-untouched ng-valid-validator ng-invalid ng-invalid-required">
                      </div>
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label>Город</label>
                    <div class="input-group m-b">
                      <span class="input-group-addon"><i class="icon-pointer"></i></span>
                      <input  id="geocomplete" type="text" required=""  placeholder="Например: Москва" class="form-control ng-pristine ng-untouched ng-invalid ng-invalid-required ng-valid-pattern">
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label>Телефон</label>
                    <div class="input-group m-b">
                      <span class="input-group-addon"><i class="icon-call-end"></i></span>
                      <input id="tel" type="text" required="" ng-pattern="/\([0-9]{3}\) ([0-9]{3}) ([0-9]{3})$/" ng-model="phone" placeholder="(XXX) XXXX XXX" class="form-control ng-pristine ng-untouched ng-invalid ng-invalid-required ng-valid-pattern">
                    </div>
                  </div>
                  <div class="checkbox">
                    <label class="i-checks">
                      <input type="checkbox" required="" ng-model="agree" class="ng-pristine ng-untouched ng-invalid ng-invalid-required"><i></i> Я принимаю условия <a class="text-info" href="">лицензионного соглашения</a>
                    </label>
                  </div>
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-rose">Регистрация</button>
          </div>
       </form>
    </div>
  </div>
</div>