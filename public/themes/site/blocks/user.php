<div id="block-user" class="pull-right">
    <ul class="nav navbar-nav navbar-right">
     <li class="dropdown" dropdown="">
        <a class="dropdown-toggle" href="javascript:" data-toggle="dropdown" aria-expanded="false">
            <i class="icon-envelope fa-fw"></i>
            <span class="visible-xs-inline">Сообщения</span>
            <span class="badge badge-sm up bg-danger pull-right-xs">2</span>
        </a>
        <div class="dropdown-menu w-xl animated fadeInUp">
              <div class="panel bg-white">
                <div class="panel-heading b-light bg-light">
                  <strong>У вас <span>2</span> сообщения</strong>
                </div>
                <div class="list-group">
                  <a class="media list-group-item" href="">
                    <span class="pull-left thumb-sm">
                      <img class="img-circle" alt="..." src="http://lorempixel.com/50/50/people/">
                    </span>
                    <span class="media-body block m-b-none">
                      Привет мир<br>
                      <small class="text-muted">10 минут назад</small>
                    </span>
                  </a>
                  <a class="media list-group-item" href="">
                    <span class="media-body block m-b-none">
                      Пака мир<br>
                      <small class="text-muted">1 час назад</small>
                    </span>
                  </a>
                </div>
                <div class="panel-footer text-sm">
                  <a data-toggle="class:show animated fadeInRight" href="#notes">Смотреть все сообщения</a>
                </div>
              </div>
            </div>
     </li>
     <li class="dropdown" dropdown="">
        <a class="dropdown-toggle" href="javascript:" data-toggle="dropdown" aria-expanded="false">
            <span class="thumb-sm avatar pull-right m-t-n-sm m-b-n-sm m-l-sm">
                <img alt="..." src="http://lorempixel.com/50/50/people/">
                <i class="on md b-white bottom"></i>
            </span>
            <span class="hidden-sm hidden-md">test@test.ru</span>
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu animated fadeInRight w">
              <li>
                <a href="">
                  <span>Настройки</span>
                </a>
              </li>
              <li>
                <a href="#">Профиль</a>
              </li>
              <li class="divider"></li>
              <li>
                <a href="#">Выход</a>
              </li>
        </ul>
     </li>
          
    </ul>
</div>