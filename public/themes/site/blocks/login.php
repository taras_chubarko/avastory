<div id="block-login" class="pull-right">
    <form role="form" class="form-inline ng-pristine ng-valid ng-submitted">
        <div class="form-group">
          <label for="exampleInputEmail2" class="sr-only">Почта</label>
          <input type="email" placeholder="Почта" id="exampleInputEmail2" class="form-control">
        </div>
        <div class="form-group">
          <label for="exampleInputPassword2" class="sr-only">Пароль</label>
          <input type="password" placeholder="Пароль" id="exampleInputPassword2" class="form-control">
        </div>
        <button class="btn btn-rose" type="submit"><i class="icon-login"></i> Войти</button>
    </form>
</div>