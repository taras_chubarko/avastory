$(document).ready(function() {

    win_w = $('#wrapper').width();
    win_h = $('#wrapper').height();
    
    $('#map-canvas').height(win_h - 250);
    //console.log(win_h);
    $('#myModal18').modal('show');
    
    $( window ).resize(function() {
      
         win_w = $('#wrapper').width();
         win_h = $('#wrapper').height();
         $('#map-canvas').height(win_h - 250);
    });
    
    
    $.extend({
          getUrlVars: function(){
            var vars = [], hash;
            var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
            for(var i = 0; i < hashes.length; i++)
            {
              hash = hashes[i].split('=');
              vars.push(hash[0]);
              vars[hash[0]] = hash[1];
            }
            return vars;
          },
          getUrlVar: function(name){
            return $.getUrlVars()[name];
          }
     });
    
    if ($('body').hasClass('front')) {
       
       if (location.search.length > 0) {
          
          var user_to = $.getUrlVar('user_to');
          $.getJSON('/profiles/load/chat', {id:user_to}, function(data){
                    
                    $('#chatMap').html(data.users + data.forma);
                    
                    $('#user-1').animate({'margin-left': '20px'}, 500);
                    $('#user-2').animate({'margin-left': '960px'}, 500);
                    $('#chat').animate({'margin-top': '70px'}, 500);
                    
                    sendMessage();
                    
                    var chat_id = $('input[name=chat_id]').val();
                    loadMessage(chat_id);  
                    
          });
       }
       
    }
    
    
    
    
     
    
    var map;
    function initialize() {
               
               //console.log(userLatLng);
               
               var myLatlng = new google.maps.LatLng(parseFloat(Lat), parseFloat(Lng));
               
               var mapOptions = {
               zoom: 13,
               center: myLatlng
               };
               
               map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
               
               $.getJSON('/profiles/json', {}, function(data){
                         //console.log(data);
                    
               var markers = [];
               var that;
                    
                    $.each(data, function(i, item){
                         
                         var marker = new google.maps.Marker({
                             position: new google.maps.LatLng(item.lat, item.lng),
                             map: map,
                             title: item.name,
                             icon: item.icon,
                             bounds: true,
                         });
                         
                    
                    var boxText = document.createElement("div");
                    //boxText.style.cssText = "border: 4px solid #fff; margin-top: 8px; background: #fff; -webkit-border-radius: 75px; -moz-border-radius: 75px; border-radius: 75px;";
                    //boxText.innerHTML = '<div class="ovrls"><span class="badge bg-success">в сети</span><img class="img-circle" width="150" height="150" src="/image/users/'+item.user_id+'/150/150">'+btn+'</div>';
                    boxText.innerHTML = item.ovrls;
                            
                    var myOptions = {
                            content: boxText,
                            disableAutoPan: false,
                            maxWidth: 0,
                            pixelOffset: new google.maps.Size(-140, 0),
                            zIndex: null,
                            boxStyle: { 
                            background: 'url(themes/site/img/tbx.png) no-repeat 134px 0',
                              opacity: 80,
                              width: "160px",
                              height: "160px",
                             },
                            closeBoxMargin: "75px 1px 4px 223px",
                            closeBoxURL: "http://www.google.com/intl/en_us/mapfiles/close.gif",
                            infoBoxClearance: new google.maps.Size(1, 1),
                            isHidden: false,
                            pane: "floatPane",
                            enableEventPropagation: false,
                    };
            
                         var ib = new InfoBox(myOptions);
                         //ib.open(map, marker);
                         google.maps.event.addListener(marker, 'click', function() {
                              //console.log(marker);
                              //ib.close();
                              //e.preventDefault();
                              $('.infoBox').css('visibility', 'hidden');
                              ib.open(map, this);
                              map.panTo(this.position);
                              if (that)
                              {
                                   that.setZIndex();
                              }
                              that = this;
                              this.setZIndex(google.maps.Marker.MAX_ZINDEX + 1);
                              
                         });
                         
                         google.maps.event.addListener(ib, 'domready', function() {
                              startMess();
                         });
                         
                    });     
                         
               });/*$.getJSON*/
    }
    
    google.maps.event.addDomListener(window, 'load', initialize);
     // window.onload=initialize;
      
      var $myRequest;
      
       
      
      function startMess(user_to) {
          
          $('.start-mess').click(function(e){
                e.preventDefault();
                
               $('#chatMap').html('');
               var id;
               if (user_to)
               {
                    id = user_to;
               }
               else{
                    id = $(this).attr('data-user-id');
               }
               $.getJSON('/profiles/load/chat', {id:id}, function(data){
                    
                    $('#chatMap').html(data.users + data.forma);
                    
                    $('#user-1').animate({'margin-left': '20px'}, 500);
                    $('#user-2').animate({'margin-left': '960px'}, 500);
                    $('#chat').animate({'margin-top': '70px'}, 500);
                    
                    sendMessage();
                    
                    var chat_id = $('input[name=chat_id]').val();
                    loadMessage(chat_id);  
                    
               });
               
               
               
          });
      }
      
      
      
      function sendMessage() {
           
           //$('input[name=message]').keyup(function (event) {
           //    if (event.keyCode == '13') {
           //        event.preventDefault();
           //       // $("#sendMessageForm").submit();
           //    }
           //    return false;
           //});
          
          $("#sendMessageForm").submit(function(e) {
              e.preventDefault();
               var datas = $("#sendMessageForm").serialize();
               
               var message = $('input[name=message]').val();
               var id = $('input[name=chat_id]').val();
               
               if (message != '') {
                     
                    $.ajax({
                         type: "POST",
                         url: '/chats/send/message/ajax',
                         data: datas,
                         success: function(data)
                         {
                            $('input[name=message]').val('');
                            $('#chat .panel-body').append(data.chat);
                            $('#chat .panel-body').scrollTop(90000);
                         }
                    });
               }
               
               return false; 
           });
      }
      
      
      
      function loadMessage(id) {
           
           $myRequest = $.ajax({
               type: "POST",
               url: '/chats/load/'+id+'/ajax',
               //data: datas,
               success: function(data)
               {
                  $('#chat .panel-body').html(data.chat);
                  $('#chat .panel-body').scrollTop(90000);
                  
                  $('#chat .close').on('click', function(e){
                         e.preventDefault();
                         $('#chatMap').html('');
                         //location.reload();
                          clearTimeout(timeoutId);
                    });
                 
                 var timeoutId = setTimeout(function(){
                    loadMessage(id);
                   
                 }, 5000); 
                 
               }
          });
          
      }
      
      
      $.fn.scrollTo = function( target, options, callback ){
          if(typeof options == 'function' && arguments.length == 2){ callback = options; options = target; }
          var settings = $.extend({
            scrollTarget  : target,
            offsetTop     : 50,
            duration      : 500,
            easing        : 'swing'
          }, options);
          return this.each(function(){
            var scrollPane = $(this);
            var scrollTarget = (typeof settings.scrollTarget == "number") ? settings.scrollTarget : $(settings.scrollTarget);
            var scrollY = (typeof scrollTarget == "number") ? scrollTarget : scrollTarget.offset().top + scrollPane.scrollTop() - parseInt(settings.offsetTop);
            scrollPane.animate({scrollTop : scrollY }, parseInt(settings.duration), settings.easing, function(){
              if (typeof callback == 'function') { callback.call(this); }
            });
          });
        }
      	
}); 
