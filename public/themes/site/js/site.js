$(document).ready(function() {

    win_w = $('#wrapper').width();
    win_h = $('#wrapper').height();
    
    $('#map-canvas').height(win_h - 250);
    //console.log(win_h);
    $('#myModal18').modal('show');
    
    $("#tel").mask("+9(999) 9999 999");
    
    $( window ).resize(function() {
      
         win_w = $('#wrapper').width();
         win_h = $('#wrapper').height();
         $('#map-canvas').height(win_h - 250);
    });
    
    var input = document.getElementById('geocomplete');         
    var autocomplete = new google.maps.places.Autocomplete(input, {
        types: ["geocode"]
    });
    
    google.maps.event.addListener(autocomplete, 'place_changed', function(event) {
           fillInAddress();
    });
    
    function fillInAddress() {
          // Get the place details from the autocomplete object.
          var place = autocomplete.getPlace();
          $('input[name="lat"]').val(place.geometry.location.lat());
          $('input[name="lng"]').val(place.geometry.location.lng());
          for (var i = 0; i < place.address_components.length; i++) {
               
               var addressType = place.address_components[i].types[0];
               if (addressType == 'locality') {
                 $('input[name="city"]').val(place.address_components[i].long_name);
               }
          }
     }

    var map;
    function initialize() {
        
        if(navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
               var latitude = position.coords.latitude;
               var longitude = position.coords.longitude;
               
               //console.log(position);
               var mapOptions = {
               zoom: 12,
               center: new google.maps.LatLng(latitude, longitude)
               };
               
               map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
                
                var markers = [];
                var that;
                
                
                $.getJSON('/profiles/json', {}, function(data){
                    
                    
                    $.each(data, function(i, item){
                         
                         var marker = new google.maps.Marker({
                             position: new google.maps.LatLng(item.lat, item.lng),
                             map: map,
                             title: item.name,
                             icon: item.icon,
                             bounds: true,
                         });
                         
                     marker.setMap(map);
                    var boxText = document.createElement("div");
                    //boxText.style.cssText = "border: 4px solid #fff; margin-top: 8px; background: #fff; -webkit-border-radius: 75px; -moz-border-radius: 75px; border-radius: 75px;";
                    //boxText.innerHTML = '<div class="ovrls"><span class="badge bg-success">в сети</span><img class="img-circle" width="150" height="150" src="/image/users/'+item.user_id+'/150/150">'+btn+'</div>';
                    boxText.innerHTML = item.ovrls;
                            
                    var myOptions = {
                            content: boxText,
                            disableAutoPan: false,
                            maxWidth: 0,
                            pixelOffset: new google.maps.Size(-140, 0),
                            zIndex: null,
                            boxStyle: { 
                            background: 'url(themes/site/img/tbx.png) no-repeat 134px 0',
                              opacity: 80,
                              width: "160px",
                              height: "160px",
                             },
                            closeBoxMargin: "75px 1px 4px 223px",
                            closeBoxURL: "http://www.google.com/intl/en_us/mapfiles/close.gif",
                            infoBoxClearance: new google.maps.Size(1, 1),
                            isHidden: false,
                            pane: "floatPane",
                            enableEventPropagation: false,
                    };
            
                         var ib = new InfoBox(myOptions);
                         //ib.open(map, marker);
                         
                         google.maps.event.addListener(marker, 'click', function() {
                              //console.log(marker);
                              ib.open(map, this);
                              map.panTo(this.position);
                              if (that)
                              {
                                   
                                   that.setZIndex();
                              }
                              that = this;
                              this.setZIndex(google.maps.Marker.MAX_ZINDEX + 1);
                              //ib.close();
                         });
                         
                        
                         
                         //google.maps.event.addListener(ib, 'domready', function() {
                         //     startMess(ib);
                         //});
                         
                    });     
                         
               });/*$.getJSON*/ 
               
            
          });
        } else {
            alert("Geolocation API не поддерживается в вашем браузере");
        }    
      
    }
    
    google.maps.event.addDomListener(window, 'load', initialize);
    
    
    $('#form-register').validate({
        rules: {
            name: {
                required: true
            },
            email: {
                required: true,
                email : true,
            },
            adres: {
                required: true
            },
            telephone: {
                required: true
            },
            password: {
                required: true,
                minlength: 6,
            },
            password_confirmation: {
                required: true,
                minlength: 6,
                //equalTo: '#password',
            },
            gender: {
                required: true
            },
            gender_search: {
                required: true
            },
            //pravila: {
            //    required: true
            //}
        },
        messages:{

            name:{
                required: 'Поле "Имя" обязательно для заполнения',
            },
            
            gender:{
                required: 'Это поле обязательно для заполнения',
            },
            
            gender_search:{
                required: 'Это поле обязательно для заполнения',
            },
            
            //pravila:{
            //    required: 'Это поле обязательно для заполнения',
            //},
            
            email:{
                required: 'Поле "Почта" обязательно для заполнения',
                email: 'Поле "Почта" заполнено не корректно',
            },

            adres:{
                required: 'Поле "Я жииву в" обязательно для заполнения',
            },
            telephone:{
               required: 'Поле "Телефон" обязательно для заполнения',
            },
            password:{
               required: 'Поле "Пароль" обязательно для заполнения',
               minlength: 'Пароль должен быть минимум 6 символа',
            },
            password_confirmation:{
               required: 'Поле "Повторить пароль" обязательно для заполнения',
               minlength: 'Пароль должен быть минимум 6 символа',
               //equalTo: 'Не совпадение паролей',
            },

       },
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function(error, element) {
            if(element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        }
    });
    
   
}); 
