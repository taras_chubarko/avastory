$(document).ready(function() {
    
    $('#irinaCarousel').carousel({
        interval: 10000
    });
    
    $( '.thinking' ).hover(function() {
        $(this).popover('show');
    }, function() {
        $(this).popover('destroy');
    });
    
    
    
    $('.fdi-Carousel .item').each(function () {
        var next = $(this).next();
        if (!next.length) {
            next = $(this).siblings(':first');
        }
        next.children(':first-child').clone().appendTo($(this));

        if (next.next().length > 0) {
            next.next().children(':first-child').clone().appendTo($(this));
        }
        else {
            $(this).siblings(':first').children(':first-child').clone().appendTo($(this));
        }
    });
    
    
    /**
      * uploadUslugiImage
      * @param 
      */
     
    $.fn.uploadImage = function(folder) {
         
        if($.fn.fileupload){ 
          $('#'+folder+'-image').fileupload({
               url: '/upload/'+folder,
               method: 'PUT',
               dataType: 'json',
               done: function (e, data) {
                    //console.log(data);
                    $('#progress').addClass('hidden');
                    $('input[name="image"]').val(data.result.file);
                    $('.thumb-lg').html(data.result.img).css('margin-bottom', 20);
                    $.fn.uploadImage(folder);
                    
               },
               progressall: function (e, data) {
                   $('#progress').removeClass('hidden');
                   var progress = parseInt(data.loaded / data.total * 100, 10);
                   $('#progress .progress-bar').css(
                       'width',
                       progress + '%'
                   );
               }
          });
        }
     }
     
    
    
    //uslugiBlock
    
    $.fn.equalize = function(mh) {
        //$(this).css('min-height', mh);
        var maxHeight = 0;
        this.each(function(){
            if( $(this).height() > maxHeight ) {
                maxHeight = $(this).height();
            }
        });
        this.height(maxHeight);
    };
     //$('#uslugiBlock .thumbnail').equalize(260);
    
    /**
     * loadPhotoAjax
     * @param arg1
     */
    
    function loadPhotoAjax() {
       
       $('.loadPhotoAjax').on('click', function(){
         
            var id = $(this).attr('data-photo-id');
             
            $.get( '/photos/'+id).done(function( data ) {
                  $('body').append(data.photo);
                  
                  var t = $('#loadPhotoAjax .pan');
                  var w = $( window );
                  var top = (w.height() - t.height()) / 2;
                  //var left = (w.width() - t.width()) / 2;
                    t.css({
                        position: "fixed",
                        top: top,
                        //left: left
                    });
                    
                    loadPhotoAjaxNextPrev('.prev');
                    loadPhotoAjaxNextPrev('.next');
                    saveCommentAjax();
                    strarRating();
                    
                    $('#photo-comments').slimScroll({
                        height: '530px'
                    });
                    
                    $('.close').on('click', function(){
                        $('#loadPhotoAjax').remove();
                    });
                  
                  
            });
        
       });
       
       
    }
    loadPhotoAjax();
    
    /**
     * loadPhotoAjaxNextPrev
     * @param arg
     */
    
    function loadPhotoAjaxNextPrev(arg) {
       
       $(arg).on('click', function(){
            //$('#loadPhotoAjax .pan').remove();
            var id = $(this).attr('data-id');
            
            $.get( '/photos/'+id+'/nextprev').done(function( data ) {
                  //$('body').append(data.photo);
                  
                  var t = $('#loadPhotoAjax .pan');
                  var w = $( window );
                  var top = (w.height() - t.height()) / 2;
                  //var left = (w.width() - t.width()) / 2;
                    t.css({
                        position: "fixed",
                        top: top,
                        //left: left
                    });
                    
                    $('#loadPhotoAjax .middle-content').html(data.photo);
                    $('#loadPhotoAjax .middle-right-sidebar').html(data.comments);
                    
                    
                    loadPhotoAjaxNextPrev('.prev');
                    loadPhotoAjaxNextPrev('.next');
                    saveCommentAjax();
                    strarRating();
                    
                    $('#photo-comments').slimScroll({
                        height: '530px'
                    });
                    
                    $('.close').on('click', function(){
                        $('#loadPhotoAjax').remove();
                    });
                  
                  
            });
            
        });
       
    }
    
    
    /**
     * setLike
     * @param 
     */
    
    function setLike() {
       
        $('a.like-sm').on('click', function(){
          
            var type = $(this).attr('data-type');
            var type_id = $(this).attr('data-type_id');
          
            $.post('/set/like', {type:type, type_id:type_id} ,function(data, status){
                
                $('.like-'+data.type_id).parent().find('.like-count').remove();
                $('.like-'+data.type_id).after(' <span class="label bg-light dk like-count">+'+data.likes+'</span>');
                
            });
         
        });
        
        
        $('a.like-link').on('click', function(){
          
            var type = $(this).attr('data-type');
            var type_id = $(this).attr('data-type_id');
          
            $.post('/set/like', {type:type, type_id:type_id} ,function(data, status){
                $('.like-link-'+data.type_id+' span').text(data.likes);
            });
         
        });
       
    }
    setLike();
    
    //$('#photo-comments').niceScroll();
    
    /**
     * saveCommentAjax
     * @param
     */
    
    function saveCommentAjax() {
      
        $('#comments-store-ajax').on('submit', function(e){
            
            e.preventDefault();
             
            $.post( $( this ).prop( 'action' ),
                $( this ).serialize(),
            function(data, status){
                
                if (data.msg)
                {
                    $.each(data.msg, function(key, value){
                        alert(value);
                    });
                }
                
                $('#comments-store-ajax input[name="comment"]').val('');
                $('#photo-comments').html(data.comments).css('display', 'none');
                $('#photo-comments').fadeIn( "slow" );
                $('#photo-comments').slimScroll({
                    height: '530px'
                });
            });
            
            return false;
        });      
      
    }
    
    
    /*
     * function strarRating
     * @param 
     */
    
    function strarRating() {
        
        $('.rating').rating();
        
        $('.rating').on('change', function (e) {
            e.preventDefault();
            var value = $(this).val();
            var type = $(this).data('type');
            var type_id = $(this).data('type_id');
            
            $.post('/star/add', {type:type, type_id:type_id, value:value} ,function(data, status){
                //alert('Ваша оценка принята.');
            });
           
        });
        
        
    }
    
    
    
});

var admin = (function() {
        
    var that = {};
    
    that.article = function (folder) {
      
       $('#input-image').fileinput({
        allowedFileExtensions : ['png', 'jpg', 'jpeg'],
        uploadUrl: '/upload/'+folder,
        uploadAsync: true,
        showUpload: true,
        dropZoneEnabled:false,
        showPreview:false,
        maxFileCount: 1,
        //uploadExtraData:{id:id},
        slugCallback: function (filename){
          return filename;
        }
        }).on('filebatchuploadsuccess', function(event, data, previewId, index) {
            $('.image-upload').html(data.response.result);
            //$('input[name=logo]').val(data.response.file);
        });
    }
   
   return that;
})();

