$(document).ready(function() {
    

     
     /**
      * uploadAvatar
      * @param 
      */
     
     function uploadAvatar() {
          
          $('#avatar-image').fileupload({
               url: '/upload/avatar/'+profileID,
               dataType: 'json',
               done: function (e, data) {
                    //console.log(data);
                    //$('#progress').addClass('hidden');
                    $('.ava-big').replaceWith(data.result.imgBig);
                    $('.ava-smal').replaceWith(data.result.imgSmal);
                    uploadAvatar();
                    uploadFon();
               },
               progressall: function (e, data) {
                   //$('#progress').removeClass('hidden');
                   var progress = parseInt(data.loaded / data.total * 100, 10);
                   //$('#progress .progress-bar').css(
                   //    'width',
                   //    progress + '%'
                   //);
                   $('.easyPieChart').data('easyPieChart').update(progress);
               }
          });
     }
     
     uploadAvatar();
     
     $('.easyPieChart').easyPieChart({
        size: 208,
        percent: 0,
        lineWidth: 5,
        trackColor: '#e8eff0',
        barColor: '#23b7e5',
        scaleColor: false,
        lineCap: 'butt',
        rotate: -90,
        animate: 1000                   
     });
     
     $('#avatar-image').filestyle();
     $('#load-avatar, #load-fon').tooltip();
     
     /**
      * uploadFon
      * @param 
      */
     
     function uploadFon() {
          
          $('#fon-image').fileupload({
               url: '/upload/fon/'+profileID,
               dataType: 'json',
               done: function (e, data) {
                    //console.log(data);
                    $('#progress').addClass('hidden');
                    $('#ubf').css('background', 'url(/uploads/users/fon/'+data.result.file+') center center');
                    $('#ubf').css('background-size', 'cover');
                   
                    uploadFon();
                    uploadAvatar();
               },
               progressall: function (e, data) {
                   $('#progress').removeClass('hidden');
                   var progress = parseInt(data.loaded / data.total * 100, 10);
                   $('#progress .progress-bar').css(
                       'width',
                       progress + '%'
                   );
               }
          });
     }
     uploadFon();
     
     
     /**
      * editName
      * @param 
      */
     
     function editName() {
          
          $('.editName').click(function(){
               
               var profile_id = $(this).attr('data-profire-id');
               var name = $('#kontakt').find('.name');
               var nametext = $('#kontakt').find('.name').text();
               
               name.html('<div class="input-group"><input type="text" name="name" value="'+nametext+'" class="form-control"  placeholder="Ваше имя"><span class="input-group-addon"><a href="javascript:" class="nameSave"><i class="fa fa-floppy-o"></i></a> &ensp;<a href="javascript:" class="nameClose"><i class="fa fa-times"></i></a></span></div>');
               
               $('.nameClose').click(function(){
                    
                    $('#kontakt').find('.name').html(nametext);
                    
               });
               
               $('.nameSave').click(function(){
                    
                    var name = $('input[name="name"]').val();
                    
                    if (name.length > 0)
                    {
                        $('#kontakt').find('.name').html(name);
                        $('.names .h3').text(name);
                        $.post('/user/profile/save/name', { name:name, profile_id:profile_id } );
                    }
                    else
                    {
                         alert('Заполните поле имени');
                    }
                    
               });      
               
          });
          
     }
     editName();
     
     
     /**
      * editTelephone
      * @param 
      */
     
     function editTelephone() {
        
          $('.editTelephone').click(function(){
               
               var profile_id = $(this).attr('data-profire-id');
               var telephone = $('#kontakt').find('.telephone');
               var telephonetext = $('#kontakt').find('.telephone').text();
               
               telephone.html('<div class="input-group"><input type="text" name="telephone" value="'+telephonetext+'" class="form-control"  placeholder="Мобильный телефон"><span class="input-group-addon"><a href="javascript:" class="telephoneSave"><i class="fa fa-floppy-o"></i></a> &ensp;<a href="javascript:" class="telephoneClose"><i class="fa fa-times"></i></a></span></div>');
               $('input[name="telephone"]').mask("+9(999) 9999 999");
               
               $('.telephoneClose').click(function(){
                    
                    $('#kontakt').find('.telephone').html(telephonetext);
                    
               });
               
               $('.telephoneSave').click(function(){
                    
                    var telephone = $('input[name="telephone"]').val();
                    
                    if (telephone.length > 0)
                    {
                        $('#kontakt').find('.telephone').html(telephone);
                        $.post('/user/profile/save/telephone', { telephone:telephone, profile_id:profile_id } );
                    }
                    else
                    {
                         alert('Заполните поле телефона');
                    }
                    
               });
               
          });
        
     }
     editTelephone();
     
     
     /**
      * editCity
      * @param
      */
     
     function editCity() {
          $('.editCity').click(function(){
               
               var profile_id = $(this).attr('data-profire-id');
               var city = $('#kontakt').find('.city');
               var citytext = $('#kontakt').find('.city').text();
               
               city.html('<div class="input-group"><input type="hidden" name="adres" value=""><input id="cityGoogle" type="text" name="city" value="'+citytext+'" class="form-control"  placeholder="Город"><span class="input-group-addon"><a href="javascript:" class="citySave"><i class="fa fa-floppy-o"></i></a> &ensp;<a href="javascript:" class="cityClose"><i class="fa fa-times"></i></a></span></div>');
               
               var input = document.getElementById('cityGoogle');         
               var autocomplete = new google.maps.places.Autocomplete(input, {
                   types: ["geocode"]
               });
               
               google.maps.event.addListener(autocomplete, 'place_changed', function(event) {
                      fillInAddress();
               });
               
               function fillInAddress() {
                     // Get the place details from the autocomplete object.
                     var place = autocomplete.getPlace();
                     
                     $('input[name="adres"]').val(place.formatted_address);
                     
                     for (var i = 0; i < place.address_components.length; i++) {
                          var addressType = place.address_components[i].types[0];
                          if (addressType == 'locality') {
                            $('input[name="city"]').val(place.address_components[i].long_name);
                          }
                     }
                }
                
                //console.log(fulladres);
               
               
               $('.cityClose').click(function(){
                    
                    $('#kontakt').find('.city').html(citytext);
                    
               });
               
               $('.citySave').click(function(){
                    
                    var city = $('input[name="city"]').val();
                    var adres = $('input[name="adres"]').val();
                    
                    if (city.length > 0)
                    {
                        $('#kontakt').find('.city').html(city);
                        $('.names small').text(adres);
                        $.post('/user/profile/save/city', { city:city, profile_id:profile_id, adres:adres } );
                    }
                    else
                    {
                         alert('Заполните поле города');
                    }
                    
               });
               
          });
     }
     editCity();
     
     
     /**
      * editBithday
      * @param
      */
     
     function editBithday() {
          $('.editBithday').click(function(){
               
               var profile_id = $(this).attr('data-profire-id');
               var bithday = $('#kontakt').find('.bithday');
               var bithdaytext = $('#kontakt').find('.bithday').text();
               
               bithday.html('<div class="input-group"><input id="datetimepickerBithday" type="text" name="bithday" value="'+bithdaytext+'" class="form-control"  placeholder="Дата рождения"><span class="input-group-addon"><a href="javascript:" class="bithdaySave"><i class="fa fa-floppy-o"></i></a> &ensp;<a href="javascript:" class="bithdayClose"><i class="fa fa-times"></i></a></span></div>');
               
                $('#datetimepickerBithday').datetimepicker({
                    locale: 'ru',
                    format: 'DD MMMM YYYY'
                });
               
               $('.bithdayClose').click(function(){
                    
                    $('#kontakt').find('.bithday').html(bithdaytext);
                    
               });
               
               $('.bithdaySave').click(function(){
                    
                    var bithday = $('input[name="bithday"]').val();
                    
                    if (bithday.length > 0)
                    {
                        $('#kontakt').find('.bithday').html(bithday);
                        $.post('/user/profile/save/bithday', { bithday:bithday, profile_id:profile_id } ).done(function( data ) {
                          $('.age').text(data.age);
                        });;
                    }
                    else
                    {
                         alert('Заполните поле даты рождения');
                    }
                    
               });
               
          });
     }
     editBithday();
     
     
     /**
      * editGender
      * @param 
      */
     
     function editGender() {
          
          $('.editGender').click(function(){
               
               var profile_id = $(this).attr('data-profire-id');
               var gender = $('#kontakt').find('.gender');
               var gendertext = $('#kontakt').find('.gender').text();
               
               gender.html('<div class="input-group"><select id="gender-select" name="gender" class="form-control"><option value="">Выбрать пол</option><option value="1">Мужской</option><option value="2">Женский</option></select><span class="input-group-addon"><a href="javascript:" class="genderSave"><i class="fa fa-floppy-o"></i></a> &ensp;<a href="javascript:" class="genderClose"><i class="fa fa-times"></i></a></span></div>');
               
               $('.genderClose').click(function(){
                    
                    $('#kontakt').find('.gender').html(gendertext);
                    
               });
               
               $('.genderSave').click(function(){
                    var gender = $('select[name="gender"]').val();
                    
                    if (gender.length > 0)
                    {
                         $('#kontakt').find('.gender').html($( "#gender-select option:selected" ).text());
                         $.post('/user/profile/save/gender', { gender:gender, profile_id:profile_id} );
                    }
                    else
                    {
                         alert('Заполните поле Пол');
                    }
               });
          });
          
     }
     editGender();
     
     /**
      * editGenderSearch
      * @param
      */
     
     function editGenderSearch() {
          $('.editGenderSearch').click(function(){
               
               var profile_id = $(this).attr('data-profire-id');
               var gender = $('#kontakt').find('.gender_search');
               var gendertext = $('#kontakt').find('.gender_search').text();
               
               gender.html('<div class="input-group"><select id="gender-select-search" name="gender_search" class="form-control"><option value="">Выбрать пол</option><option value="1">Мужчину</option><option value="2">Женщину</option></select><span class="input-group-addon"><a href="javascript:" class="gender_searchSave"><i class="fa fa-floppy-o"></i></a> &ensp;<a href="javascript:" class="gender_searchClose"><i class="fa fa-times"></i></a></span></div>');
               
               $('.gender_searchClose').click(function(){
                    
                    $('#kontakt').find('.gender_search').html(gendertext);
                    
               });
               
               $('.gender_searchSave').click(function(){
                    var gender = $('select[name="gender_search"]').val();
                    
                    if (gender.length > 0)
                    {
                         $('#kontakt').find('.gender_search').html($( "#gender-select-search option:selected" ).text());
                         $.post('/user/profile/save/gender-search', { gender_search:gender, profile_id:profile_id} );
                    }
                    else
                    {
                         alert('Заполните поле Я ищу');
                    }
               });
          });
     }
     editGenderSearch();
     
     
      /**
       * userLoadMessages
       * @param arg1
       */
      
      function userLoadMessages() {
         
         $('.userLoadMessages').on('click', function(e){
               e.preventDefault();
               $(this).find('.badge').remove();
               var id = $(this).data('chat-id');
               var user_to = $(this).data('user-id');
               
               $.ajax({
                    type: "POST",
                    url: '/chats/load/'+id+'/ajax',
                    //data: datas,
                    success: function(data)
                    {
                      var result;
                      if (data.chat.length > 0) {
                         result =  data.chat; 
                      }
                      else
                      {
                         result = '<p class="text-center">Новых сообщений нет.</p>';
                      }
                      $('.umRight').html(result);
                      $('.umRight').append('<a href="javascript:" class="btn m-b-xs btn-default sendME" data-user-id="'+user_to+'"><i class="icon-envelope-letter"></i> Написать</a>');
                      
                         $('.sendME').on('click', function(e){
                         e.preventDefault();
                             var id = $(this).data('user-id');
                             $.ajax({
                                 type: "POST",
                                 url: '/chats/sendme/ajax',
                                 data: {user_to:id},
                                 success: function(data)
                                 {
                                   location.href='/?chat='+data.chat+'&user_to='+id;
                                 }
                             });
                         
                         });
                      
                    }
              });
               
          });
         
      }
      userLoadMessages();
     
});
